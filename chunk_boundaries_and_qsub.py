import os

def chunk_file(f, chunksize=25):
    with open(f, "r") as ff:
        f_lines = ff.readlines()

    for i in xrange(1, len(f_lines), chunksize):
        yield f_lines[i:i+chunksize]

def rm_mkdir(d):
    if os.path.exists(d):
        os.system("rf -rf "+d)
    os.mkdir(d)

def main(steps, boundary_file):
    output_dir = "Resources/Boundaries"

    qsub_template = """ 
#!/bin/sh
#PBS -l nodes=1
#PBS -l walltime=24:00:00
#PBS -V
#PBS -m bea
#PBS -M michaelgrosner@gmail.com
cd $PBS_O_WORKDIR

./LoopingSimulation -n %(n)i -s Ideal+%(steps)i -l fixed -b %(chunk_boundary_file)s -z Resources/%(boundary_file)s -p None --radi 20 -o output/output_%(boundary_file)s_%(i)i_%(steps)i -w 5000 -a 1000
    """

    if steps == 385:
        n = 1000000
    else:
        n = 700000000

    for i, chunk in enumerate(chunk_file(boundary_file)):
        chunk_boundary_file = "%(output_dir)s/%(boundary_file)s_%(i)i" % locals()
        with open(chunk_boundary_file, "w") as chunkfile:
            for bc in chunk:
                chunkfile.write(boundary_file+'-'+bc)
        with open("%(chunk_boundary_file)s_%(steps)i.qsub" % locals(), "w") as qsub:
            qsub.write(qsub_template % locals())

for steps, op_file in [(78, "Osym_Osym"), (76, "O1_O3"), (385, "O1_O2"), (76, "O1_O2")]:
    main(steps, op_file)
