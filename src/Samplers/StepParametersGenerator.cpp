/*
 * StepParametersGenerator.cpp
 *
 *  Created on: Mar 31, 2013
 *      Author: grosner
 */

#include "StepParametersGenerator.h"
#include "PlaceProteinsOnSteps.h"
#include "BaseSequence.h"
#include "BaseSampler.h"

StepParametersVector StepParametersGenerator::generate_steps_vector(const Seed& s, const unsigned int end, const unsigned int start, const LocationCollection& lc) {

	auto steps = sampler->sample(s, end, start);
	protein_placer->place_proteins_on_steps(lc, start, steps);

	return steps;
}

StepsVectorAndEnergyVectors StepParametersGenerator::generate_steps_vector_with_energy(
		const Seed& s, const unsigned int end, const unsigned int start,
		const LocationCollection& lc) {
	vector<Real> E;

	auto steps = sampler->sample(s, end, start);

	for (unsigned int s = start; s < end; s++) {
		auto tp = steps[s-start] - dna->mean(s);
		E.push_back(.5*(tp.transpose()*dna->force_constant(s)*tp)(0,0));
	}

	protein_placer->place_proteins_on_steps(lc, start, steps);

	return make_pair(steps, E);
}
