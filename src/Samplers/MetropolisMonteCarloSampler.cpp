#include "MetropolisMonteCarloSampler.h"
#include "RandomSampler.h"
#include "BaseSequence.h"
#include "MetropolisMonteCarloAcceptanceCriteria.h"

Real MetropolisMonteCarloSampler::calculate_energy(const StepParametersVector& steps) {
	Real E;
	// needs to be adjusted along dna seq, also needs to be extracted from StepParametersGenerator
	for (unsigned int s = 0; s < steps.size(); s++) {
		auto tp = steps[s] - dna->mean(s);
		E += .5*(tp.transpose()*dna->force_constant(s)*tp)(0,0);
	}
	return E;
}

StepParametersVector MetropolisMonteCarloSampler::sample(const Seed& s, const unsigned int end, const unsigned int start) {
	rng->reseed(s);

	if (stored_steps.size() == 0 and stored_energy == 0) {
		stored_steps = gaussian_sampler->sample(s, end, start);
		stored_energy = calculate_energy(stored_steps);
	}

	auto chosen_step = rng->sample_uniform()*(end-start);
	StepParameters temp(stored_steps.at(chosen_step));

	auto new_step = gaussian_sampler->sample(s, start+1, start).front();

	stored_steps[chosen_step] = new_step;

	auto new_energy = calculate_energy(stored_steps);

	if (acceptance_function->accept(stored_energy, new_energy)) {
		stored_energy = new_energy;
		return stored_steps;
	}
	else {
		stored_steps[chosen_step] = temp;
		return stored_steps;
	}

}
