/*
 * StepParametersGenerator.h
 *
 *  Created on: Mar 31, 2013
 *      Author: grosner
 */

#ifndef StepParametersGenerator_H_
#define StepParametersGenerator_H_

#include "Forwards.h"
#include "BasePairAndStep.h"
#include "LocationCollection.h"
#include <utility>
#include <vector>

using namespace std;

typedef pair<StepParametersVector, vector<Real> > StepsVectorAndEnergyVectors;

class StepParametersGenerator {
	PlaceProteinsOnStepsPtr protein_placer;
	BaseSequencePtr dna;
	BaseSamplerPtr sampler;
public:
	StepParametersGenerator(BaseSequencePtr dna,
					        PlaceProteinsOnStepsPtr protein_placer,
					        BaseSamplerPtr sampler) : dna(dna),
					        						  protein_placer(protein_placer),
					        						  sampler(sampler) {}
	virtual ~StepParametersGenerator() {}

	StepParametersVector generate_steps_vector(const Seed& s, const unsigned int end, const unsigned int start, const LocationCollection& lc);
	StepsVectorAndEnergyVectors generate_steps_vector_with_energy(const Seed& s, const unsigned int end, const unsigned int start, const LocationCollection& lc);
};

#endif /* GAUSSIANSAMPLER_H_ */
