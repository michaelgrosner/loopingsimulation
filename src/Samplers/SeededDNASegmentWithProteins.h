/*
 * SeededDNASegmentWithProteins.h
 *
 *  Created on: May 27, 2013
 *      Author: grosner
 */

#ifndef SEEDEDDNASEGMENTWITHPROTEINS_H_
#define SEEDEDDNASEGMENTWITHPROTEINS_H_

#include "Includes.h"
#include "LocationCollection.h"

class SeededDNASegmentWithProteins {
	Seed                      _seed;
	Integer64                 _start;
	Integer64                 _stop;
	const LocationCollection& _protein_locations;
public:
	SeededDNASegmentWithProteins(const Seed seed,
								 const Integer64 start,
								 const Integer64 stop,
								 const LocationCollection& protein_locations) : _seed(seed),
										 	 	 	 	 	 	 	 	  _start(start),
										 	 	 	 	 	 	 	 	  _stop(stop),
										 	 	 	 	 	 	 	 	  _protein_locations(protein_locations) {}
	inline const Seed seed() const                             { return _seed; }
	inline const Integer64 start() const                       { return _start; }
	inline const Integer64 stop() const                        { return _stop; }
	inline const LocationCollection& protein_locations() const { return _protein_locations; }
};


#endif /* SEEDEDDNASEGMENTWITHPROTEINS_H_ */
