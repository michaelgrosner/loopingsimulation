/*
 * MetropolisMonteCarloAcceptanceCriteria.h
 *
 *  Created on: May 30, 2013
 *      Author: grosner
 */

#ifndef METROPOLISMONTECARLOACCEPTANCECRITERIA_H_
#define METROPOLISMONTECARLOACCEPTANCECRITERIA_H_

#include "Includes.h"

class MetropolisMonteCarloAcceptanceCriteria {
public:
	virtual ~MetropolisMonteCarloAcceptanceCriteria() {}
	virtual bool accept(const Real previous_energy, const Real new_energy) = 0;
};

#endif /* METROPOLISMONTECARLOACCEPTANCECRITERIA_H_ */
