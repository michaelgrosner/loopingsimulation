/*
 * AcceptWithTemperature.cpp
 *
 *  Created on: May 30, 2013
 *      Author: grosner
 */

#include "AcceptWithTemperature.h"
#include "RandomSampler.h"

bool AcceptWithTemperature::accept(const Real previous_energy, const Real new_energy) {
	if (new_energy < previous_energy) return true;
	if (rng->sample_uniform() > temperature) return true;
	return false;
}
