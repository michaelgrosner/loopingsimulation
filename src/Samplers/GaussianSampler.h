/*
 * GaussianSampler.h
 *
 *  Created on: May 27, 2013
 *      Author: grosner
 */

#ifndef GAUSSIANSAMPLER_H_
#define GAUSSIANSAMPLER_H_

#include "Forwards.h"
#include "BaseSampler.h"

class GaussianSampler : public BaseSampler {
	RandomSamplerPtr rng;
	BaseSequencePtr dna;
public:
	virtual ~GaussianSampler() {}
	GaussianSampler(RandomSamplerPtr rng, BaseSequencePtr dna) : rng(rng), dna(dna) {}
	virtual StepParametersVector sample(const Seed& s, const unsigned int end, const unsigned int start);
};

#endif /* GAUSSIANSAMPLER_H_ */
