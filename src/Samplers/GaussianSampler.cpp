/*
 * GaussianSampler.cpp
 *
 *  Created on: May 27, 2013
 *      Author: grosner
 */

#include "GaussianSampler.h"
#include "RandomSampler.h"
#include "BaseSequence.h"

StepParametersVector GaussianSampler::sample(const Seed& s, const unsigned int end, const unsigned int start) {
	rng->reseed(s);
	StepParametersVector steps(end-start);
	for (auto n = start; n < end; n++) {
		for (auto j = 0; j < 6; j++) {
			steps[n - start][j] = dna->std(n)[j] * rng->sample_normal();
		}
		steps[n-start] = dna->mean(n) + dna->coupling_matrix(n)*steps[n-start];
	}
	return steps;
}
