/*
 * BaseSampler.h
 *
 *  Created on: May 27, 2013
 *      Author: grosner
 */

#ifndef BASESAMPLER_H_
#define BASESAMPLER_H_

#include "Includes.h"
#include "BasePairAndStep.h"

class BaseSampler {
public:
	virtual ~BaseSampler() {}
	virtual StepParametersVector sample(const Seed& s, const unsigned int end, const unsigned int start) = 0;
};


#endif /* BASESAMPLER_H_ */
