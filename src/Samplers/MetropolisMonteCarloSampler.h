/*
 * MetropolisMonteCarloSampler.h
 *
 *  Created on: May 30, 2013
 *      Author: grosner
 */

#ifndef METROPOLISMONTECARLOSAMPLER_H_
#define METROPOLISMONTECARLOSAMPLER_H_

#include "Forwards.h"
#include "Includes.h"
#include "BaseSampler.h"

class MetropolisMonteCarloAcceptanceCriteria;
typedef boost::shared_ptr<MetropolisMonteCarloAcceptanceCriteria> MetropolisMonteCarloAcceptanceCriteriaPtr;

class MetropolisMonteCarloSampler : public BaseSampler {
	MetropolisMonteCarloAcceptanceCriteriaPtr acceptance_function;
	RandomSamplerPtr rng;
	StepParametersVector stored_steps;
	Real stored_energy;
	BaseSamplerPtr gaussian_sampler;
	BaseSequencePtr dna;

	Real calculate_energy(const StepParametersVector& steps);

public:
	MetropolisMonteCarloSampler(RandomSamplerPtr rng,
								MetropolisMonteCarloAcceptanceCriteriaPtr acceptance_function,
								BaseSamplerPtr gaussian_sampler,
								BaseSequencePtr dna) : rng(rng),
													   acceptance_function(acceptance_function),
													   gaussian_sampler(gaussian_sampler),
													   stored_energy(0),
													   dna(dna) {}

	virtual ~MetropolisMonteCarloSampler() {}
	virtual StepParametersVector sample(const Seed& s, const unsigned int end, const unsigned int start);
};

#endif /* METROPOLISMONTECARLOSAMPLER_H_ */
