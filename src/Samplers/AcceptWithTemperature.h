/*
 * AcceptWithTemperature.h
 *
 *  Created on: May 30, 2013
 *      Author: grosner
 */

#ifndef ACCEPTWITHTEMPERATURE_H_
#define ACCEPTWITHTEMPERATURE_H_

#include "MetropolisMonteCarloAcceptanceCriteria.h"
#include <boost/shared_ptr.hpp>

class RandomSampler;
typedef boost::shared_ptr<RandomSampler> RandomSamplerPtr;

class AcceptWithTemperature : public MetropolisMonteCarloAcceptanceCriteria {
	RandomSamplerPtr rng;
	Real temperature;
public:
	AcceptWithTemperature(RandomSamplerPtr rng,
						  Real temperature) : rng(rng),
						   			          temperature(temperature) {}
	virtual ~AcceptWithTemperature() {}
	virtual bool accept(const Real previous_energy, const Real new_energy);
};

#endif /* ACCEPTWITHTEMPERATURE_H_ */
