/*
 * LoopReporter.h
 *
 *  Created on: Jan 6, 2013
 *      Author: grosner
 */

#ifndef LOOPREPORTER_H_
#define LOOPREPORTER_H_

#include "Includes.h"
#include "Reporter.h"
#include "BoundaryConditionCounter.h"
#include "ClosureStringGenerator.h"
#include <boost/thread/mutex.hpp>


class LoopReporter : public Reporter {
public:
	LoopReporter(const ClosureStringGenerator& csv_generator,
				 const Integer64 n_loops,
			     const string output_prefix,
			     const string run_hash);

	bool add_loop(const ClosurePtr& l);

	virtual void report();
private:
	mutable boost::mutex mutex;

	const ClosureStringGenerator& csv_generator;
	Integer64 n_loops;
	string output_prefix;
	string run_hash;

	string header;
	vector<string> loop_infos;
	string path;
	BoundaryConditionCounter boundaries_to_n_reported_loops;
};

#endif /* LOOPREPORTER_H_ */
