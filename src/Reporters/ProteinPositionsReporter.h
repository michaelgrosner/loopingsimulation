/*
 * ProteinPositionsReporter.h
 *
 *  Created on: Dec 30, 2012
 *      Author: grosner
 */

#ifndef PROTEINPOSITIONSREPORTER_H_
#define PROTEINPOSITIONSREPORTER_H_

#include "Includes.h"
#include "Reporter.h"
#include "Forwards.h"
#include <boost/thread/mutex.hpp>
#include <vector>
#include <string>

using namespace std;

class ProteinPositionsReporter : public Reporter {
public:
	ProteinPositionsReporter(ClosureStringGenerator& unique_id_generator,
							 const string output_prefix,
							 const string run_hash);

	void add_proteins_from_loop(const ClosurePtr& l);

	virtual void report();

private:
	mutable boost::mutex mutex;

	ClosureStringGenerator& unique_id_generator;
	string output_prefix;
	string run_hash;
	vector<string> protein_infos;
	string path, header;
};

#endif /* PROTEINPOSITIONSREPORTER_H_ */
