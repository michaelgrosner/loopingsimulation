/*
 * SimulationProgressReporter.h
 *
 *  Created on: Dec 12, 2013
 *      Author: grosner
 */

#ifndef SIMULATIONPROGRESSREPORTER_H_
#define SIMULATIONPROGRESSREPORTER_H_

#include "Includes.h"
#include "Reporter.h"
#include <string>
#include <vector>
#include <boost/thread/mutex.hpp>
#include "FileSink.h"

using namespace std;

class SimulationCheckpoint {
public:
	SimulationCheckpoint(UInteger64 n_halfchains,
						 UInteger64 n_generated_loops,
						 vector<UInteger64> boundary_counts,
						 UInteger64 n_passed_radi,
						 UInteger64 n_passed_gam,
						 UInteger64 n_passed_twi) : n_halfchains(n_halfchains),
													n_generated_loops(n_generated_loops),
													boundary_counts(boundary_counts),
													n_passed_radi(n_passed_radi),
													n_passed_gam(n_passed_gam),
													n_passed_twi(n_passed_twi) {}

	UInteger64 n_halfchains;
	UInteger64 n_generated_loops;
	vector<UInteger64> boundary_counts;
	UInteger64 n_passed_radi;
	UInteger64 n_passed_gam;
	UInteger64 n_passed_twi;
};

class SimulationProgressReporter : public Reporter {
public:
	SimulationProgressReporter(const StreamSinkFactoryPtr& sink_factory,
							   const string& output_prefix,
							   const string& run_hash) : sink_factory(sink_factory) {
		char summary_path[512];
		sprintf(summary_path, "%s/progress-%s.dat", output_prefix.c_str(), run_hash.c_str());
		path = string(summary_path);
	}

	inline void Add(SimulationCheckpoint checkpoint) { checkpoints.push_back(checkpoint); }
	virtual void report();
private:
	const StreamSinkFactoryPtr& sink_factory;
	boost::mutex mutex;
	string path;
	vector<SimulationCheckpoint> checkpoints;
};

#endif /* SIMULATIONPROGRESSREPORTER_H_ */
