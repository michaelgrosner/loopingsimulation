/*
 * StatusReporter.h
 *
 *  Created on: Dec 11, 2012
 *      Author: grosner
 */

#ifndef STATUSREPORTER_H_
#define STATUSREPORTER_H_

#include <boost/progress.hpp>
#include <boost/thread/mutex.hpp>

typedef uint64_t UInteger64;
typedef int64_t Seed;

namespace boost { class mutex; class progress_display; }

class StatusReporter {
public:
	StatusReporter(const UInteger64& n_configurations, const UInteger64 progress_display_interval);

	void report(const Seed& s);

private:
	mutable boost::mutex mutex;

	UInteger64 progress_display_interval;
	boost::progress_display display;
};

#endif /* STATUSREPORTER_H_ */
