/*
 * ProteinPositionsReporter.cpp
 *
 *  Created on: Dec 30, 2012
 *      Author: grosner
 */

#include "ProteinPositionsReporter.h"
#include "MutexLockGuard.h"
#include "ClosureStringGenerator.h"
#include "Closure.h"
#include <fstream>

ProteinPositionsReporter::ProteinPositionsReporter(ClosureStringGenerator& unique_id_generator,
												   const string output_prefix,
												   const string run_hash) :
		output_prefix(output_prefix), run_hash(run_hash), unique_id_generator(unique_id_generator) {
	char t[512];
	sprintf(t, "%s/protein_positions-%s.dat", output_prefix.c_str(), run_hash.c_str());
	path = string(t);

	header = "Sequence,Lac,Proteins,Orientation,Seed1,Seed2,Position,Reversed,Name";
}

void ProteinPositionsReporter::add_proteins_from_loop(const ClosurePtr& l) {
	MutexLockGuard lock_guard(mutex);
	string info;

	string loop_id = unique_id_generator.generate(l);
	for (auto pl = l->protein_locations().begin(); pl != l->protein_locations().end(); pl++) {
		char t[512];
		sprintf(t, "%s,%llu,%i,%s", loop_id.c_str(), pl->start_step(), pl->reversed(), pl->structure()->short_name().c_str());

		protein_infos.push_back(string(t));
	}
}

void ProteinPositionsReporter::report() {
	if (protein_infos.size() == 0) return;

	ofstream summary(path.c_str());

	summary << header << endl;

	for (auto it = protein_infos.begin(); it != protein_infos.end(); it++) {
		summary << *it << endl;
	}

	summary.close();
}
