/*
 * MetropolisMonteCarloEnergyReporter.h
 *
 *  Created on: May 30, 2013
 *      Author: grosner
 */

#ifndef METROPOLISMONTECARLOENERGYREPORTER_H_
#define METROPOLISMONTECARLOENERGYREPORTER_H_

#include "Includes.h"
#include "Reporter.h"
#include <string>
#include <boost/thread/mutex.hpp>
#include <vector>
#include <utility>
using namespace std;

class MetropolisMonteCarloEnergyReporter : public Reporter {
	string path;
	mutable boost::mutex mutex;
	vector<pair<Seed, Real> > seed_and_energies;
public:
	virtual ~MetropolisMonteCarloEnergyReporter() {}
	MetropolisMonteCarloEnergyReporter(const string output_prefix, const string run_hash);

	void add_seed_and_energy(const Seed s, const Real energy);
	virtual void report();
};

#endif /* METROPOLISMONTECARLOENERGYREPORTER_H_ */
