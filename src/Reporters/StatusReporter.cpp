/*
 * StatusReporter.cpp
 *
 *  Created on: Dec 11, 2012
 *      Author: grosner
 */

#include "StatusReporter.h"
#include <boost/thread/mutex.hpp>
#include "MutexLockGuard.h"

StatusReporter::StatusReporter(const UInteger64& n_configurations, const UInteger64 progress_display_interval = 500):
	progress_display_interval(progress_display_interval), display(n_configurations/progress_display_interval) {};

void StatusReporter::report(const Seed& s) {
	if (s % progress_display_interval == 0) {
		MutexLockGuard lock_guard(mutex);
		++display;
	}
}
