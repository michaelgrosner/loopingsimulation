/*
 * AlongLoopReporter.h
 *
 *  Created on: Jan 24, 2013
 *      Author: grosner
 */

#ifndef ALONGLOOPREPORTER_H_
#define ALONGLOOPREPORTER_H_

#include "Forwards.h"
#include "Includes.h"
#include "Reporter.h"
#include "BoundaryConditionCounter.h"
#include "ClosureStringGenerator.h"
#include <boost/thread/mutex.hpp>

class AlongLoopReporter : public Reporter {
public:
	AlongLoopReporter(ClosureStringGenerator& along_closure_csv_generator,
					  const Integer64 n_alongs,
					  const string output_prefix,
					  const string run_hash,
					  const BaseSequencePtr dna_sequence,
					  const Integer64 n_to_report);

	void add_loop(const ClosurePtr& l);

	virtual void report();
private:
	mutable boost::mutex mutex;

	ClosureStringGenerator& along_closure_csv_generator;
	string output_prefix;
	string run_hash;
	string header;
	vector<string> loop_infos;
	string path;
	Integer64 n_alongs;
	BaseSequencePtr dna_sequence;
	BoundaryConditionCounter boundaries_to_n_reported_loops;
};

#endif /* ALONGLOOPREPORTER_H_ */
