/*
 * JFactorReporter.cpp
 *
 *  Created on: Dec 8, 2012
 *      Author: grosner
 */

#include "JFactorReporter.h"
#include "MutexLockGuard.h"
#include "BaseSequence.h"
#include "Closure.h"
#include <fstream>
#include <iostream>
#include <boost/algorithm/string/join.hpp>

JFactorReporter::JFactorReporter(const SimulationParameters& params,
								 const BaseSequencePtr& dna_sequence,
								 SimulationProgressReporter* progress_reporter) :
			params(params), n_passed_radi(0), n_passed_gam(0), n_passed_twi(0),
			n_good_loops(0), dna_sequence(dna_sequence), progress_reporter(progress_reporter) {

	char summary_path[512];
	sprintf(summary_path, "%s/summary-%s.dat", params.output_prefix.c_str(), params.run_hash(dna_sequence).c_str());
	path = string(summary_path);
}

void JFactorReporter::loop_nears_end_point() {
	n_passed_radi++;
}

void JFactorReporter::loop_end_is_parallel() {
	n_passed_gam++;
}

void JFactorReporter::loop_is_not_overtwisted() {
	n_passed_twi++;
}

void JFactorReporter::loop_is_good(const ClosurePtr& closure) {
	MutexLockGuard lock_guard(mutex);
	number_of_valid_loops_by_orientation[closure->boundary()]++;
	//report_progress(closure);
}

void JFactorReporter::report_progress(const ClosurePtr& closure) {
	auto n = number_of_good_loops();

	if (n % 1000 == 0) {

		// can be an initializer list on gcc 4.4.5?
		vector<UInteger64> points(4);
		for (auto i = number_of_valid_loops_by_orientation.begin();
				i != number_of_valid_loops_by_orientation.end();
				i++) {
			int pt;
			if ("A1" == i->first.name()) {
				pt = 0;
			}
			else if ("A2" == i->first.name()) {
				pt = 1;
			}
			else if ("P1" == i->first.name()) {
				pt = 2;
			}
			else if ("P2" == i->first.name()) {
				pt = 3;
			}
			points[pt] = i->second;
		}

		progress_reporter->Add(
				SimulationCheckpoint(closure->second_half().seed(),
									 n,
									 points,
									 n_loop_nears_end_point(),
									 n_loop_end_is_parallel(),
									 n_loop_is_not_overtwisted()));
	}

}

UInteger64 JFactorReporter::number_of_good_loops_by_boundary(const BoundaryCondition& boundary) const {
	MutexLockGuard lock_guard(mutex);
	return number_of_valid_loops_by_orientation.at(boundary);
}

double JFactorReporter::current_J() {
	auto radi_cubed = params.radi*params.radi*params.radi;
	auto n_configurations_squared = (Real)params.n_configurations*(Real)params.n_configurations;
	auto pre_wr = (params.lac_type == FIXED or params.lac_type == MINICIRCLE ? 2.0 : 1.0);
	auto wr  = pre_wr*(Real)n_passed_radi/(n_configurations_squared*4.0*M_PI*radi_cubed/3.0);
	auto wtr = (Real)n_passed_gam/((1.0-params.gam)*(Real)n_passed_radi);
	auto wtw = (Real)number_of_good_loops()*360.0/(4.0*params.twi*M_PI*(Real)n_passed_gam);
	auto J   = 4.0*M_PI*10000.0*wr*wtr*wtw/6.022;
	if (isnan(J)) J = 0.0;
	return J;
}

UInteger64 JFactorReporter::number_of_good_loops() const {
	auto total_number_of_loops = 0;
	for (auto named_boundary_counts_it = number_of_valid_loops_by_orientation.begin();
                named_boundary_counts_it != number_of_valid_loops_by_orientation.end();
                named_boundary_counts_it++) {
		total_number_of_loops += named_boundary_counts_it->second;
	}
	return total_number_of_loops;
}

void JFactorReporter::report() {
	MutexLockGuard lock(mutex);
	ofstream summary(path.c_str());

	summary << "Sequence,Lac,Proteins,Orientation,NLoops,JFactor" << endl;

	auto total_number_of_loops = number_of_good_loops();

	auto J = current_J();
	cout << "J   = " << J << endl;

	for (auto named_boundary_counts_it = number_of_valid_loops_by_orientation.begin();
                named_boundary_counts_it != number_of_valid_loops_by_orientation.end();
                named_boundary_counts_it++) {

		auto J_boundary = J*named_boundary_counts_it->second/Real(total_number_of_loops);

		char record_line[512];
		sprintf(record_line, "%s,%s,%s,%2s,%llu,%le", dna_sequence->name().c_str(),
													  params.lac_string.c_str(),
													  params.proteins_configuration_string.c_str(),
													  named_boundary_counts_it->first.name().c_str(),
													  named_boundary_counts_it->second,
													  J_boundary);
		summary << record_line << endl;
	}

	summary.close();
}
