/*
 * JFactorReporter.h
 *
 *  Created on: Dec 8, 2012
 *      Author: grosner
 */

#ifndef JFACTORREPORTER_H_
#define JFACTORREPORTER_H_

#include "Includes.h"
#include "Reporter.h"
#include "SimulationParameters.h"
#include "BoundaryConditionCounter.h"
#include "Forwards.h"
#include "Atomic.h"
#include "SimulationProgressReporter.h"
#include <boost/thread/mutex.hpp>

typedef map<int, map<string, map<int, int> > > FuckinThing;

struct IHatePhDWork {
	FuckinThing twists;
	FuckinThing writhes;
	FuckinThing energies;
};

using namespace std;

class BaseJFactorReporter : public Reporter {
public:
	virtual void loop_nears_end_point() = 0;
	virtual void loop_end_is_parallel() = 0;
	virtual void loop_is_not_overtwisted() = 0;
	virtual void loop_is_good(const ClosurePtr& closure) = 0;

	virtual UInteger64 n_loop_nears_end_point() const = 0;
	virtual UInteger64 n_loop_end_is_parallel() const = 0;
	virtual UInteger64 n_loop_is_not_overtwisted() const = 0;
	inline virtual BoundaryConditionCounter number_of_loops_by_orientation() const = 0;

	virtual double current_J() = 0;
	virtual void report() = 0;
	virtual UInteger64 number_of_good_loops() const = 0;
};

class JFactorReporter : public BaseJFactorReporter {
public:
	virtual ~JFactorReporter() {}
	JFactorReporter(const SimulationParameters& params,
					const BaseSequencePtr& dna_sequence,
					SimulationProgressReporter* progress_reporter);

	virtual void loop_nears_end_point();
	virtual void loop_end_is_parallel();
	virtual void loop_is_not_overtwisted();
	virtual double current_J();
	virtual void loop_is_good(const ClosurePtr& closure);

	inline virtual UInteger64 n_loop_nears_end_point() const { return n_passed_radi; }
	inline virtual UInteger64 n_loop_end_is_parallel() const { return n_passed_gam; }
	inline virtual UInteger64 n_loop_is_not_overtwisted() const { return n_passed_twi; }
	inline virtual BoundaryConditionCounter number_of_loops_by_orientation() const { return number_of_valid_loops_by_orientation; }

	virtual UInteger64 number_of_good_loops() const;
	UInteger64 number_of_good_loops_by_boundary(const BoundaryCondition& boundary) const;

	virtual void report();

private:
	mutable boost::mutex mutex;

	boost_or_std::atomic<UInteger64> n_passed_radi;
	boost_or_std::atomic<UInteger64> n_passed_twi;
	boost_or_std::atomic<UInteger64> n_passed_gam;
	boost_or_std::atomic<UInteger64> n_good_loops;

	BoundaryConditionCounter number_of_valid_loops_by_orientation;
	void report_progress(const ClosurePtr& closure);

	string path;
	SimulationParameters params;
	SimulationProgressReporter* progress_reporter;
	BaseSequencePtr dna_sequence;
	IHatePhDWork ass_butt;
};

#endif /* JFACTORREPORTER_H_ */
