/*
 * AlongLoopReporter.cpp
 *
 *  Created on: Jan 24, 2013
 *      Author: grosner
 */

#include "AlongLoopReporter.h"
#include "MutexLockGuard.h"
#include "Closure.h"
#include <boost/algorithm/string/join.hpp>
#include "BaseSequence.h"

AlongLoopReporter::AlongLoopReporter(ClosureStringGenerator& along_closure_csv_generator,
									 const Integer64 n_alongs,
									 const string output_prefix,
									 const string run_hash,
									 const BaseSequencePtr dna_sequence,
									 const Integer64 n_to_report) : along_closure_csv_generator(along_closure_csv_generator),
																	n_alongs(n_alongs),
																	output_prefix(output_prefix),
																	run_hash(run_hash),
																	dna_sequence(dna_sequence) {

	char summary_path[512];
	sprintf(summary_path, "%s/along-%s.dat", output_prefix.c_str(), run_hash.c_str());
	path = string(summary_path);

	// TODO: Why 14+2? wont work with MC.
	vector<string> step_numbers_as_strings; step_numbers_as_strings.reserve(dna_sequence->n_steps()+1);
	for (unsigned int i = 0; i < dna_sequence->n_steps()+16; i++)
		step_numbers_as_strings.push_back(boost::lexical_cast<string>(i));
	header = "Sequence,Lac,Proteins,Orientation,Seed1,Seed2,Attr," + boost::algorithm::join(step_numbers_as_strings, ",");
}

void AlongLoopReporter::add_loop(const ClosurePtr& l) {
	string info = along_closure_csv_generator.generate(l);

	MutexLockGuard lock_guard(mutex);
	if (boundaries_to_n_reported_loops[l->boundary()] < n_alongs or n_alongs < 0) {
		loop_infos.push_back(info);
		boundaries_to_n_reported_loops[l->boundary()]++;
	}
}

void AlongLoopReporter::report() {

	MutexLockGuard lock_guard(mutex);

	ofstream summary(path.c_str());
	summary << header << endl;

	for (auto it = loop_infos.begin(); it != loop_infos.end(); it++) {
		summary << *it;
	}

	summary.close();
}
