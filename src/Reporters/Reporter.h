/*
 * Reporter.h
 *
 *  Created on: May 1, 2013
 *      Author: grosner
 */

#ifndef REPORTER_H_
#define REPORTER_H_

class Reporter {
public:
	virtual ~Reporter() {};
	virtual void report() = 0;
};

#endif /* REPORTER_H_ */
