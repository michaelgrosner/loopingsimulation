/*
 * SimulationProgressReporter.cpp
 *
 *  Created on: Dec 12, 2013
 *      Author: grosner
 */

#include "SimulationProgressReporter.h"
#include "MutexLockGuard.h"
#include <iostream>

void SimulationProgressReporter::report() {
	MutexLockGuard lock(mutex);

	StreamSinkPtr sink = sink_factory->create_sink_with_path(path);

	sink->add_line("NHalfchains,NLoops,NPRadi,NPGam,NPTwi,A1,A2,P1,P2\n");

	for (auto it = checkpoints.begin(); it != checkpoints.end(); it++) {
		char buf[512];
		sprintf(buf, "%llu,%llu,%llu,%llu,%llu,%llu,%llu,%llu,%llu\n", it->n_halfchains, it->n_generated_loops,
				it->n_passed_radi, it->n_passed_gam, it->n_passed_twi,
				it->boundary_counts[0], it->boundary_counts[1],
				it->boundary_counts[2], it->boundary_counts[3]);
		sink->add_line(string(buf));
	}
	sink->flush();
}
