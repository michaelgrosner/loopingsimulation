/*
 * MetropolisMonteCarloEnergyReporter.cpp
 *
 *  Created on: May 30, 2013
 *      Author: grosner
 */

#include "MetropolisMonteCarloEnergyReporter.h"
#include "MutexLockGuard.h"
#include <fstream>
#include <iostream>

MetropolisMonteCarloEnergyReporter::MetropolisMonteCarloEnergyReporter(const string output_prefix, const string run_hash)  {
	char summary_path[512];
	sprintf(summary_path, "%s/metropolis-%s.dat", output_prefix.c_str(), run_hash.c_str());
	path = string(summary_path);
}

void MetropolisMonteCarloEnergyReporter::add_seed_and_energy(const Seed s, const Real energy) {
	MutexLockGuard lock(mutex);
	seed_and_energies.push_back(make_pair(s,energy));
}

void MetropolisMonteCarloEnergyReporter::report() {
	ofstream summary(path.c_str());

	summary << "Seed,Energy" << endl;
	for (auto it = seed_and_energies.begin(); it != seed_and_energies.end(); it++) {
		summary << it->first << "," << it->second << endl;
	}

	summary.close();
}
