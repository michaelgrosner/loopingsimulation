/*
 * LoopReporter.cpp
 *
 *  Created on: Jan 6, 2013
 *      Author: grosner
 */

#include "LoopReporter.h"
#include "MutexLockGuard.h"
#include "Closure.h"

LoopReporter::LoopReporter(const ClosureStringGenerator& csv_generator,
						   const Integer64 n_loops,
						   const string output_prefix,
						   const string run_hash) : csv_generator(csv_generator),
						   	   	   	   	   	   	    n_loops(n_loops),
						   	   	   	   	   	   	    output_prefix(output_prefix),
						   	   	   	   	   	   	    run_hash(run_hash) {

	char summary_path[512];
	sprintf(summary_path, "%s/loops-%s.dat", output_prefix.c_str(), run_hash.c_str());
	path = string(summary_path);

	header = "Sequence,Lac,Proteins,Orientation,Seed1,Seed2,Energy,nProteins,Tw,Wr,Lk,Angle";
}

bool LoopReporter::add_loop(const ClosurePtr& l) {
	bool loop_has_been_added = false;

	string info = csv_generator.generate(l);

	MutexLockGuard lock_guard(mutex);
	if (boundaries_to_n_reported_loops[l->boundary()] < n_loops or n_loops < 0) {
		loop_infos.push_back(info);
		boundaries_to_n_reported_loops[l->boundary()]++;
		loop_has_been_added = true;
	}

	return loop_has_been_added;
}

void LoopReporter::report() {
	ofstream summary(path.c_str());

	summary << header << endl;

	for (auto it = loop_infos.begin(); it != loop_infos.end(); it++) {
		summary << *it << endl;
	}

	summary.close();
}
