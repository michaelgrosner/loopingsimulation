/*
 * Math.cpp
 *
 *  Created on: Dec 7, 2012
 *      Author: grosner
 */

#include "BasePairAndStep.h"
#include <DNASim/dsMatrix4.h>

BasePair step_to_bp(const StepParameters& tp) {

	double gamma, phi, omega,
	sp, cp, sm, cm, sg, cg,
	t1, t2, t3;
	BasePair M;

	t1 = tp[0]*M_PI/180.0;
	t2 = tp[1]*M_PI/180.0;
	t3 = tp[2]*M_PI/180.0;

	gamma = sqrt(t1*t1+t2*t2);
	phi = atan2(t1,t2);
	omega = t3;

	sp = sin(omega/2+phi); cp = cos(omega/2+phi); sm = sin(omega/2-phi);
	cm = cos(omega/2-phi); sg = sin(gamma); cg = cos(gamma);

	M(0,0) = cm*cg*cp-sm*sp;
	M(0,1) = -cm*cg*sp-sm*cp;
	M(0,2) = cm*sg;
	M(1,0) = sm*cg*cp+cm*sp;
	M(1,1) = -sm*cg*sp+cm*cp;
	M(1,2) = sm*sg;
	M(2,0) = -sg*cp;
	M(2,1) = sg*sp;
	M(2,2) = cg;
	M(3,0) = 0;
	M(3,1) = 0;
	M(3,2) = 0;
	M(3,3) = 1;

	sp = sin(phi); cp = cos(phi); sg = sin(gamma/2); cg = cos(gamma/2);

	M(0,3) = tp[3]*(cm*cg*cp-sm*sp) + tp[4]*(-cm*cg*sp-sm*cp) + tp[5]*(cm*sg);
	M(1,3) = tp[3]*(sm*cg*cp+cm*sp) + tp[4]*(-sm*cg*sp+cm*cp) + tp[5]*(sm*sg);
	M(2,3) = tp[3]*(-sg*cp) +         tp[4]*(sg*sp) +           tp[5]*(cg);

	return M;

}

StepParameters bp_to_step(const BasePair& W) {

	StepParameters M;
	double cosgamma, gamma, phi, omega, sgcp, omega2_minus_phi,
	sm, cm, sp, cp, sg, cg;

	cosgamma = W(2,2);
	if (cosgamma > 1.0) cosgamma = 1.0;
	else if (cosgamma <= -1.0) cosgamma = -1.0;

	gamma = acos(cosgamma);

	sgcp = W(1,1)*W(0,2)-W(0,1)*W(1,2);

	if (gamma == 0.0) omega = -atan2(W(0,1),W(1,1));
	else omega = atan2((W(2,1)*W(0,2)+sgcp*W(1,2)),(sgcp*W(0,2)-W(2,1)*W(1,2)));

	omega2_minus_phi = atan2(W(1,2),W(0,2));

	phi = omega/2.0 - omega2_minus_phi;

	M[0] = gamma*sin(phi)*180.0/M_PI;
	M[1] = gamma*cos(phi)*180.0/M_PI;
	M[2] = omega*180.0/M_PI;

	sm = sin(omega/2.0-phi);
	cm = cos(omega/2.0-phi);
	sp = sin(phi);
	cp = cos(phi);
	sg = sin(gamma/2.0);
	cg = cos(gamma/2.0);

	M[3] = (cm*cg*cp-sm*sp)* W(0,3)+ (sm*cg*cp+cm*sp)*W(1,3)-sg*cp*W(2,3);
	M[4] = (-cm*cg*sp-sm*cp)*W(0,3)+(-sm*cg*sp+cm*cp)*W(1,3)+sg*sp*W(2,3);
	M[5] =           (cm*sg)*W(0,3)+          (sm*sg)*W(1,3)+   cg*W(2,3);

	return M;

}

DNASim::dsMatrix4 bp_to_dsMatrix4(const BasePair& bp) {
    DNASim::dsMatrix4 ds;
    for (unsigned int i = 0; i < 4; i++)
        for (unsigned int j = 0; j < 4; j++)
            ds(i,j) = bp(i,j);
    return ds;
}

BasePair dsMatrix4_to_bp(const DNASim::dsMatrix4 m) {
	BasePair bp;
    for (unsigned int i = 0; i < 4; i++)
        for (unsigned int j = 0; j < 4; j++)
            bp(i,j) = m(i,j);
    return bp;
}
