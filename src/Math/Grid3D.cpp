/*
 * Grid3D.cpp
 *
 *  Created on: Dec 8, 2012
 *      Author: grosner
 */

#include "Grid3D.h"
#include "BaseSequence.h"
#include "MutexLockGuard.h"

Grid3D::Grid3D(const SimulationParameters& params) :
		params(params), terminal_steps(params.n_configurations, 6), epsilon(5) {

	if (params.proteins_configuration.size() > 0)
		first_half_overhang_cache.resize(params.n_configurations, 0);

	dis = Integer64(params.radi/epsilon);
	grid.set_empty_key(GridPoint(99999999, 99999999, 99999999));
	grid.resize(params.n_configurations);
}

void Grid3D::add_halfchain_to_grid(const HalfChain& hc) {
	auto point = -1.0 * hc.bp().topLeftCorner<3,3>().transpose() * hc.bp().topRightCorner<3,1>();
	GridPoint gp(point[0]/epsilon, point[1]/epsilon, point[2]/epsilon);

	auto adj_seed = hc.seed() - params.seed;
	terminal_steps.row(adj_seed) = bp_to_step(hc.bp());

	if (first_half_overhang_cache.size() > 0)
		first_half_overhang_cache[adj_seed] = hc.overhang_code();

	MutexLockGuard lock_guard(mutex);
	grid[gp].push_back(hc.seed());
}

vector<Seed> Grid3D::find_neighbors_to_point(const Vector3d& point) const {
	vector<Seed> matches;

	auto x = point[0]/epsilon;
	auto y = point[1]/epsilon;
	auto z = point[2]/epsilon;

	for (auto a = x - dis; a < x + dis; a++) {
		auto ax = (a-x)*(a-x);
		for (auto b = y - dis; b < y + dis; b++) {
			auto by = (b-y)*(b-y);
			for (auto c = z - dis; c < z + dis; c++) {
				auto cz = (c-z)*(c-z);
				if ((ax+by+cz)*epsilon*epsilon > params.radi_squared()) continue;

				GridPoint bin(a, b, c);
				auto gm = grid.find(bin);

				if (gm == grid.end()) continue;

				for (auto idx = gm->second.begin(); idx != gm->second.end(); idx++) {
					matches.push_back(*idx);
				}

			}
		}
	}

	return matches;
}

HalfChain Grid3D::rebuild_halfchain_from_seed(const Seed& s) const {
	auto adj_seed = s - params.seed;
	auto bp = step_to_bp(terminal_steps.row(adj_seed));
	auto overhang_code = (first_half_overhang_cache.size() > 0 ? first_half_overhang_cache[adj_seed] : 0);
	return HalfChain(bp, LocationCollection(), s, overhang_code);
}

bool Grid3D::is_seed_in_overhang_cache(const Seed& s) const {
	if (params.proteins_configuration.size() > 0)
		return true;
	else
		return false;
}
