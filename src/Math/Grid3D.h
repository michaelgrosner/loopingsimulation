/*
 * Grid3D.h
 *
 *  Created on: Dec 8, 2012
 *      Author: grosner
 */

#ifndef GRID3D_H_
#define GRID3D_H_

#include "Includes.h"
#include "HalfChainStorage.h"
#include "BasePairAndStep.h"
#include "SimulationParameters.h"
#include <vector>
#include <google/dense_hash_map>
#include <boost/thread/mutex.hpp>
#include <boost/functional/hash.hpp>

struct GridPoint {
	GridPoint() {}
	GridPoint(Integer64 a, Integer64 b, Integer64 c) : a(a), b(b), c(c) {};
	Integer64 a, b, c;
};

struct GridPointHash : std::unary_function<GridPoint, std::size_t> {
    std::size_t operator()(GridPoint const& e) const {
        std::size_t seed = 0;
        boost::hash_combine(seed, e.a);
        boost::hash_combine(seed, e.b);
        boost::hash_combine(seed, e.c);
        return seed;
    }
};

struct GridPointEquals : std::binary_function<GridPoint, GridPoint, bool> {
    bool operator()(GridPoint const& x, GridPoint const& y) const {
        return (x.a==y.a &&
                x.b==y.b &&
                x.c==y.c);
    }
};

typedef google::dense_hash_map<GridPoint, vector<Seed>, GridPointHash, GridPointEquals> GridMap;

class Grid3D : public HalfChainStorage {
public:
	Grid3D() {};
	Grid3D(const SimulationParameters& params);
	virtual ~Grid3D() {};

	virtual void add_halfchain_to_grid(const HalfChain& hc);
	virtual vector<Seed> find_neighbors_to_point(const Vector3d& point) const;
	virtual HalfChain rebuild_halfchain_from_seed(const Seed& s) const;
	virtual void finalize() {};

private:
	mutable boost::mutex mutex;
	Grid3D(const Grid3D& other);
	bool is_seed_in_overhang_cache(const Seed& s) const;

	Integer64 dis;
	GridMap grid;
	Integer64 epsilon;

	BaseSequencePtr dna_sequence;
	SimulationParameters params;
	RowMajorDynamicMatrix terminal_steps;
	vector<OverhangCode> first_half_overhang_cache;

};

#endif /* GRID3D_H_ */
