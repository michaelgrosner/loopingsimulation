/*
 * Math.h
 *
 *  Created on: Dec 2, 2012
 *      Author: grosner
 */

#ifndef BPANDSTEP_H_
#define BPANDSTEP_H_

#include <Eigen/Dense>
#include <Eigen/StdVector>
#include <vector>

using namespace std;
using namespace Eigen;

namespace DNASim {
    class dsMatrix4;
}

typedef double   Real;
typedef Matrix<Real,4,4> BasePair;
typedef Matrix<Real,6,1> StepParameters;
typedef Matrix<Real,6,6> SixBySix;
typedef vector<BasePair, aligned_allocator<BasePair> > BasePairVector;
typedef vector<StepParameters, aligned_allocator<StepParameters> > StepParametersVector;
typedef vector<SixBySix, aligned_allocator<SixBySix> > SixBySixVector;
typedef Matrix<Real,Dynamic,Dynamic,RowMajor> RowMajorDynamicMatrix;
typedef Eigen::Matrix<Real, Eigen::Dynamic, 3> CAlphasMatrix;

BasePair step_to_bp(const StepParameters& tp);
StepParameters bp_to_step(const BasePair& W);

DNASim::dsMatrix4 bp_to_dsMatrix4(const BasePair& bp);
BasePair dsMatrix4_to_bp(const DNASim::dsMatrix4 m);

#endif /* BPANDSTEP_H_ */
