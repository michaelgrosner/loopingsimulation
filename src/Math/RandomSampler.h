/*
 * RandomSampler.h
 *
 *  Created on: Dec 7, 2012
 *      Author: grosner
 */

#ifndef RandomSampler_H_
#define RandomSampler_H_

typedef double Real;

#include <boost/random.hpp>
#include <boost/random/normal_distribution.hpp>

typedef boost::mt19937 Twister;
typedef boost::uniform_real<> UniformDistribution;
typedef boost::normal_distribution<> NormalDistribution;
typedef boost::variate_generator<Twister&, NormalDistribution> NormalVariateGenerator;
typedef boost::variate_generator<Twister&, UniformDistribution> UniformVariateGenerator;

class RandomSampler {
public:
	RandomSampler() : t(0), nd(0.0, 1.0), ud(0.0, 1.0), uvg(t, ud), nvg(t, nd) {}

	void reseed(const Seed& s) {
		uvg.engine().seed(s);
		uvg.distribution().reset();
		nvg.distribution().reset();
	}

	inline Real sample_normal()  { return nvg(); }
	inline Real sample_uniform() { return uvg(); }

private:
	Twister t;
	NormalDistribution  nd;
	UniformDistribution ud;
	NormalVariateGenerator  nvg;
	UniformVariateGenerator uvg;
};

#endif /* RandomSampler_H_ */
