/*
 * HalfChainStorage.h
 *
 *  Created on: May 5, 2013
 *      Author: grosner
 */

#ifndef HALFCHAINSTORAGE_H_
#define HALFCHAINSTORAGE_H_

#include "Includes.h"
#include "BasePairAndStep.h"
#include "HalfChain.h"

class HalfChainStorage {
public:
	virtual ~HalfChainStorage() {};
	virtual void add_halfchain_to_grid(const HalfChain& hc) = 0;
	virtual vector<Seed> find_neighbors_to_point(const Vector3d& point) const = 0;
	virtual HalfChain rebuild_halfchain_from_seed(const Seed& s) const = 0;
	virtual void finalize() = 0;
};

#endif /* HALFCHAINSTORAGE_H_ */
