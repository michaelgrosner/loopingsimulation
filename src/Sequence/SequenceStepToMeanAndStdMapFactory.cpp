/*
 * SequenceStepToMeanAndStdMapFactory.cpp
 *
 *  Created on: Jan 4, 2013
 *      Author: grosner
 */

#include "SequenceStepToMeanAndStdMapFactory.h"
#include <boost/tokenizer.hpp>
#include <Eigen/Eigenvalues>
#include <boost/algorithm/string.hpp>
#include <fstream>
#include <boost/lexical_cast.hpp>

SequenceStepToMeanAndStdMapFactory::SequenceStepToMeanAndStdMapFactory(const string& params_filepath) :
	params_filepath(params_filepath) {}

pair<SequenceStep, MeanStdCouplingEnergy> SequenceStepToMeanAndStdMapFactory::parse_sequence_and_step(const string& line) {
	boost::char_separator<char> colon_seperator(";");
	boost::tokenizer<boost::char_separator<char> > line_tokens(line, colon_seperator);

	vector<string> tokens(line_tokens.begin(), line_tokens.end());

	SequenceStep ss(tokens[0][0], tokens[1][0]);
	auto means = parse_step(tokens[2]);

	auto forces = parse_forces(tokens[3]);
	EigenSolver<SixBySix> solver(forces);

	auto stds = solver.eigenvalues().real().cwiseAbs().cwiseSqrt().cwiseInverse();
	auto couplings = solver.eigenvectors().real();

	// HACK: This should not be happening... something is wrong with ordering/signs of returned eigenvalues.
	if (!couplings.isDiagonal())
		couplings.block(1,1,2,2) *= -1.0;

	MeanStdCouplingEnergy msp(means, stds, couplings, forces);

	return make_pair(ss, msp);
}

SixBySix SequenceStepToMeanAndStdMapFactory::parse_forces(const string& item) {
	boost::char_separator<char> space_seperator(" ");
	boost::tokenizer<boost::char_separator<char> > item_tokens(item, space_seperator);

	Real params[6*6];
	int i = 0;
	for (auto b = item_tokens.begin(); b != item_tokens.end(); b++) {
		params[i] = boost::lexical_cast<Real>(*b);
		i++;
	}
	return SixBySix(params);
}

StepParameters SequenceStepToMeanAndStdMapFactory::parse_step(const string& item) {
	boost::char_separator<char> space_seperator(" ");
	boost::tokenizer<boost::char_separator<char> > item_tokens(item, space_seperator);

	Real params[6];
	int i = 0;
	for (auto b = item_tokens.begin(); b != item_tokens.end(); b++) {
		params[i] = boost::lexical_cast<Real>(*b);
		i++;
	}
	return StepParameters(params);
}

SequenceStepToMeanAndStdMap SequenceStepToMeanAndStdMapFactory::build() {
	SequenceStepToMeanAndStdMap map;
	ifstream f(params_filepath.c_str());
	while (f.good()) {
		string line;
		getline(f, line);

		// Handle comments or blank lines
		if (boost::algorithm::starts_with(line, "#") or line.empty()) continue;

		map.insert(parse_sequence_and_step(line));
	}
	return map;
}
