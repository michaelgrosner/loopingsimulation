/*
 * SequenceFactory.cpp
 *
 *  Created on: Jan 4, 2013
 *      Author: grosner
 */

#include "SequenceFactory.h"
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include "BaseSequence.h"
#include <iostream>

BaseSequencePtr SequenceFactory::build(const string& config) {
	BaseSequencePtr dna_sequence;

	string name_str;
	string sequence_str;
	vector<SequenceStep> sequence_steps;
	StepParametersVector means_by_step;
	StepParametersVector stds_by_step;
	SixBySixVector couplings_by_step;
	SixBySixVector forces_by_step;
	SequenceStepToMeanAndStdMap mapper;

	if (boost::algorithm::starts_with(config, "Seq+")) {
		vector<string> tokens;
		boost::algorithm::split(tokens, config, boost::is_any_of("+"));

		name_str = tokens[1];
		sequence_str = tokens[2];

		mapper = sampling_parameters_provider->build();

		for (UInteger64 i = 1; i < sequence_str.size(); i++) {
			SequenceStep ss(sequence_str[i-1], sequence_str[i]);
			sequence_steps.push_back(ss);
			if (mapper.find(ss) != mapper.end()) {
				MeanStdCouplingEnergy mscf = mapper[ss];
				means_by_step.push_back(mscf.get<0>());
				stds_by_step.push_back(mscf.get<1>());
				couplings_by_step.push_back(mscf.get<2>());
				forces_by_step.push_back(mscf.get<3>());
			}
			else {
				cout << "Illegal sequence characters: "  << ss.first << " or " << ss.second << endl;
				throw 33;
			}
		}
		dna_sequence = BaseSequencePtr(new BaseSequence(sequence_str, name_str, means_by_step, stds_by_step, couplings_by_step, forces_by_step, mapper));
	}
	else if (boost::algorithm::starts_with(config, "Ideal+")) {
		UInteger64 n_steps = boost::lexical_cast<UInteger64>(config.substr(6,config.size()-1));

		dna_sequence = BaseSequencePtr(new BaseSequence(n_steps));
	}
	else {
		cout << "Invalid format for Sequence specification" << endl;
		throw 4;
	}

	cout << "Sequence Length: " << dna_sequence->n_steps() << endl;
	cout << "Sequence Name: " << dna_sequence->name() << endl;

	return dna_sequence;
}
