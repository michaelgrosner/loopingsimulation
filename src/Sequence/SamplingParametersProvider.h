/*
 * SamplingParametersProvider.h
 *
 *  Created on: Feb 20, 2013
 *      Author: grosner
 */

#ifndef SAMPLINGPARAMETERSPROVIDER_H_
#define SAMPLINGPARAMETERSPROVIDER_H_

#include "Includes.h"
#include "BasePairAndStep.h"
#include <boost/tuple/tuple.hpp>
#include <utility>
#include <map>

typedef boost::tuples::tuple<StepParameters, StepParameters, SixBySix, SixBySix> MeanStdCouplingEnergy;

typedef pair<char,char> SequenceStep;
typedef map<SequenceStep, MeanStdCouplingEnergy> SequenceStepToMeanAndStdMap;

class SamplingParametersProvider {
public:
	virtual ~SamplingParametersProvider() {};
	virtual SequenceStepToMeanAndStdMap build() = 0;
};

class NullSamplingParametersProvider : public SamplingParametersProvider {
public:
	virtual ~NullSamplingParametersProvider() {}
	virtual SequenceStepToMeanAndStdMap build() { return SequenceStepToMeanAndStdMap(); }
};


#endif /* SAMPLINGPARAMETERSPROVIDER_H_ */
