/*
 * SequenceStepToMeanAndStdMapFactory.h
 *
 *  Created on: Jan 4, 2013
 *      Author: grosner
 */

#ifndef SEQUENCESTEPTOMEANANDSTDMAPFACTORY_H_
#define SEQUENCESTEPTOMEANANDSTDMAPFACTORY_H_

#include "SamplingParametersProvider.h"

class SequenceStepToMeanAndStdMapFactory : public SamplingParametersProvider {
public:
	SequenceStepToMeanAndStdMapFactory(const string& params_filepath);

	StepParameters parse_step(const string& item);
	SixBySix parse_forces(const string& item);

	pair<SequenceStep, MeanStdCouplingEnergy> parse_sequence_and_step(const string& line);
	SequenceStepToMeanAndStdMap build();
private:
	string params_filepath;
};

#endif /* SEQUENCESTEPTOMEANANDSTDMAPFACTORY_H_ */
