/*
 * SequenceFactory.h
 *
 *  Created on: Jan 4, 2013
 *      Author: grosner
 */

#ifndef SEQUENCEFACTORY_H_
#define SEQUENCEFACTORY_H_

#include <string>
#include "BaseSequence.h"
#include "SamplingParametersProvider.h"

typedef boost::shared_ptr<SamplingParametersProvider> SamplingParametersProviderPtr;

using namespace std;

class BaseSequenceFactory {
public:
	virtual ~BaseSequenceFactory() {}
	virtual BaseSequencePtr build(const string& config) = 0;
};

class SequenceFactory : public BaseSequenceFactory {
public:
	SequenceFactory(SamplingParametersProviderPtr& sampling_parameters_provider) :
		sampling_parameters_provider(sampling_parameters_provider)  {};
	virtual ~SequenceFactory() {}

	virtual BaseSequencePtr build(const string& config);

private:
	SamplingParametersProviderPtr& sampling_parameters_provider;
};

#endif /* SEQUENCEFACTORY_H_ */
