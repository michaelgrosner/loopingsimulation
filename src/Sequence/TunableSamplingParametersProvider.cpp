/*
 * TunableSamplingParametersProvider.cpp
 *
 *  Created on: Feb 20, 2013
 *      Author: grosner
 */

#include "TunableSamplingParametersProvider.h"

TunableSamplingParametersProvider::TunableSamplingParametersProvider(const StepParameters& mean, const Real& e) :
		mean(mean), e(e) {
}

SequenceStepToMeanAndStdMap TunableSamplingParametersProvider::build() {
	SequenceStepToMeanAndStdMap map;

	Real A = pow(e, -2.0);
	Real C = 1.4*A;
	SixBySix f = SixBySix::Zero();
	StepParameters diag;
	diag << A, A, C, 50, 50, 50;
	f.diagonal() = diag;
	f(1,2) = pow(5.41, -2.0);
	f(2,1) = pow(5.41, -2.0);

	EigenSolver<SixBySix> solver(f, true);
	StepParameters stds = solver.eigenvalues().real().cwiseAbs().cwiseSqrt().cwiseInverse();
	SixBySix couplings = solver.eigenvectors().real();

	// HACK: This should not be happening... something is wrong with ordering/signs of returned eigenvalues.
	if (!couplings.isDiagonal())
		couplings.block(1,1,2,2) *= -1.0;

	const char bases[] = {'A', 'C', 'T', 'G', 'Z'};
	for (unsigned int i = 0; i < 5; i++) {
		for (unsigned int j = 0; j < 5; j++) {
			SequenceStep ss(bases[i], bases[j]);
			MeanStdCouplingEnergy msce(mean, stds, couplings, f);
			map.insert(make_pair(ss, msce));
		}
	}
	return map;
}
