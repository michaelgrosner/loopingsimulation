/*
 * DependentSequence.h
 *
 *  Created on: Jan 4, 2013
 *      Author: grosner
 */

#ifndef DEPENDENTSEQUENCE_H_
#define DEPENDENTSEQUENCE_H_

#include "Includes.h"
#include "BasePairAndStep.h"
#include "SequenceStepToMeanAndStdMapFactory.h"
#include <boost/shared_ptr.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

class BaseSequence {
	string sequence_str, name_str;
	StepParametersVector means_by_step;
	StepParametersVector stds_by_step;
	SixBySixVector couplings_by_step;
	SixBySixVector forces_by_step;
	SequenceStepToMeanAndStdMap mapper;
public:
	BaseSequence(const string sequence_str,
	             const string name,
	             const StepParametersVector means_by_step,
	             const StepParametersVector stds_by_step,
	             const SixBySixVector couplings_by_step,
	             const SixBySixVector forces_by_step,
	             const SequenceStepToMeanAndStdMap mapper) : sequence_str(sequence_str),
				   	 	  	  	  	  	  	  	  	  	     name_str(name),
				   	 	  	  	  	  	  	  	  	  	     means_by_step(means_by_step),
				   	 	  	  	  	  	  	  	  	  	     stds_by_step(stds_by_step),
				   	 	  	  	  	  	  	  	  	  	     couplings_by_step(couplings_by_step),
				   	 	  	  	  	  	  	  	  	  	     forces_by_step(forces_by_step),
				   	 	  	  	  	  	  	  	  	  	     mapper(mapper) {}

	// Ideal sequence constructor.
	BaseSequence(const UInteger64 n_steps) {
		StepParameters means, stds;
		SixBySix coupling, force;

		means << 0, 0, 34.28, 0, 0, 3.4;
		means_by_step = StepParametersVector(n_steps, means);

		stds  << 4.83934, 4.83934, 4.09273, 0.141421, 0.141421, 0.141421;
		stds_by_step = StepParametersVector(n_steps, stds);

		force << 0.0427,      0,      0,      0,      0,      0,
	                  0, 0.0427,      0,      0,      0,      0,
	                  0,      0, 0.0597,      0,      0,      0,
	                  0,      0,      0,     50,      0,      0,
	                  0,      0,      0,      0,     50,      0,
	                  0,      0,      0,      0,      0,     50;
		forces_by_step = SixBySixVector(n_steps, force);

		coupling = SixBySix::Identity();
		couplings_by_step = SixBySixVector(n_steps, coupling);

		sequence_str = string('Z', n_steps);
		name_str = boost::lexical_cast<string>(n_steps);
	};

	inline const StepParameters& mean(const UInteger64& s) const      { return means_by_step[s]; }
	inline const StepParameters& std(const UInteger64& s) const       { return stds_by_step[s]; }
	inline const SixBySix& coupling_matrix(const UInteger64& s) const { return couplings_by_step[s]; }
	inline const SixBySix& force_constant(const UInteger64& s) const  { return forces_by_step[s]; }
	inline UInteger64 n_steps() const           { return means_by_step.size(); }
	inline UInteger64 first_half_length() const { return means_by_step.size()/2; }
	inline UInteger64 second_half_length() const{ return means_by_step.size() - means_by_step.size() / 2; }
	inline const string& sequence() const { return sequence_str; }
	inline const string& name() const     { return name_str; }
};

typedef boost::shared_ptr<BaseSequence> BaseSequencePtr;

#endif /* DEPENDENTSEQUENCE_H_ */
