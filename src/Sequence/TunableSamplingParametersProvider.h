/*
 * TunableSamplingParametersProvider.h
 *
 *  Created on: Feb 20, 2013
 *      Author: grosner
 */

#ifndef TUNABLESAMPLINGPARAMETERSPROVIDER_H_
#define TUNABLESAMPLINGPARAMETERSPROVIDER_H_

#include "Includes.h"
#include "BasePairAndStep.h"
#include "SamplingParametersProvider.h"

class TunableSamplingParametersProvider: public SamplingParametersProvider {
public:
	TunableSamplingParametersProvider(const StepParameters& mean, const Real& e);

	SequenceStepToMeanAndStdMap build();
private:
	Real e;
	StepParameters mean;
};

#endif /* TUNABLESAMPLINGPARAMETERSPROVIDER_H_ */
