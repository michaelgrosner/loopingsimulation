/*
 * ClosureStringGenerator.h
 *
 *  Created on: May 19, 2013
 *      Author: grosner
 */

#ifndef CLOSUREIDGENERATOR_H_
#define CLOSUREIDGENERATOR_H_

#include "Forwards.h"
#include <string>

using namespace std;

class ClosureStringGenerator {
public:
	virtual ~ClosureStringGenerator() {}
	virtual string generate(const ClosurePtr& closure) const = 0;
};

#endif /* CLOSUREIDGENERATOR_H_ */
