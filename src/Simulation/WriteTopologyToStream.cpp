/*
 * WriteTopologyToStream.cpp
 *
 *  Created on: May 19, 2013
 *      Author: grosner
 */

#include "WriteTopologyToStream.h"

void WriteTopologyToStream::Process(const ClosurePtr& closure) {
	DNASim::CurvePoints base = closure->ribbon_representation().ribbon_base_curve();
	DNASim::CurvePoints edge = closure->ribbon_representation().ribbon_edge_reference_curve();

	char fp[512];
	string loop_id = unique_id_generator.generate(closure);
	sprintf(fp, "%s/curves_%s.dat", output_prefix.c_str(), loop_id.c_str());
	StreamSinkPtr sink = sink_factory->create_sink_with_path(fp);

	for (unsigned int i = 0; i < base.size(); i++) {
		char f[512];
		sprintf(f, "%.3f,%.3f,%.3f,%.3f,%.3f,%.3f\n", base[i][X],
													  base[i][Y],
													  base[i][Z],
													  edge[i][X],
													  edge[i][Y],
													  edge[i][Z]);
		sink->add_line(string(f));
	}
	sink->flush();
};
