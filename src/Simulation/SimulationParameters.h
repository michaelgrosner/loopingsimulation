/*
 * SimulationParameters.h
 *
 *  Created on: Dec 7, 2012
 *      Author: grosner
 */

#ifndef SIMULATIONPARAMETERS_H_
#define SIMULATIONPARAMETERS_H_

#include "Forwards.h"
#include "Includes.h"
#include "LacType.h"
#include <vector>
#include <string>
#include <boost/program_options.hpp>

using namespace std;
using namespace boost::program_options;

struct SimulationParameters {

	SimulationParameters() {};
	SimulationParameters(options_description& desc, int argc, char *argv[]);
	SimulationParameters(const options_description& desc, const string& parameters_filepath);
	SimulationParameters(const boost::program_options::variables_map& vm);

	UInteger64 seed;

	Integer64 n_configurations;
	Integer64 n_cores;
	Integer64 n_snapshots;
	Integer64 n_alongs;
	Integer64  n_loops;

	LacType lac_type;
	string  lac_string;
	bool use_through_lac_topology;

	Real radi;
	Real gam;
	Real twi;
	Real trff;

	string sampling_parameters;
	string flexible_type;
	string boundary_condition_file;
	string output_prefix;
	vector<string> proteins_configuration;
	string proteins_configuration_string;
	string lac_repressor_directory;

	string sampler_type;
	Real temperature;

	string dna_sequence;

	unsigned int progress_display_interval;

	inline Real radi_squared() const { return radi*radi; }

	string run_hash(const BaseSequencePtr& dna_sequence) const;

private:
	variables_map vm;
	void initialize_variables_map();
};

#endif /* SIMULATIONPARAMETERS_H_ */
