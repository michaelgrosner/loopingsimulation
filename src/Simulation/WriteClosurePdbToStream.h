/*
 * WriteClosurePdbToStream.h
 *
 *  Created on: May 20, 2013
 *      Author: grosner
 */

#ifndef WRITECLOSUREPDBTOSTREAM_H_
#define WRITECLOSUREPDBTOSTREAM_H_

#include "ClosureProcessor.h"
#include "Forwards.h"

class WriteClosurePdbToStream: public ClosureProcessor {
public:
	WriteClosurePdbToStream(const string output_prefix,
							ClosureProcessor& write_steps_to_stream,
							PDBWriter& pdb_writer,
							ClosureStringGenerator& unique_id_generator) : output_prefix(output_prefix),
																				 write_steps_to_stream(write_steps_to_stream),
																				 pdb_writer(pdb_writer),
																				 unique_id_generator(unique_id_generator) {}
	virtual void Process(const ClosurePtr& closure);
private:
	ClosureStringGenerator& unique_id_generator;
	string output_prefix;
	ClosureProcessor& write_steps_to_stream;
	PDBWriter& pdb_writer;
};

#endif /* WRITECLOSUREPDBTOSTREAM_H_ */
