/*
 * AlongClosureCSVGenerator.h
 *
 *  Created on: May 20, 2013
 *      Author: grosner
 */

#ifndef ALONGCLOSURECSVGENERATOR_H_
#define ALONGCLOSURECSVGENERATOR_H_

#include "ClosureStringGenerator.h"

class AlongClosureCSVGenerator : public ClosureStringGenerator {
public:
	AlongClosureCSVGenerator(ClosureStringGenerator& unique_id_generator) : unique_id_generator(unique_id_generator) {}
	~AlongClosureCSVGenerator() {}
	virtual string generate(const ClosurePtr& closure) const;

private:
	ClosureStringGenerator& unique_id_generator;
};

#endif /* ALONGCLOSURECSVGENERATOR_H_ */
