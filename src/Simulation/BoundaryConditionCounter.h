/*
 * BoundaryConditionCounter.h
 *
 *  Created on: May 25, 2013
 *      Author: grosner
 */

#ifndef BOUNDARYCONDITIONCOUNTER_H_
#define BOUNDARYCONDITIONCOUNTER_H_

#include <map>
#include "Includes.h"
#include "BoundaryCondition.h"

typedef map<BoundaryCondition, UInteger64> BoundaryConditionCounter;

#endif /* BOUNDARYCONDITIONCOUNTER_H_ */
