/*
 * OutputStepsToStream.h
 *
 *  Created on: May 20, 2013
 *      Author: grosner
 */

#ifndef OUTPUTSTEPSTOSTREAM_H_
#define OUTPUTSTEPSTOSTREAM_H_

#include "ClosureProcessor.h"
#include "Forwards.h"

class OutputStepsToStream : public ClosureProcessor {
public:
	OutputStepsToStream(const string& output_prefix,
						const ClosureStringGenerator& unique_closure_generator,
						const StreamSinkFactoryPtr& sink_factory) : output_prefix(output_prefix),
																   unique_closure_generator(unique_closure_generator),
																   sink_factory(sink_factory) {}
	~OutputStepsToStream() {}

	virtual void Process(const ClosurePtr& closure);
private:
	const ClosureStringGenerator& unique_closure_generator;
	const StreamSinkFactoryPtr& sink_factory;
	string output_prefix;
};

#endif /* OUTPUTSTEPSTOSTREAM_H_ */
