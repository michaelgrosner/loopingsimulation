/*
 * LoopToDiscreteRibbonConverter.cpp
 *
 *  Created on: May 16, 2013
 *      Author: grosner
 */

#include "LoopToDiscreteRibbonConverter.h"
#include <DNASim/Triad.h>
#include <DNASim/CurveGeometry.h>
#include <DNASim/dsMatrix4.h>
#include <DNASim/DiscreteRibbon.h>

DNASim::DiscreteRibbon LoopToDiscreteRibbonConverter::convert(const BasePairVector& bps, const OptionalLacRepressorAndAngle& lac_and_angle) {
	DNASim::CurvePoints base, edge;

	vector<DNASim::Triad> loop_triads; loop_triads.reserve(bps.size()+14);

	if (lac_and_angle) {
		auto start_triad = DNASim::Triad();
		auto first_lac_triads  = lac_and_angle->lac_repressor().half1().get_triads_nearest_triad(start_triad);
		loop_triads.insert(loop_triads.end(), first_lac_triads.begin(), first_lac_triads.end());
	}

	for (auto i = 0; i < bps.size(); i++) {
		loop_triads.push_back(bp_to_dsMatrix4(bps[i]));
	}

	if (lac_and_angle) {
		auto lac_repressor = lac_and_angle->lac_repressor();
		auto second_lac_triads = lac_repressor.half2().get_triads_nearest_triad(loop_triads.back());
		loop_triads.insert(loop_triads.end(), second_lac_triads.begin(), second_lac_triads.end());

		if (include_lac_atoms) {
			base.push_back(lac_repressor.half1().atoms().front().second);
			edge.push_back(lac_repressor.half1().atoms().back().second);
		}
	}

	for (auto t = loop_triads.begin(); t != loop_triads.end(); t++) {
		auto base_vector = t->origin();
		auto edge_vector = t->origin() + t->axis(TriadAxis(1))*8.0;

		base.push_back(base_vector);
		edge.push_back(edge_vector);
	}

	if (lac_and_angle && include_lac_atoms) {
		auto lac_repressor = lac_and_angle->lac_repressor();
		auto last_generated_triad = bp_to_dsMatrix4(bps.back());
		DNASim::Triad t1;
		t1.set_origin(lac_repressor.half2().atoms().back().second);
		base.push_back(DNASim::Triad(last_generated_triad * t1.matrix_representation()).origin());

		DNASim::Triad t2;
		t2.set_origin(lac_repressor.half2().atoms().front().second);
		edge.push_back(DNASim::Triad(last_generated_triad * t2.matrix_representation()).origin());
	}

	return DNASim::DiscreteRibbon(base, edge);
}
