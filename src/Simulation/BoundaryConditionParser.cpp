/*
 * BoundaryConditionParser.cpp
 *
 *  Created on: Mar 13, 2013
 *      Author: grosner
 */

#include "BoundaryConditionParser.h"
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include "BasePairAndStep.h"
#include "BoundaryCondition.h"
#include <iostream>

BoundaryConditionParser::BoundaryConditionParser() {
	// TODO Auto-generated constructor stub

}

BoundaryCondition BoundaryConditionParser::parse(const string& boundary_string) {

	boost::char_separator<char> sep(" ");
	boost::tokenizer<boost::char_separator<char> > tokenizer(boundary_string, sep);

	vector<string> tokens(tokenizer.begin(), tokenizer.end());
	if (tokens.size() < 6) {
		cout << "Parsing boundary string with less than 6 tokens" << endl;
		throw 40;
	};

	StepParameters step;
	for (int i = 1; i < 7; i++) {
		step[i-1] = boost::lexical_cast<Real>(tokens[i]);
	}

	return BoundaryCondition(step, tokens[0]);
}
