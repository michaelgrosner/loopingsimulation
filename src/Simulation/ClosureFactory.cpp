/*
 * ClosureFactory.cpp
 *
 *  Created on: Dec 29, 2012
 *      Author: grosner
 */

#include "ClosureFactory.h"
#include "HalfChain.h"
#include "StepParametersGenerator.h"
#include "ProteinLocation.h"
#include "DiscreteRibbonConverter.h"
#include "Closure.h"
#include "BaseProteinLocationSampler.h"
#include "OptionalLacRepressorRetriever.h"
#include "BasePairAndStep.h"
#include "ProteinLocationComparison.h"
#include "BaseSequence.h"

ClosureFactory::ClosureFactory() {
	// TODO Auto-generated constructor stub

}

ClosurePtr ClosureFactory::generate_loop(HalfChain& first_half,
								const HalfChain& second_half,
								const BoundaryCondition& boundary,
								const Real& angle) {

	// Need to get back protein locations after recovering from the grid.
	if (first_half.protein_locations().size() == 0)
		first_half.set_protein_locations(protein_placer->sample_first_half(first_half.seed()));

	auto first_half_steps = steps_generator->generate_steps_vector_with_energy(first_half.seed(),
			dna_sequence->first_half_length(), 0, first_half.protein_locations());
	auto second_half_steps = steps_generator->generate_steps_vector_with_energy(second_half.seed(),
			dna_sequence->n_steps(), dna_sequence->first_half_length(), second_half.protein_locations());

	Real energy = 0;
	for (auto k = 0; k < first_half_steps.second.size();  k++) energy+=first_half_steps.second.at(k);
	for (auto k = 0; k < second_half_steps.second.size(); k++) energy+=second_half_steps.second.at(k);

	StepParametersVector loop_steps; loop_steps.reserve(dna_sequence->n_steps());

	BasePairVector loop_bps; loop_bps.reserve(dna_sequence->n_steps()+1);
	loop_bps.push_back(BasePair::Identity());

	for (auto s = first_half_steps.first.begin(); s != first_half_steps.first.end(); s++) {
		BasePair bp = loop_bps.back() * step_to_bp(*s);
		loop_bps.push_back(bp);
		loop_steps.push_back(*s);
	}
	for (auto s = second_half_steps.first.begin(); s != second_half_steps.first.end(); s++) {
		BasePair bp = loop_bps.back() * step_to_bp(*s);
		loop_bps.push_back(bp);
		loop_steps.push_back(*s);
	}

	// This could have doubled proteins -- fix this
	ProteinLocationSet protein_locations_set;
	protein_locations_set.insert(first_half.protein_locations().begin(),  first_half.protein_locations().end());
	protein_locations_set.insert(second_half.protein_locations().begin(), second_half.protein_locations().end());
	LocationCollection protein_locations(protein_locations_set.begin(), protein_locations_set.end());

	auto lac_and_angle = lac_repressor_retriever->retrieve(boundary, angle);
	auto ribbon_representation = ribbon_converter->convert(loop_bps, lac_and_angle);


	return ClosurePtr(new Closure(boundary,
								  first_half,
								  second_half,
								  loop_bps,
								  loop_steps,
								  protein_locations,
								  ribbon_representation,
								  lac_and_angle,
								  dna_sequence,
								  energy));

}
