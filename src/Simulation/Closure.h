/*
 * Closure.h
 *
 *  Created on: Feb 28, 2013
 *      Author: grosner
 */

#ifndef CLOSURE_H_
#define CLOSURE_H_

#include "Forwards.h"
#include "Includes.h"
#include "BasePairAndStep.h"
#include "BoundaryCondition.h"
#include "LocationCollection.h"
#include "HalfChain.h"
#include "LacRepressor.h"
#include <DNASim/DiscreteRibbon.h>

using namespace std;

class Closure {
public:
	Closure(const BoundaryCondition boundary,
		    const HalfChain first_half,
		    const HalfChain second_half,
		    const BasePairVector loop_bps,
		    const StepParametersVector loop_steps,
		    const LocationCollection protein_locations,
		    const DNASim::DiscreteRibbon ribbon_representation,
		    const OptionalLacRepressorAndAngle lac_and_angle,
		    BaseSequencePtr dna_sequence,
		    Real energy) : _boundary(boundary),
		    			   _first_half(first_half),
		    			   _second_half(second_half),
		    			   _bps(loop_bps),
		    			   _steps(loop_steps),
		    			   _protein_locations(protein_locations),
		    			   _ribbon_representation(ribbon_representation),
		    			   _dna_sequence(dna_sequence),
		    			   _energy(energy),
		    			   _lac_and_angle(lac_and_angle) {}

	virtual ~Closure() {};

	inline const BoundaryCondition&            boundary()              const { return _boundary; }
	inline const HalfChain&                    first_half()            const { return _first_half; }
	inline const HalfChain&                    second_half()           const { return _second_half; }
	inline const BasePairVector&               bps()                   const { return _bps; }
	inline const StepParametersVector&         steps()                 const { return _steps; }
	inline const LocationCollection&           protein_locations()     const { return _protein_locations; }
	inline const BaseSequencePtr&              dna_sequence()          const { return _dna_sequence; }
	inline const DNASim::DiscreteRibbon&       ribbon_representation() const { return _ribbon_representation; }
	inline const OptionalLacRepressorAndAngle& lac_and_angle()         const { return _lac_and_angle; }
	inline Real                                energy()                const { return _energy; }

protected:
	BoundaryCondition            _boundary;
	HalfChain                    _first_half;
	HalfChain                    _second_half;
	BasePairVector               _bps;
	StepParametersVector         _steps;
	LocationCollection           _protein_locations;
	BaseSequencePtr              _dna_sequence;
	DNASim::DiscreteRibbon       _ribbon_representation;
	OptionalLacRepressorAndAngle _lac_and_angle;
	Real                         _energy;
};

typedef boost::shared_ptr<Closure> ClosurePtr;

#endif /* CLOSURE_H_ */
