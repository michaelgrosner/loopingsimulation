/*
 * HalfChain.h
 *
 *  Created on: Dec 17, 2012
 *      Author: grosner
 */

#ifndef HALFCHAIN_H_
#define HALFCHAIN_H_

#include "Includes.h"
#include "BasePairAndStep.h"
#include "ProteinLocation.h"
#include "RandomSampler.h"
#include "LocationCollection.h"

class HalfChain {
public:
	HalfChain() {};
	HalfChain(const BasePair bp,
			  const LocationCollection protein_locations,
			  const Seed seed,
			  const OverhangCode overhang_code) : _bp(bp),
					  	  	  	                  _protein_locations(protein_locations),
					  	  	  	                  _seed(seed),
					  	  	  	                  _overhang_code(overhang_code) {}

	// TODO: Remove this method -- somehow
	inline void set_protein_locations(LocationCollection locations) { _protein_locations = locations; }

	inline const BasePair& bp() const { return _bp; }
	inline const LocationCollection& protein_locations() const { return _protein_locations; }
	inline Seed seed() const { return _seed; }
	inline OverhangCode overhang_code() const { return _overhang_code; }
private:
	BasePair _bp;
	LocationCollection _protein_locations;
	Seed _seed;
	OverhangCode _overhang_code;
};

#endif /* HALFCHAIN_H_ */
