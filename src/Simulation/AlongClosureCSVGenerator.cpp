/*
 * AlongClosureCSVGenerator.cpp
 *
 *  Created on: May 20, 2013
 *      Author: grosner
 */

#include "AlongClosureCSVGenerator.h"
#include "Closure.h"
#include <sstream>
#include <boost/algorithm/string/join.hpp>

// Move me!
inline std::string real_to_string_with_precision(Real x, int precision) {
    std::ostringstream ss;
    ss << std::fixed << std::setprecision(precision);
    ss << x;
    return ss.str();
}

string AlongClosureCSVGenerator::generate(const ClosurePtr& closure) const {
	vector<string> twists;
    auto ribbon = closure->ribbon_representation();
	for (unsigned int i = 0; i < ribbon.ribbon_base_curve().size(); i++) {
		twists.push_back(real_to_string_with_precision(ribbon.vertex_twist(i)*ribbon.vertex_length(i)*57.2957795, 2));
	}

	auto twist = unique_id_generator.generate(closure) + ",twist_density," + boost::algorithm::join(twists, ",") + "\n";

	return twist;
}
