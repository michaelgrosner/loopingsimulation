/*
 * ValidClosureProcessor.h
 *
 *  Created on: May 15, 2013
 *      Author: grosner
 */

#ifndef CLOSUREPROCESSOR_H_
#define CLOSUREPROCESSOR_H_

#include "Includes.h"
#include "JFactorReporter.h"
#include "LoopReporter.h"
#include "AlongLoopReporter.h"
#include "ProteinPositionsReporter.h"
#include "Closure.h"
#include "ClosureOverlapDeterminator.h"
#include "ClosureProcessor.h"

class ClosureProcessor {
public:
	virtual ~ClosureProcessor() {}
	virtual void Process(const ClosurePtr& closure) = 0;
};

class ValidClosureProcessor : public ClosureProcessor {
public:
	ValidClosureProcessor(JFactorReporter& j_factor_reporter,
					 LoopReporter& loop_reporter,
					 AlongLoopReporter& along_loop_reporter,
					 ProteinPositionsReporter& protein_positions_reporter,
					 ClosureOverlapDeterminator& overlap_determiner,
					 ClosureProcessor& make_images_processor) : j_factor_reporter(j_factor_reporter),
					 	 	 	 	 	 	   loop_reporter(loop_reporter),
					 	 	 	 	 	 	   along_loop_reporter(along_loop_reporter),
					 	 	 	 	 	 	   protein_positions_reporter(protein_positions_reporter),
					 	 	 	 	 	 	   overlap_determiner(overlap_determiner),
					 	 	 	 	 	 	   make_images_processor(make_images_processor) {}
	virtual ~ValidClosureProcessor() {}
	virtual void Process(const ClosurePtr& closure);
private:
	JFactorReporter& j_factor_reporter;
    LoopReporter& loop_reporter;
    AlongLoopReporter& along_loop_reporter;
    ProteinPositionsReporter& protein_positions_reporter;
    ClosureOverlapDeterminator& overlap_determiner;
    ClosureProcessor& make_images_processor;
};

#endif /* CLOSUREPROCESSOR_H_ */
