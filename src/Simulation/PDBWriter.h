/*
 * PDBWriter.h
 *
 *  Created on: Mar 16, 2013
 *      Author: grosner
 */

#ifndef PDBWRITER_H_
#define PDBWRITER_H_

class StreamProvider;
class StreamSink;

#include "BasePairAndStep.h"

using namespace std;

class PDBWriter {
public:
	PDBWriter() {};

	void rewrite_pdb(StreamProvider& in, StreamSink& out, const BasePair& bp);
};

#endif /* PDBWRITER_H_ */
