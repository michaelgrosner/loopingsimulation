/*
 * LoopToDiscreteRibbonConverter.h
 *
 *  Created on: May 16, 2013
 *      Author: grosner
 */

#ifndef LOOPTODISCRETERIBBONCONVERTER_H_
#define LOOPTODISCRETERIBBONCONVERTER_H_

#include "DiscreteRibbonConverter.h"

using namespace std;

class LoopToDiscreteRibbonConverter : public DiscreteRibbonConverter {
	bool include_lac_atoms;
public:
	LoopToDiscreteRibbonConverter(const bool include_lac_atoms) : include_lac_atoms(include_lac_atoms) {}
	virtual ~LoopToDiscreteRibbonConverter() {}
	virtual DNASim::DiscreteRibbon convert(const BasePairVector& bps, const OptionalLacRepressorAndAngle& lac_and_angle);
};

#endif /* LOOPTODISCRETERIBBONCONVERTER_H_ */
