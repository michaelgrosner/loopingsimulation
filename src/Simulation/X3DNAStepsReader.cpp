/*
 * X3DNAStepsReader.cpp
 *
 *  Created on: Jan 21, 2013
 *      Author: grosner
 */

#include "X3DNAStepsReader.h"
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include "FileProvider.h"
#include "Structure.h"
#include "ProteinClass.h"
#include "ProteinLocation.h"

StepParametersVector X3DNAStepsReader::get_steps_vector() {
	typedef boost::tokenizer<boost::char_separator<char> > LineTokens;

	StepParametersVector steps;

	auto lines = stream_provider.consume();
	for (auto line = lines.begin(); line != lines.end(); line++) {

		// Handle comments or blank lines
		if (boost::algorithm::starts_with(*line, "#") or
				boost::algorithm::starts_with(*line, " ") or
				boost::algorithm::starts_with(*line, "$") or
				line->empty()) continue;

		boost::char_separator<char> space_seperator(" ");
		LineTokens line_tokens(*line, space_seperator);

		vector<string> string_tokens(line_tokens.begin(), line_tokens.end());
		vector<double> tokens;
		for (unsigned int i = 1; i < 7; i++) {
			tokens.push_back(boost::lexical_cast<double>(string_tokens[i]));
		}

		StepParameters step;
		step << tokens[3], tokens[4], tokens[5], tokens[0], tokens[1], tokens[2];
		if (step.isApprox(StepParameters::Zero())) continue;
		steps.push_back(step);
	}

	return steps;
}

pair<StepParametersVector, LocationCollection> X3DNAStepsReader::get_steps_and_protein_locations(const ProteinClassPtr& pclass) {
	typedef boost::tokenizer<boost::char_separator<char> > LineTokens;

	LocationCollection locs;

	auto lines = stream_provider.consume();
	for (auto line = lines.begin(); line != lines.end(); line++) {

		if (!boost::algorithm::starts_with(*line, "$")) continue;
		line->erase(remove(line->begin(), line->end(), '$'), line->end());

		boost::char_separator<char> comma_seperator(",");
		LineTokens line_tokens(*line, comma_seperator);
		vector<string> string_tokens(line_tokens.begin(), line_tokens.end());

		StructurePtr structure;
		for (int i = 0; i < pclass->structures().size(); i++) {
			if (pclass->structures()[i]->short_name() == string_tokens[0]) structure = pclass->structures()[i];
		}
		if (structure.get() == NULL) throw 100;

		ProteinLocation pl(structure,
						   boost::lexical_cast<UInteger64>(string_tokens[1]),
						   boost::lexical_cast<int>(string_tokens[2]));
		locs.push_back(pl);
	}

	return make_pair(get_steps_vector(), locs);

}
