/*
 * ClosureOverlapDeterminator.cpp
 *
 *  Created on: May 16, 2013
 *      Author: grosner
 */

#include "ClosureOverlapDeterminator.h"
#include "Closure.h"
#include "BasePairAndStep.h"

bool ClosureOverlapDeterminator::has_overlap(const ClosurePtr& closure) {
	auto protein_locations = closure->protein_locations();
	vector<CAlphasMatrix> c_alphas_for_all_proteins;
	c_alphas_for_all_proteins.reserve(protein_locations.size());

	for (auto loc = protein_locations.begin(); loc != protein_locations.end(); loc++) {

		BasePair bp;
		if (loc->reversed()) {
			bp = closure->bps()[loc->start_step()];
		}
		else {
			bp = closure->bps()[loc->end_step()];
			bp.block(0,1,3,2) *= -1.0;
		}

		c_alphas_for_all_proteins.push_back(loc->structure()->get_c_alphas_transformed_by(bp));
	}

	for (auto i = 0; i < c_alphas_for_all_proteins.size(); i++) {
		auto ca_i = c_alphas_for_all_proteins[i];
		for (auto j = i+1; j < c_alphas_for_all_proteins.size(); j++) {
			auto ca_j = c_alphas_for_all_proteins[j];
			for (auto a = 0; a < ca_i.rows(); a++) {
				auto av = ca_i.row(a);
				for (auto b = 0; b < ca_j.rows(); b++) {
					auto bv = ca_j.row(b);
					auto d = (av-bv);
					if (fabs(d.dot(d)) < 6)
						return true;
				}
			}
		}
	}
	return false;
}
