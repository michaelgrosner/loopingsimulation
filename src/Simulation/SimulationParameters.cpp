/*
 * SimulationParameters.cpp
 *
 *  Created on: Dec 7, 2012
 *      Author: grosner
 */

#include "SimulationParameters.h"
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string/join.hpp>
#include <fstream>
#include <iostream>
#include "BaseSequence.h"

void SimulationParameters::initialize_variables_map() {

	seed = vm["seed"].as<UInteger64>();

	n_configurations = vm["n_configurations"].as<UInteger64>();
	n_snapshots      = vm["n_snapshots"].as<UInteger64>();
	n_alongs         = vm["n_alongs"].as<UInteger64>();
	n_loops          = vm["n_loops"].as<Integer64>();

	radi    = vm["radi"].as<Real>();
	gam     = vm["gam"].as<Real>();
	twi     = vm["twi"].as<Real>();
	trff    = 180.0*acos(gam)/M_PI;

	n_cores  = vm["cores"].as<UInteger64>();
	if (n_cores == -1) n_cores = boost::thread::hardware_concurrency();

	boundary_condition_file = vm["boundary_condition_file"].as<string>();
	if (!boost::filesystem::exists(boundary_condition_file)) {
		cout << "Cannot find boundary_condition_file " << boundary_condition_file << endl;
		throw 100;
	}
	output_prefix = vm["output_prefix"].as<string>();
	if (!boost::filesystem::exists(output_prefix)) {
		cout << "Cannot find output_prefix " << output_prefix << " -- defaulting to creating new directory" << endl;
		boost::filesystem::create_directory(output_prefix);
	}

	if (vm["lac"].as<string>() == "flexible") {
		lac_type = FLEXIBLE;
		flexible_type = vm["flexible_type"].as<string>();
		lac_string = "flexible";
	}
	else if (vm["lac"].as<string>() == "fixed") {
		lac_type = FIXED;
		lac_string = "fixed";
	}
	else if (vm["lac"].as<string>() == "minicircle") {
		lac_type = MINICIRCLE;
		lac_string = "minicircle";
	}
	else {
		cout << "Invalid lac type " << vm["lac"].as<string>() << endl;
		throw 8;
	}

	if (vm.find("proteins") != vm.end()) {
		proteins_configuration = vm["proteins"].as<vector<string> >();
		if (proteins_configuration.front() == "None") {
			proteins_configuration_string = "None";
			proteins_configuration.clear();
		}
		else {
			proteins_configuration_string = boost::algorithm::join(proteins_configuration, "_");
		}
	}
	else {
		proteins_configuration_string = "None";
	}

	if (vm.find("sampler_type") != vm.end()) {
		sampler_type = vm["sampler_type"].as<string>();
		temperature  = vm["temperature"].as<Real>();
	}

	if (vm.find("lac_repressor_directory") != vm.end()) {
		lac_repressor_directory = vm["lac_repressor_directory"].as<string>();
	}
	else{
		lac_repressor_directory = ".";
	}

	use_through_lac_topology = vm.find("dont_use_through_lac_topology") != vm.end() && !vm["dont_use_through_lac_topology"].as<bool>();

	dna_sequence = vm["dna_sequence"].as<string>();
	sampling_parameters = vm["sampling_parameters"].as<string>();

	progress_display_interval = vm["progress_display_interval"].as<unsigned int>();

}

SimulationParameters::SimulationParameters(const boost::program_options::variables_map& vm) : vm(vm) {
	initialize_variables_map();
}

SimulationParameters::SimulationParameters(options_description& desc, int argc, char* argv[]) {
	store(parse_command_line(argc, argv, desc), vm);
	notify(vm);
	initialize_variables_map();
}

SimulationParameters::SimulationParameters(const options_description& desc, const string& parameters_filepath) {
	ifstream f(parameters_filepath.c_str());
	store(parse_config_file(f , desc, true), vm);
	notify(vm);
	initialize_variables_map();
}

string SimulationParameters::run_hash(const BaseSequencePtr& dna_sequence) const {
	char h[256];
	string proteins_str;
	if (proteins_configuration.size() > 0) {
		proteins_str = boost::join(proteins_configuration, "");
	}
	else {
		proteins_str = "None";
	}

	sprintf(h, "%s_%s_%s", lac_string.c_str(), dna_sequence->name().c_str(), proteins_str.c_str());

	string sh = string(h);
	if (lac_type == FLEXIBLE) {
		sh += "_" + flexible_type;
	}

	return sh;
}
