/*
 * X3DNAStepsWriter.cpp
 *
 *  Created on: Mar 16, 2013
 *      Author: grosner
 */

#include "X3DNAStepsWriter.h"
#include <stdio.h>
#include "BaseSequence.h"

void X3DNAStepsWriter::write_to_file(const string& filepath,
									 const BaseSequencePtr& dna_sequence,
									 const StepParametersVector& steps) const {
	FILE *f = fopen(filepath.c_str(), "w");

    fprintf(f, "  %llu base-pairs\n", dna_sequence->n_steps()+1);
    fprintf(f, "   0  step parameters\n");
    fprintf(f, "      shift   slide    rise    tilt    roll   twist\n");
    fprintf(f, "A-T   0.000   0.000   0.000   0.000   0.000   0.000\n");

	for (auto step = steps.begin(); step != steps.end(); step++) {
		auto sp = *step;
		fprintf(f, "A-T%8.3lf%8.3lf%8.3lf%8.3lf%8.3lf%8.3lf\n", sp[3], sp[4], sp[5], sp[0], sp[1], sp[2]);
	}
	fclose(f);
}
