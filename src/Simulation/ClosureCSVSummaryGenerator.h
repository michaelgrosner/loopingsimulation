/*
 * ClosureCSVSummaryGenerator.h
 *
 *  Created on: May 19, 2013
 *      Author: grosner
 */

#ifndef CLOSURECSVSUMMARYGENERATOR_H_
#define CLOSURECSVSUMMARYGENERATOR_H_

#include "ClosureStringGenerator.h"

class ClosureCSVSummaryGenerator : public ClosureStringGenerator {
public:
	virtual ~ClosureCSVSummaryGenerator() {}
	ClosureCSVSummaryGenerator(ClosureStringGenerator& id_generator) : id_generator(id_generator) {}
	virtual string generate(const ClosurePtr& closure) const;
private:
	ClosureStringGenerator& id_generator;
};

#endif /* CLOSURECSVSUMMARYGENERATOR_H_ */
