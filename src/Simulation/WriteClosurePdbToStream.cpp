/*
 * WriteClosurePdbToStream.cpp
 *
 *  Created on: May 20, 2013
 *      Author: grosner
 */

#include "WriteClosurePdbToStream.h"
#include "PDBWriter.h"
#include "FileProvider.h"
#include "FileSink.h"
#include <DNASim/dsMatrix4.h>

void WriteClosurePdbToStream::Process(const ClosurePtr& closure) {
	write_steps_to_stream.Process(closure);

	string loop_id = unique_id_generator.generate(closure);

	auto protein_locations = closure->protein_locations();
	for (auto loc = protein_locations.begin(); loc != protein_locations.end(); loc++) {

		BasePair bp;
		if (!loc->reversed()) {
			bp = closure->bps()[loc->start_step()];
		}
		else {
			bp = BasePair(closure->bps()[loc->end_step()]);
			bp.block(0,1,3,2) *= -1.0;
		}
		char ppath[512];
		sprintf(ppath, "%s/%s_%llu_%i_%s.pdb", output_prefix.c_str(), loc->structure()->short_name().c_str(), loc->start_step(), loc->reversed(), loop_id.c_str());

		FileProvider fin(loc->structure()->pdb_path());
		FileSink fout(ppath);
		pdb_writer.rewrite_pdb(fin, fout, bp);
	}

	if (closure->lac_and_angle()) {
		auto lac_repressor = closure->lac_and_angle()->lac_repressor();
		char h1[512];
		sprintf(h1, "%s/half1_%s.pdb", output_prefix.c_str(), loop_id.c_str());

		// this may be a hack
		FileProvider h1_fin(lac_repressor.half1().pdb_filename());
		FileSink h1_sink(h1);
		pdb_writer.rewrite_pdb(h1_fin, h1_sink, BasePair::Identity());

		char h2[512];
		sprintf(h2, "%s/half2_%s.pdb", output_prefix.c_str(), loop_id.c_str());
		FileProvider h2_fin(lac_repressor.half2().pdb_filename());
		FileSink h2_sink(h2);
		pdb_writer.rewrite_pdb(h2_fin, h2_sink, closure->bps().back());
	}
}
