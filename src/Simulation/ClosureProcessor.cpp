/*
 * ValidClosureProcessor.cpp
 *
 *  Created on: May 15, 2013
 *      Author: grosner
 */

#include "ClosureProcessor.h"

void ValidClosureProcessor::Process(const ClosurePtr& closure) {
	if (overlap_determiner.has_overlap(closure)) return;

	j_factor_reporter.loop_is_good(closure);
	if (loop_reporter.add_loop(closure))
		protein_positions_reporter.add_proteins_from_loop(closure);
	along_loop_reporter.add_loop(closure);

	make_images_processor.Process(closure);
}
