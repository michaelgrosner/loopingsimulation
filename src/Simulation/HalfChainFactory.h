/*
 * HalfChainFactory.h
 *
 *  Created on: Dec 7, 2012
 *      Author: grosner
 */

#ifndef HALFCHAINFACTORY_H_
#define HALFCHAINFACTORY_H_

#include <vector>
#include "BasePairAndStep.h"
#include "HalfChain.h"
#include "Forwards.h"

class TwoSidedHalfChainFactory {
public:
	virtual ~TwoSidedHalfChainFactory() {}
	virtual HalfChain generate_first_half(const Seed& s) = 0;
	virtual vector<HalfChain> generate_second_half(const Seed& s) = 0;
};

class HalfChainFactory : public TwoSidedHalfChainFactory {
public:
	HalfChainFactory() {};
	HalfChainFactory(const StepParametersGeneratorPtr steps_generator,
					 const BasePair& origin,
                     const ProteinLocationSamplerPtr protein_sampler,
					 const BaseOverhangCodeMapPtr overhang_map,
					 const BaseSequencePtr& dna_sequence) : steps_generator(steps_generator),
                                                            origin(origin),
                                                            protein_sampler(protein_sampler),
                                                            overhang_map(overhang_map),
                                                            dna_sequence(dna_sequence) {}
	virtual ~HalfChainFactory() {};

	virtual HalfChain generate_first_half(const Seed& s);
	virtual vector<HalfChain> generate_second_half(const Seed& s);
private:

	HalfChain _generate(const Seed& s, const unsigned int start, const unsigned int end,
			const BasePair& origin, const LocationCollection& lc);

    ProteinLocationSamplerPtr protein_sampler;
    BaseOverhangCodeMapPtr overhang_map;
	BasePair origin;
	StepParametersGeneratorPtr steps_generator;
	BaseSequencePtr dna_sequence;
};

#endif /* HALFCHAINFACTORY_H_ */
