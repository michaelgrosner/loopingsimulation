/*
 * BoundaryConditionMapFactory.h
 *
 *  Created on: Mar 13, 2013
 *      Author: grosner
 */

#ifndef BOUNDARYCONDITIONMAPFACTORY_H_
#define BOUNDARYCONDITIONMAPFACTORY_H_

#include "Forwards.h"
#include <string>
#include "LacType.h"
#include "BoundaryConditionToLacRepressorsMap.h"
#include "LacAndOrientationFileParser.h"

class BoundaryConditionMapFactory {
public:
	BoundaryConditionMapFactory(const LacAndOrientationFileParser& parser,
								const StreamProviderPtr boundary_condition_file,
								const LacType lac_type,
								const string flexible_type) :
									lac_type(lac_type),
									boundary_condition_file(boundary_condition_file),
									flexible_type(flexible_type),
									parser(parser) {};

	BoundaryConditionToLacRepressorsMap build();
private:
	BoundaryConditionToLacRepressorsMap load_fixed_conditions();
	BoundaryConditionToLacRepressorsMap load_flexible_conditions();
	BoundaryConditionToLacRepressorsMap load_minicircle_condition();

	string flexible_type;
	LacType lac_type;
	LacAndOrientationFileParser parser;
	StreamProviderPtr boundary_condition_file;
};

#endif /* BOUNDARYCONDITIONMAPFACTORY_H_ */
