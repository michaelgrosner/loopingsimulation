/*
 * BoundaryConditionMapFactory.cpp
 *
 *  Created on: Mar 13, 2013
 *      Author: grosner
 */

#include "BoundaryConditionMapFactory.h"
#include "FileProvider.h"
#include "BoundaryCondition.h"
#include <boost/algorithm/string.hpp>
#include "BasePairAndStep.h"
#include "LacHalfFactory.h"
#include "LacAndOrientationFileParser.h"

BoundaryConditionToLacRepressorsMap BoundaryConditionMapFactory::build() {
	if      (lac_type == FIXED) return load_fixed_conditions();
	else if (lac_type == FLEXIBLE) return load_flexible_conditions();
	else if (lac_type == MINICIRCLE) return load_minicircle_condition();
	throw   "Cannot find lac_type";
}

BoundaryConditionToLacRepressorsMap BoundaryConditionMapFactory::load_fixed_conditions() {
	return parser.parse_file(boundary_condition_file);
}

BoundaryConditionToLacRepressorsMap BoundaryConditionMapFactory::load_flexible_conditions() {
	LacHalfFactoryPtr lac_half_factory(new LacHalfFactory());

	auto h10 = lac_half_factory->build(StreamProviderPtr(new FileProvider("Resources/Lac/half1_0.dat")), "Resources/Lac/half1_0.dat", "Resources/Lac/half1_0.pdb", 1);
	auto h11 = lac_half_factory->build(StreamProviderPtr(new FileProvider("Resources/Lac/half1_1.dat")), "Resources/Lac/half1_1.dat", "Resources/Lac/half1_1.pdb", 1);
	auto h20 = lac_half_factory->build(StreamProviderPtr(new FileProvider("Resources/Lac/half2_0.dat")), "Resources/Lac/half2_0.dat", "Resources/Lac/half2_0.pdb", 2);
	auto h21 = lac_half_factory->build(StreamProviderPtr(new FileProvider("Resources/Lac/half2_1.dat")), "Resources/Lac/half2_1.dat", "Resources/Lac/half2_1.pdb", 2);

	BoundaryConditionToLacRepressorsMap boundary_conditions;
	BasePair A, B, C, D;

	A << -0.5869, -0.2543,  0.7686,  57.6981,   // A1P1
		  0.5716,  0.5422,  0.6159,  26.5508,
		 -0.5734,  0.8009, -0.1729,  60.8759,
		       0,       0,       0,        1;

	B << -0.3543,  0.9351, -0.0037,  34.9491,   // A2P2
		 -0.8959, -0.3405, -0.2855,   8.9248,
		 -0.2682, -0.0978,  0.9584,  90.2921,
		       0,       0,       0,        1;

	C <<  -0.781, -0.5779, -0.2369,   75.223,   // 3
		  -0.587,  0.5497,  0.5944, -18.0357,
		 -0.2132,  0.6033, -0.7685,  31.6546,
		       0,       0,       0,        1;

	D << -0.4194, -0.3722,  -0.828,  89.3486,   // 4
		  0.8861, -0.3661, -0.2843,  -1.0677,
		 -0.1973, -0.8529,  0.4833,  -3.0732,
		       0,       0,       0,        1;

	BoundaryCondition A1(A, D.inverse(), "A1");
	BoundaryCondition P1(A, C.inverse(), "P1");
	BoundaryCondition A2(B, C.inverse(), "A2");
	BoundaryCondition P2(B, D.inverse(), "P2");

	if (boost::algorithm::contains(flexible_type, "A1")) {
		boundary_conditions[A1] = LacRepressor("A1", h10, h20);
	}
	if (boost::algorithm::contains(flexible_type, "A2")) {
		boundary_conditions[A2] = LacRepressor("A2", h11, h21);
	}
	if (boost::algorithm::contains(flexible_type, "P1")) {
		boundary_conditions[P1] = LacRepressor("P1", h10, h21);
	}
	if (boost::algorithm::contains(flexible_type, "P2")) {
		boundary_conditions[P2] = LacRepressor("P2", h11, h20);
	}

	return boundary_conditions;
}

BoundaryConditionToLacRepressorsMap BoundaryConditionMapFactory::load_minicircle_condition() {
	BoundaryConditionToLacRepressorsMap boundary_conditions;
	BasePair A = BasePair::Identity();
	boundary_conditions[BoundaryCondition(A, A.inverse(), "MC")] = OptionalLacRepressor();
	return boundary_conditions;
}
