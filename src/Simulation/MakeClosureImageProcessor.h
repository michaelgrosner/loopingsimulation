/*
 * MakeClosureImageProcessor.h
 *
 *  Created on: May 19, 2013
 *      Author: grosner
 */

#ifndef MAKECLOSUREIMAGEPROCESSOR_H_
#define MAKECLOSUREIMAGEPROCESSOR_H_

#include "ClosureProcessor.h"
#include "Forwards.h"

class MakeClosureImageProcessor: public ClosureProcessor {
public:
	virtual ~MakeClosureImageProcessor() {};
	MakeClosureImageProcessor(JFactorReporter& j_factor_reporter,
							  ClosureProcessor& topology_writer,
							  ClosureProcessor& image_writer,
							  const UInteger64 n_snapshots) : j_factor_reporter(j_factor_reporter),
									  	  	  	  	  	  	  topology_writer(topology_writer),
									  	  	  	  	  	  	  image_writer(image_writer),
									  	  	  	  	  	  	  n_snapshots(n_snapshots) {}

	virtual void Process(const ClosurePtr& closure);
private:
	JFactorReporter& j_factor_reporter;
	ClosureProcessor& topology_writer;
	ClosureProcessor& image_writer;
	UInteger64 n_snapshots;
};

#endif /* MAKECLOSUREIMAGEPROCESSOR_H_ */
