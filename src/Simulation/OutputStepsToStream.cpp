/*
 * OutputStepsToStream.cpp
 *
 *  Created on: May 20, 2013
 *      Author: grosner
 */

#include "OutputStepsToStream.h"
#include "Closure.h"
#include "FileSink.h"

void OutputStepsToStream::Process(const ClosurePtr& closure) {

	char fp[512];
	string loop_id = unique_closure_generator.generate(closure);
	sprintf(fp, "%s/steps_%s.dat", output_prefix.c_str(), loop_id.c_str());

	StreamSinkPtr sink = sink_factory->create_sink_with_path(string(fp));

	char f[512];
	sprintf(f, "  %lu base-pairs\n", closure->steps().size()+1);
	sink->add_line(f);
	sprintf(f, "   0  step parameters\n");
	sink->add_line(f);
	sprintf(f, "      shift   slide    rise    tilt    roll   twist\n");
	sink->add_line(f);
	sprintf(f, "A-T   0.000   0.000   0.000   0.000   0.000   0.000\n");
	sink->add_line(f);

	for (auto step = closure->steps().begin(); step != closure->steps().end(); step++) {
		auto sp = *step;
		sprintf(f, "A-T%8.3lf%8.3lf%8.3lf%8.3lf%8.3lf%8.3lf\n", sp[3], sp[4], sp[5], sp[0], sp[1], sp[2]);
		sink->add_line(f);
	}

	sink->flush();
}
