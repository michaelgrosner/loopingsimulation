/*
 * MakeClosureImageProcessor.cpp
 *
 *  Created on: May 19, 2013
 *      Author: grosner
 */

#include "MakeClosureImageProcessor.h"
#include "JFactorReporter.h"
#include "WriteTopologyToStream.h"
#include "FileSink.h"

void MakeClosureImageProcessor::Process(const ClosurePtr& closure) {
	// write out
	if (j_factor_reporter.number_of_good_loops_by_boundary(closure->boundary()) < n_snapshots) {
		image_writer.Process(closure);
		topology_writer.Process(closure);
	}
}
