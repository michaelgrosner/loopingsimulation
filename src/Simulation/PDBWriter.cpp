/*
 * PDBWriter.cpp
 *
 *  Created on: Mar 16, 2013
 *      Author: grosner
 */

#include "PDBWriter.h"
#include "FileProvider.h"
#include "FileSink.h"
#include "BasePairAndStep.h"
#include <boost/algorithm/string/predicate.hpp>
#include <string>

void PDBWriter::rewrite_pdb(StreamProvider& in, StreamSink& out, const BasePair& bp) {
	auto lines = in.consume();

	for (auto line_it = lines.begin(); line_it != lines.end(); line_it++) {
		if (!boost::algorithm::starts_with(*line_it, "ATOM")) continue;
		auto head = line_it->substr(0, 30);
		auto x = atof(line_it->substr(30,8).c_str());
		auto y = atof(line_it->substr(38,8).c_str());
		auto z = atof(line_it->substr(46,8).c_str());
		auto tail = line_it->substr(54, line_it->size()-1);

		auto xn = bp(0,3)+x*bp(0,0)+y*bp(0,1)+z*bp(0,2);
		auto yn = bp(1,3)+x*bp(1,0)+y*bp(1,1)+z*bp(1,2);
		auto zn = bp(2,3)+x*bp(2,0)+y*bp(2,1)+z*bp(2,2);

		char f2[512];
		sprintf(f2, "%s%8.3f%8.3f%8.3f%s\n", head.c_str(), xn, yn, zn, tail.c_str());
		out.add_line(f2);
	}
	out.flush();
}
