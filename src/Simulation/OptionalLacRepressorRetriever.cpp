/*
 * OptionalLacRepressorRetriever.cpp
 *
 *  Created on: May 24, 2013
 *      Author: grosner
 */

#include "OptionalLacRepressorRetriever.h"
#include "BoundaryCondition.h"
#include "LacRepressor.h"

OptionalLacRepressorAndAngle OptionalLacRepressorRetriever::retrieve(const BoundaryCondition& boundary, const Real& angle) const {
	auto lac_it = lac_boundary_map.find(boundary);
	auto lac = lac_it->second;
	OptionalLacRepressorAndAngle optional_lac;
	if (lac_it == lac_boundary_map.end() or !lac) return optional_lac;

	auto from_map = lac_boundary_map[boundary];
	return OptionalLacRepressorAndAngle(LacRepressorAndAngle(*from_map, angle));
}
