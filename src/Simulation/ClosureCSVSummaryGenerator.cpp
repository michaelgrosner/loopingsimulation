/*
 * ClosureCSVSummaryGenerator.cpp
 *
 *  Created on: May 19, 2013
 *      Author: grosner
 */

#include "ClosureCSVSummaryGenerator.h"
#include "Closure.h"

string ClosureCSVSummaryGenerator::generate(const ClosurePtr& closure) const {
	char line[1024];
    
    auto angle = (closure->lac_and_angle() ?  closure->lac_and_angle()->angle() : 0);
    
	sprintf(line, "%s,%.3f,%lu,%.3f,%.3f,%.3f,%.3f",
			id_generator.generate(closure).c_str(),
			closure->energy(),
			closure->protein_locations().size(),
			closure->ribbon_representation().ribbon_total_twist(),
			closure->ribbon_representation().ribbon_writhing_number(),
			closure->ribbon_representation().ribbon_linking_number(),
			angle);
	return string(line);
}
