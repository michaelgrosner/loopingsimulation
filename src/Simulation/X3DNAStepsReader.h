/*
 * X3DNAStepsReader.h
 *
 *  Created on: Jan 21, 2013
 *      Author: grosner
 */

#ifndef X3DNASTEPSREADER_H_
#define X3DNASTEPSREADER_H_

#include "Forwards.h"
#include "BasePairAndStep.h"
#include "LocationCollection.h"
#include <utility>

using namespace std;

class X3DNAStepsReader {
public:
	X3DNAStepsReader(StreamProvider& stream_provider) : stream_provider(stream_provider) {};

	StepParametersVector get_steps_vector();
	pair<StepParametersVector, LocationCollection> get_steps_and_protein_locations(const ProteinClassPtr& pclass);

private:
	StreamProvider& stream_provider;
};

#endif /* X3DNASTEPSREADER_H_ */
