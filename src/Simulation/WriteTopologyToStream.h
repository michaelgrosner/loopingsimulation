/*
 * WriteTopologyToStream.h
 *
 *  Created on: May 19, 2013
 *      Author: grosner
 */

#ifndef WRITETOPOLOGYTOFILE_H_
#define WRITETOPOLOGYTOFILE_H_

#include "Closure.h"
#include "FileSink.h"
#include "ClosureProcessor.h"

class WriteTopologyToStream : public ClosureProcessor {
public:
	~WriteTopologyToStream() {}
	WriteTopologyToStream(StreamSinkFactoryPtr sink_factory,
						  ClosureStringGenerator& unique_id_generator,
						  const string output_prefix) : sink_factory(sink_factory),
								  	  	  	  	  	    unique_id_generator(unique_id_generator),
						  	   	   	   	   	   	   	   	output_prefix(output_prefix) {}
	virtual void Process(const ClosurePtr& closure);
private:
	StreamSinkFactoryPtr sink_factory;
	ClosureStringGenerator& unique_id_generator;
	string output_prefix;
};

#endif /* WRITETOPOLOGYTOFILE_H_ */
