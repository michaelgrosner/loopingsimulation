/*
 * X3DNAStepsWriter.h
 *
 *  Created on: Mar 16, 2013
 *      Author: grosner
 */

#ifndef X3DNASTEPSWRITER_H_
#define X3DNASTEPSWRITER_H_

#include "Forwards.h"
#include <string>
#include "BasePairAndStep.h"

class X3DNAStepsWriter {
public:
	void write_to_file(const string& filepath,
				       const BaseSequencePtr& dna_sequence,
				       const StepParametersVector& steps) const;
};

#endif /* X3DNASTEPSWRITER_H_ */
