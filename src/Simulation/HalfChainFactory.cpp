/*
 * HalfChainFactory.cpp
 *
 *  Created on: Dec 7, 2012
 *      Author: grosner
 */

#include "HalfChainFactory.h"
#include "BaseProteinLocationSampler.h"
#include "StepParametersGenerator.h"
#include "OverhangCodeMapFactory.h"
#include "HalfChain.h"
#include "BaseSequence.h"

HalfChain HalfChainFactory::generate_first_half(const Seed& s) {
	auto bound_proteins = protein_sampler->sample_first_half(s);
	return _generate(s, 0, dna_sequence->first_half_length(), origin, bound_proteins);
}

vector<HalfChain> HalfChainFactory::generate_second_half(const Seed& s) {
	vector<HalfChain> second_halfs;

	auto lcs = protein_sampler->sample_second_half(s);
	for (auto lc = lcs.begin(); lc != lcs.end(); lc++) {
		auto hc = _generate(s, dna_sequence->first_half_length(), dna_sequence->n_steps(), BasePair::Identity(), *lc);
		second_halfs.push_back(hc);
	}

	return second_halfs;
}

HalfChain HalfChainFactory::_generate(const Seed& s,
									  const unsigned int start,
									  const unsigned int end,
									  const BasePair& origin,
									  const LocationCollection& lc) {

	BasePair bp = origin;
	auto steps = steps_generator->generate_steps_vector(s, end, start, lc);
	for (auto s = steps.begin(); s != steps.end(); s++) {
		bp *= step_to_bp(*s);
	}

	return HalfChain(bp, lc, s, overhang_map->get_overhang_code(lc));
}
