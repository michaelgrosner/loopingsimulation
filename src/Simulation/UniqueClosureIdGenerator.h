/*
 * UniqueClosureIdGenerator.h
 *
 *  Created on: May 19, 2013
 *      Author: grosner
 */

#ifndef UNIQUECLOSUREIDGENERATOR_H_
#define UNIQUECLOSUREIDGENERATOR_H_

#include "ClosureStringGenerator.h"

using namespace std;

class UniqueClosureIdGenerator : public ClosureStringGenerator {
public:
	~UniqueClosureIdGenerator() {}
	UniqueClosureIdGenerator(const string lac_string,
							 const string proteins_configuration_string) : lac_string(lac_string),
							 	 	 	 	 	 	 	 	 	 	 	   proteins_configuration_string(proteins_configuration_string) {}

	virtual string generate(const ClosurePtr& closure) const;

private:
	string lac_string;
	string proteins_configuration_string;
};

#endif /* UNIQUECLOSUREIDGENERATOR_H_ */
