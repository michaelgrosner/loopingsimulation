/*
 * BoundaryCondition.h
 *
 *  Created on: Dec 8, 2012
 *      Author: grosner
 */

#ifndef BOUNDARYCONDITION_H_
#define BOUNDARYCONDITION_H_

#include "BasePairAndStep.h"

typedef uint64_t UInteger64;

using namespace std;

class BoundaryCondition {
public:
	BoundaryCondition() {};
	BoundaryCondition(const StepParameters& step,
					  const string& name) : _name(name) {
		_start = BasePair::Identity();
		_end = step_to_bp(step).inverse();
	};

	BoundaryCondition(const BasePair& start,
					  const BasePair& end,
					  const string& name) :
						  	  	  	  	  	_start(start),
						  	  	  	  	  	_end(end),
						  	  	  	  	  	_name(name) {};

	inline const BasePair& start() const { return _start; }
	inline const BasePair& end() const { return _end; }
	inline string name() const { return _name; }

	bool operator<(const BoundaryCondition& other) const {
		return name() < other.name();
	}
	bool operator==(const BoundaryCondition& other) const {
		return name() == other.name() && start().isApprox(other.start()) && end().isApprox(end());
	}

private:
	BasePair _start;
	BasePair _end;
	string _name;
};

#endif /* BOUNDARYCONDITION_H_ */
