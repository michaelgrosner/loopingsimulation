/*
 * OptionalLacRepressorRetriever.h
 *
 *  Created on: May 24, 2013
 *      Author: grosner
 */

#ifndef OPTIONALLACREPRESSORRETRIEVER_H_
#define OPTIONALLACREPRESSORRETRIEVER_H_

#include "BoundaryConditionToLacRepressorsMap.h"

class OptionalLacRepressorRetriever {
	BoundaryConditionToLacRepressorsMap& lac_boundary_map;
public:
	OptionalLacRepressorRetriever(BoundaryConditionToLacRepressorsMap& lac_boundary_map) : lac_boundary_map(lac_boundary_map) {}
	OptionalLacRepressorAndAngle retrieve(const BoundaryCondition& boundary, const Real& angle) const;
};

#endif /* OPTIONALLACREPRESSORRETRIEVER_H_ */
