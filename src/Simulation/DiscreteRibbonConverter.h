/*
 * DiscreteRibbonConverter.h
 *
 *  Created on: May 16, 2013
 *      Author: grosner
 */

#ifndef DISCRETERIBBONCONVERTER_H_
#define DISCRETERIBBONCONVERTER_H_

#include "Includes.h"
#include "BasePairAndStep.h"
#include "LacRepressor.h"
#include <DNASim/DiscreteRibbon.h>

using namespace std;

class DiscreteRibbonConverter {
public:
	virtual ~DiscreteRibbonConverter() {}
	virtual DNASim::DiscreteRibbon convert(const BasePairVector& bps, const OptionalLacRepressorAndAngle& lac_and_angle) = 0;
};

#endif /* DISCRETERIBBONCONVERTER_H_ */
