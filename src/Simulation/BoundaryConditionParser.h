/*
 * BoundaryConditionParser.h
 *
 *  Created on: Mar 13, 2013
 *      Author: grosner
 */

#ifndef BOUNDARYCONDITIONPARSER_H_
#define BOUNDARYCONDITIONPARSER_H_

#include "Forwards.h"
#include <string>

using namespace std;

class BoundaryConditionParser {
public:
	BoundaryConditionParser();

	BoundaryCondition parse(const string& boundary_string);
};

#endif /* BOUNDARYCONDITIONPARSER_H_ */
