/*
 * ClosureFactory.h
 *
 *  Created on: Dec 29, 2012
 *      Author: grosner
 */

#ifndef LOOPFACTORY_H_
#define LOOPFACTORY_H_

#include "Includes.h"
#include "Forwards.h"

class BaseClosureFactory {
public:
	virtual ~BaseClosureFactory() {}
	virtual ClosurePtr generate_loop(HalfChain& first_half,
				  	                     const HalfChain& second_half,
				 	                     const BoundaryCondition& boundary,
				 	                     const Real& angle) = 0;
};

class ClosureFactory : public BaseClosureFactory {
public:
	ClosureFactory();
	virtual ~ClosureFactory() {}
	ClosureFactory(const StepParametersGeneratorPtr steps_generator,
				   const OptionalLacRepressorRetrieverPtr lac_repressor_retriever,
				   const ProteinLocationSamplerPtr protein_placer,
				   const DiscreteRibbonConverterPtr ribbon_converter,
				   const BaseSequencePtr dna_sequence) : protein_placer(protein_placer),
						   	   	   	   	   	   	   	   	 steps_generator(steps_generator),
						   	   	   	   	   	   	   	   	 lac_repressor_retriever(lac_repressor_retriever),
						   	   	   	   	   	   	   	   	 ribbon_converter(ribbon_converter),
						   	   	   	   	   	   	   	   	 dna_sequence(dna_sequence) {};

	virtual ClosurePtr generate_loop(HalfChain& first_half,
			  	                     const HalfChain& second_half,
			 	                     const BoundaryCondition& boundary,
			 	                     const Real& angle);

private:
	ProteinLocationSamplerPtr protein_placer;
	StepParametersGeneratorPtr steps_generator;
	OptionalLacRepressorRetrieverPtr lac_repressor_retriever;
	BaseSequencePtr dna_sequence;
	DiscreteRibbonConverterPtr ribbon_converter;
};

typedef boost::shared_ptr<ClosureFactory> ClosureFactoryPtr;

#endif /* LOOPFACTORY_H_ */
