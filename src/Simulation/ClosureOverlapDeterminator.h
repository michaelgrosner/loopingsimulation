/*
 * ClosureOverlapDeterminator.h
 *
 *  Created on: May 16, 2013
 *      Author: grosner
 */

#ifndef CLOSUREOVERLAPDETERMINATOR_H_
#define CLOSUREOVERLAPDETERMINATOR_H_

#include "Forwards.h"

class ClosureOverlapDeterminator {
public:
	ClosureOverlapDeterminator() {}
	bool has_overlap(const ClosurePtr& closure);
};

#endif /* CLOSUREOVERLAPDETERMINATOR_H_ */
