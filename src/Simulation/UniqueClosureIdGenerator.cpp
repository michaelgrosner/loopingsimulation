/*
 * UniqueClosureIdGenerator.cpp
 *
 *  Created on: May 19, 2013
 *      Author: grosner
 */

#include "UniqueClosureIdGenerator.h"
#include "Closure.h"
#include "BaseSequence.h"

string UniqueClosureIdGenerator::generate(const ClosurePtr& closure) const {
	char id[512];
	sprintf(id, "%s,%s,%s,%2s,%llu,%llu", closure->dna_sequence()->name().c_str(),
										  lac_string.c_str(),
										  proteins_configuration_string.c_str(),
										  closure->boundary().name().c_str(),
										  closure->first_half().seed(),
										  closure->second_half().seed());
	return string(id);
};
