/*
 * BoundaryConditionToLacRepressorsMap.h
 *
 *  Created on: May 16, 2013
 *      Author: grosner
 */

#ifndef BOUNDARYCONDITIONTOLACREPRESSORSMAP_H_
#define BOUNDARYCONDITIONTOLACREPRESSORSMAP_H_

#include <map>
#include <boost/optional.hpp>
#include "LacRepressor.h"
#include "BoundaryCondition.h"

typedef boost::optional<LacRepressor> OptionalLacRepressor;
typedef std::map<BoundaryCondition, OptionalLacRepressor> BoundaryConditionToLacRepressorsMap;

#endif /* BOUNDARYCONDITIONTOLACREPRESSORSMAP_H_ */
