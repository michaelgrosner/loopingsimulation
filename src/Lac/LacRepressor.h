/*
 * LacRepressor.h
 *
 *  Created on: Dec 29, 2012
 *      Author: grosner
 */

#ifndef LACREPRESSOR_H_
#define LACREPRESSOR_H_

#include "Includes.h"
#include "SimulationParameters.h"
#include "LacHalf.h"
#include "BasePairAndStep.h"

class LacRepressor {
public:
	LacRepressor() {}
	LacRepressor(const string orientation,
				 const LacHalf half1,
				 const LacHalf half2) : _orientation(orientation),
						 	 	 	    _half1(half1),
						 	 	 	    _half2(half2) {}

	inline const LacHalf& half1()       const { return _half1; }
	inline const LacHalf& half2()       const { return _half2; }
	inline string         orientation() const { return _orientation; }

private:
	LacHalf _half1;
	LacHalf _half2;
	string  _orientation;
};

class LacRepressorAndAngle {
public:
	LacRepressorAndAngle() {}
	LacRepressorAndAngle(const LacRepressor& lac_repressor,
						 const Real angle) : _lac_repressor(lac_repressor),
						 	 	 	 	 	 _angle(angle) {}

	inline const LacRepressor& lac_repressor() const { return _lac_repressor; }
	inline Real                angle()         const { return _angle; }

private:
	LacRepressor _lac_repressor;
	Real         _angle;
};

#include <boost/optional.hpp>
typedef boost::optional<LacRepressorAndAngle> OptionalLacRepressorAndAngle;

#endif /* LACREPRESSOR_H_ */
