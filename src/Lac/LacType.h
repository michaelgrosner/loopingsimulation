/*
 * LacType.h
 *
 *  Created on: May 25, 2013
 *      Author: grosner
 */

#ifndef LACTYPE_H_
#define LACTYPE_H_

enum LacType {FIXED, FLEXIBLE, MINICIRCLE};

#endif /* LACTYPE_H_ */
