/*
 * LacAndOrientationFileParser.h
 *
 *  Created on: Apr 7, 2013
 *      Author: grosner
 */

#ifndef LACANDORIENTATIONFILEPARSER_H_
#define LACANDORIENTATIONFILEPARSER_H_

#include "Forwards.h"
#include <string>
#include "BoundaryConditionToLacRepressorsMap.h"

using namespace std;

class LacAndOrientationFileParser {
public:
	LacAndOrientationFileParser() {}
	LacAndOrientationFileParser(const string& _directory, LacHalfFactoryPtr lac_half_factory, StreamProviderPtr stream_provider) :
		directory(_directory), lac_half_factory(lac_half_factory), stream_provider(stream_provider) {}
	BoundaryConditionToLacRepressorsMap parse_file(StreamProviderPtr stream);
private:
	string directory;
	LacHalfFactoryPtr lac_half_factory;
	StreamProviderPtr stream_provider;
};

#endif /* LACANDORIENTATIONFILEPARSER_H_ */
