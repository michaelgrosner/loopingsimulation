/*
 * LacHalfFactory.cpp
 *
 *  Created on: Apr 8, 2013
 *      Author: grosner
 */

#include "LacHalfFactory.h"
#include "FileProvider.h"
#include <boost/tokenizer.hpp>

LacHalf LacHalfFactory::build(StreamProviderPtr stream, const string& dat, const string& pdb, const UInteger64& half_number) {

	vector<NamedDNASimVector3D> named_atoms;
	vector<DNASim::Triad> triads;

	auto lines = stream->consume();

	for (auto line = lines.begin(); line != lines.end(); line++) {
		boost::char_separator<char> sep(";");
		boost::tokenizer<boost::char_separator<char> > tokenizer(*line, sep);
		vector<string> tokens(tokenizer.begin(), tokenizer.end());

		if (tokens[0] == "ATOM") {
			named_atoms.push_back(NamedDNASimVector3D(tokens[1], tokens[2]));
		}
		else if (tokens[0] == "TRIAD") {
			DNASim::Triad t(tokens[1], tokens[2], tokens[3], tokens[4]);
			triads.push_back(t);
		}
	}

	bool reversed = false;
	double front_distance_from_origin = triads.front().origin().norm();
	if ((front_distance_from_origin < 5 and half_number == 1) or
	    (front_distance_from_origin > 5 and half_number == 2)) {
	    reverse(triads.begin(), triads.end());
	    reversed = true;
	}

	return LacHalf(half_number, dat, pdb, triads, named_atoms, reversed);
}
