/*
 * LacAndOrientationFileParser.cpp
 *
 *  Created on: Apr 7, 2013
 *      Author: grosner
 */

#include "LacAndOrientationFileParser.h"
#include <boost/tokenizer.hpp>
#include <DNASim/Vector.h>
#include "BoundaryConditionToLacRepressorsMap.h"
#include "FileProvider.h"
#include "LacHalfFactory.h"

BoundaryConditionToLacRepressorsMap LacAndOrientationFileParser::parse_file(StreamProviderPtr stream) {
	BoundaryConditionToLacRepressorsMap map;

	auto lines = stream->consume();

	for (vector<string>::iterator line = lines.begin(); line != lines.end(); line++) {
		boost::char_separator<char> sep(";", "", boost::keep_empty_tokens);
		boost::tokenizer<boost::char_separator<char> > tokenizer(*line, sep);
		vector<string> tokens(tokenizer.begin(), tokenizer.end());

		DNASim::Vector<Real> v(tokens[3]);
		StepParameters step;
		for (unsigned int i = 0; i < v.size(); i++)
			step[i] = v[i];

		BoundaryCondition boundary(StepParameters(step), tokens[0]);
		OptionalLacRepressor lac;
		if (!tokens[1].empty() && !tokens[2].empty()) {

			string dat1 = directory + "/" + tokens[1] + ".dat";
			string pdb1 = directory + "/" + tokens[1] + ".pdb";
			stream_provider->set_input(dat1);
			LacHalf half1 = lac_half_factory->build(stream_provider, dat1, pdb1, 1);

			string dat2 = directory + "/" + tokens[2] + ".dat";
			string pdb2 = directory + "/" + tokens[2] + ".pdb";
			stream_provider->set_input(dat2);
			LacHalf half2 = lac_half_factory->build(stream_provider, dat2, pdb2, 2);

			lac = LacRepressor(tokens[0], half1, half2);
		}

		map.insert(make_pair(boundary,lac));
	}
	return map;
}
