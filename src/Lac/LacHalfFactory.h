/*
 * LacHalfFactory.h
 *
 *  Created on: Apr 8, 2013
 *      Author: grosner
 */

#ifndef LACHALFFACTORY_H_
#define LACHALFFACTORY_H_

#include "Forwards.h"
#include <string>
#include "Includes.h"
#include "LacHalf.h"

class LacHalfFactory {
public:
	virtual ~LacHalfFactory() {}
	virtual LacHalf build(StreamProviderPtr stream, const string& dat, const string& pdb, const UInteger64& half_number);
};

#endif /* LACHALFFACTORY_H_ */
