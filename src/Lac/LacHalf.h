/*
 * LacHalf.h
 *
 *  Created on: Dec 29, 2012
 *      Author: grosner
 */

#ifndef LACHALF_H_
#define LACHALF_H_

#include "Includes.h"
#include "BasePairAndStep.h"
#include <utility>
#include <vector>
#include <string>
#include <DNASim/Vector3D.h>
#include <DNASim/Triad.h>

using namespace std;

typedef pair<string, DNASim::Vector3D<Real> > NamedDNASimVector3D;

class LacHalf {
public:
	LacHalf() {};
	LacHalf(const UInteger64& half_number,
			const string& dat_filename,
			const string& pdb_filename,
			const vector<DNASim::Triad>& triads,
			const vector<NamedDNASimVector3D>& atoms,
			const bool reversed) : _half_number(half_number),
								   _dat_filename(dat_filename),
								   _pdb_filename(pdb_filename),
								   _triads(triads),
								   _atoms(atoms),
								   _reversed(reversed) {};
	virtual ~LacHalf() {}

	vector<DNASim::Triad> get_triads_nearest_triad(const DNASim::Triad W) const;

	inline const string& dat_filename()               const { return _dat_filename; }
	inline const string& pdb_filename()               const { return _pdb_filename; }
	inline const vector<DNASim::Triad>& triads()      const { return _triads; }
	inline const vector<NamedDNASimVector3D>& atoms() const { return _atoms; }
	inline bool reversed()                     const { return _reversed; }
	inline int half_number()                   const { return _half_number; }

private:
	string _dat_filename;
	string _pdb_filename;
	vector<DNASim::Triad> _triads;
	vector<NamedDNASimVector3D> _atoms;
	bool _reversed;
	int _half_number;
};

typedef map<pair<int,int>, LacHalf> LacHalfMapping;

#endif /* LACHALF_H_ */
