/*
 * LacHalf.cpp
 *
 *  Created on: Dec 29, 2012
 *      Author: grosner
 */

#include "LacHalf.h"

#include "LacHalfFactory.h"
#include "FileProvider.h"
#include <DNASim/dsMatrix4.h>

vector<DNASim::Triad> LacHalf::get_triads_nearest_triad(const DNASim::Triad W) const {
	vector<DNASim::Triad> ordered_triads;

    auto Wds4 = W.matrix_representation();
    DNASim::Triad front = Wds4 * triads().front().matrix_representation();
    DNASim::Triad back  = Wds4 * triads().back().matrix_representation();

    auto front_distance = (W.origin() - front.origin()).norm();
    auto back_distance  = (W.origin() - back.origin()).norm();

    int midpoint = triads().size()/2;
    UInteger64 start_it, end_it;

    // Get the first half of the steps closest to the given Triad `W`.
    if (front_distance < back_distance) {
        start_it = 0;
        end_it = midpoint;
    }
    else {
        start_it = midpoint;
        end_it = triads().size();
    }

    TriadAxis ta1 = TriadAxis(1);

    for (auto i = start_it; i < end_it; i++) {
        auto t = triads().at(i);
        // To fix an issue where the Phosphate atoms would be on the opposite side,
        // this will rotate the Triad 180 degrees about the major axis.
        if (reversed()) {
            t.set_axis(ta1, -1.0*t[ta1]);
        }
        ordered_triads.push_back(Wds4 * t.matrix_representation());
    }

    return ordered_triads;
}
