/*
 * SecondHalfAlgorithm.h
 *
 *  Created on: May 1, 2013
 *      Author: grosner
 */

#ifndef SECONDHALFALGORITHM_H_
#define SECONDHALFALGORITHM_H_

#include "Forwards.h"
#include "AlgorithmStep.h"
#include <vector>

using namespace std;

class SecondHalfAlgorithm : public AlgorithmStep {
public:
	SecondHalfAlgorithm(LazyIncreasingIterator& seed_provider,
						HalfChainFactoryPtr chain_factory,
						ChainProcessorPtr chain_processor,
			            StatusReporter& status_reporter) : seed_provider(seed_provider),
			            						           chain_factory(chain_factory),
			            						           chain_processor(chain_processor),
			         							           status_reporter(status_reporter) {}
	virtual ~SecondHalfAlgorithm() {};
	virtual void run();
private:
	LazyIncreasingIterator& seed_provider;
	HalfChainFactoryPtr chain_factory;
	ChainProcessorPtr chain_processor;
    StatusReporter& status_reporter;
};

#endif /* SECONDHALFALGORITHM_H_ */
