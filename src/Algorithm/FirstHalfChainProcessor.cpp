/*
 * FirstHalfChainProcessor.cpp
 *
 *  Created on: May 15, 2013
 *      Author: grosner
 */

#include "FirstHalfChainProcessor.h"
#include "StatusReporter.h"
#include "Grid3D.h"

void FirstHalfChainProcessor::process(HalfChain& hc) {
	grid.add_halfchain_to_grid(hc);
	status_reporter.report(hc.seed());
}
