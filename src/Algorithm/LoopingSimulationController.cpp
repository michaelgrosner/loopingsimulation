/*
 * LoopingSimulationController.cpp
 *
 *  Created on: May 18, 2013
 *      Author: grosner
 */

#include "LoopingSimulationController.h"
#include "Includes.h"
#include "BasePairAndStep.h"
#include "SimulationParameters.h"
#include "HalfChainFactory.h"
#include "Grid3D.h"
#include "JFactorReporter.h"
#include "BoundaryCondition.h"
#include "StatusReporter.h"
#include "Closure.h"
#include "HalfChainProteinLocationSampler.h"
#include "LocationCollection.h"
#include "ProteinLocation.h"
#include "LacRepressor.h"
#include "ClosureFactory.h"
#include "SequenceStepToMeanAndStdMapFactory.h"
#include "SequenceFactory.h"
#include "LoopReporter.h"
#include "ProteinPositionsReporter.h"
#include "AlongLoopReporter.h"
#include "BoundaryConditionMapFactory.h"
#include "BoundaryConditionParser.h"
#include "PDBWriter.h"
#include "StepParametersGenerator.h"
#include "FileProvider.h"
#include "FirstHalfAlgorithm.h"
#include "SecondHalfAlgorithm.h"
#include "ThreadGroupManager.h"
#include "FirstHalfChainProcessor.h"
#include "SecondHalfChainProcessor.h"
#include "LoopToDiscreteRibbonConverter.h"
#include "ClosureOverlapDeterminator.h"
#include "ProteinCorpusFactory.h"
#include "StructureFactory.h"
#include "AlgorithmStep.h"
#include "WriteTopologyToStream.h"
#include "MakeClosureImageProcessor.h"
#include "ClosureCSVSummaryGenerator.h"
#include "UniqueClosureIdGenerator.h"
#include "OutputStepsToStream.h"
#include "WriteClosurePdbToStream.h"
#include "AlongClosureCSVGenerator.h"
#include "OptionalLacRepressorRetriever.h"
#include "NullProteinLocationSampler.h"
#include "PlaceProteinsOnSteps.h"
#include "LacHalfFactory.h"
#include "GaussianSampler.h"
#include <boost/algorithm/string/join.hpp>
#include "AcceptWithTemperature.h"
#include "MetropolisMonteCarloSampler.h"
#include "SimulationProgressReporter.h"

void LoopingSimulationController::run() {
	boost::timer first_t, total_t;
	string protein_configuration_info = boost::algorithm::join(params.proteins_configuration, ", ");
	if (protein_configuration_info.empty())
		protein_configuration_info = "No Proteins";
	cout << "Beginning first half" << endl;
	cout << "N Configurations: " << params.n_configurations << endl;
	cout << "N Snapshots: " << params.n_snapshots << endl;
	cout << "Lac: " << params.lac_string << endl;
	cout << "Radi: " << params.radi << ", Twi: " << params.twi << ", Gam: " << params.gam << endl;
	cout << "Protein configuration: " << protein_configuration_info << endl;
	cout << "Sampling Parameters: " << params.sampling_parameters << endl;
	if (params.lac_type == FIXED) {
		cout << "Boundary condition file: " << params.boundary_condition_file << endl;
	}
	else if (params.lac_type == FLEXIBLE) {
		cout << "Flexible Type: " << params.flexible_type << endl;
	}

	cout << "Base Pair Sampler: " << params.sampler_type << endl;
	if (params.sampler_type == "metropolis") {
		cout << "Metropolis Monte Carlo temperature: " << params.temperature << endl;
	}

	UniqueClosureIdGenerator id_generator(params.lac_string, params.proteins_configuration_string);
	SamplingParametersProviderPtr ssmsmf(new SequenceStepToMeanAndStdMapFactory(params.sampling_parameters));
	SequenceFactory sequence_factory(ssmsmf);
	auto dna_sequence = sequence_factory.build(params.dna_sequence);
	LacHalfFactoryPtr lac_half_factory(new LacHalfFactory());
	StreamProviderPtr file_provider(new FileProvider());
	LacAndOrientationFileParser fixed_lac_parser(params.lac_repressor_directory, lac_half_factory, file_provider);
	StreamProviderPtr boundary_condition_file_file_provider(new FileProvider(params.boundary_condition_file));
	auto named_boundaries = BoundaryConditionMapFactory(fixed_lac_parser, boundary_condition_file_file_provider, params.lac_type, params.flexible_type).build();

	map<string, BoundaryCondition> boundaries_by_name;
	for (auto nb = named_boundaries.begin(); nb != named_boundaries.end(); nb++) {
		boundaries_by_name[nb->first.name()] = nb->first;
	}

	Grid3D grid(params);
	auto run_hash = params.run_hash(dna_sequence);
	StreamSinkFactoryPtr file_sink_factory(new FileSinkFactory());
	SimulationProgressReporter progress_reporter(file_sink_factory, params.output_prefix, run_hash);
	JFactorReporter j_factor_reporter(params, dna_sequence, &progress_reporter);
	ClosureCSVSummaryGenerator csv_summary_generator(id_generator);
	LoopReporter loop_reporter(csv_summary_generator, params.n_loops, params.output_prefix, run_hash);
	ProteinPositionsReporter protein_position_reporter(id_generator, params.output_prefix, run_hash);
	AlongClosureCSVGenerator along_closure_csv_generator(id_generator);
	AlongLoopReporter along_loop_reporter(along_closure_csv_generator, params.n_alongs, params.output_prefix, run_hash, dna_sequence, params.n_alongs);
	WriteTopologyToStream topology_writer(file_sink_factory, id_generator, params.output_prefix);
	OutputStepsToStream steps_writer(params.output_prefix, id_generator, file_sink_factory);
	PDBWriter pdb_writer;
	WriteClosurePdbToStream closure_pdb_writer(params.output_prefix, steps_writer, pdb_writer, id_generator);
	MakeClosureImageProcessor make_images_processor(j_factor_reporter, topology_writer, closure_pdb_writer, params.n_snapshots);
	ClosureOverlapDeterminator overlap_determiner;
	ValidClosureProcessor closure_processor(j_factor_reporter, loop_reporter, along_loop_reporter, protein_position_reporter, overlap_determiner, make_images_processor);
	StatusReporter status_reporter_first(params.n_configurations, params.progress_display_interval);
	DiscreteRibbonConverterPtr discrete_ribbon_converter(new LoopToDiscreteRibbonConverter(params.use_through_lac_topology));
	StructureFactory structure_factory;
	ProteinCorpusFactory protein_corpus_factory(params.proteins_configuration, structure_factory);
	auto protein_classes = protein_corpus_factory.build();
	vector<Reporter*> reporters; reporters.push_back(&j_factor_reporter);
								 reporters.push_back(&protein_position_reporter);
								 reporters.push_back(&loop_reporter);
								 reporters.push_back(&along_loop_reporter);
								 reporters.push_back(&progress_reporter);
    BaseOverhangCodeMapFactoryPtr overhang_map_factory(new OverhangCodeMapFactory(dna_sequence, protein_classes));
    OptionalLacRepressorRetrieverPtr optional_lac_retriever(new OptionalLacRepressorRetriever(named_boundaries));
    auto overhang_map = overhang_map_factory->build();

    PlaceProteinsOnStepsPtr protein_placer(new PlaceProteinsOnSteps());

	vector<AlgorithmStepPtr> first_half_algorithms;
	for (UInteger64 thread_id = 0; thread_id < params.n_cores; thread_id++) {
		RandomSamplerPtr rng(new RandomSampler);
		ProteinLocationSamplerPtr protein_location_sampler;
		if (protein_classes.size() > 0)
			protein_location_sampler = ProteinLocationSamplerPtr(new HalfChainProteinLocationSampler(protein_classes, rng, overhang_map, dna_sequence));
		else
			protein_location_sampler = ProteinLocationSamplerPtr(new NullProteinLocationSampler());

		BaseSamplerPtr steps_sampler;
		BaseSamplerPtr gaussian_sampler(new GaussianSampler(rng, dna_sequence));
		if (params.sampler_type == "metropolis") {
			MetropolisMonteCarloAcceptanceCriteriaPtr criteria(new AcceptWithTemperature(rng, params.temperature));
			steps_sampler = BaseSamplerPtr(new MetropolisMonteCarloSampler(rng, criteria, gaussian_sampler, dna_sequence));
		}
		else steps_sampler = gaussian_sampler;

		StepParametersGeneratorPtr steps_generator(new StepParametersGenerator(dna_sequence, protein_placer, steps_sampler));
		HalfChainFactoryPtr chain_factory(new HalfChainFactory(steps_generator, named_boundaries.begin()->first.start(), protein_location_sampler, overhang_map, dna_sequence));
		ChainProcessorPtr first_half_chain_processor(new FirstHalfChainProcessor(grid, status_reporter_first));
		AlgorithmStepPtr first_half(new FirstHalfAlgorithm(i1, chain_factory, first_half_chain_processor));
		first_half_algorithms.push_back(first_half);
	}
	ThreadGroupManager first_half_thread_group(first_half_algorithms);
	first_half_thread_group.run();

	auto first_half_elapsed = first_t.elapsed();
	cout << "Completed first half in " << first_half_elapsed << "s, beginning second half..."  << endl;
	cout << "Speed (chains/second per core): " << (1.0*params.n_configurations)/first_half_elapsed << endl;

	grid.finalize();

	boost::timer second_t;
	StatusReporter status_reporter_second(params.n_configurations, params.progress_display_interval);
	vector<AlgorithmStepPtr> second_half_algorithms;
	for (UInteger64 thread_id = 0; thread_id < params.n_cores; thread_id++) {
		RandomSamplerPtr rng(new RandomSampler);
		ProteinLocationSamplerPtr protein_location_sampler;
		if (protein_classes.size() > 0)
			protein_location_sampler = ProteinLocationSamplerPtr(new HalfChainProteinLocationSampler(protein_classes, rng, overhang_map, dna_sequence));
		else
			protein_location_sampler = ProteinLocationSamplerPtr(new NullProteinLocationSampler());

		BaseSamplerPtr steps_sampler;
		BaseSamplerPtr gaussian_sampler(new GaussianSampler(rng, dna_sequence));
		if (params.sampler_type == "metropolis") {
			MetropolisMonteCarloAcceptanceCriteriaPtr criteria(new AcceptWithTemperature(rng, params.temperature));
			steps_sampler = BaseSamplerPtr(new MetropolisMonteCarloSampler(rng, criteria, gaussian_sampler, dna_sequence));
		}
		else steps_sampler = gaussian_sampler;

		StepParametersGeneratorPtr steps_generator(new StepParametersGenerator(dna_sequence, protein_placer, steps_sampler));
		HalfChainFactoryPtr chain_factory(new HalfChainFactory(steps_generator, named_boundaries.begin()->first.start(), protein_location_sampler, overhang_map, dna_sequence));
		BaseClosureFactoryPtr closure_factory(new ClosureFactory(steps_generator, optional_lac_retriever, protein_location_sampler, discrete_ribbon_converter, dna_sequence));
		ChainProcessorPtr second_half_chain_processor(new SecondHalfChainProcessor(closure_factory, params, named_boundaries, j_factor_reporter, closure_processor, grid, progress_reporter));
		AlgorithmStepPtr second_half(new SecondHalfAlgorithm(i2, chain_factory, second_half_chain_processor, status_reporter_second));
		second_half_algorithms.push_back(second_half);
	}

	ThreadGroupManager second_half_threads(second_half_algorithms);
	second_half_threads.run();

	for (auto reporter = reporters.begin(); reporter != reporters.end(); reporter++) {
		(*reporter)->report();
	}

	cout << "Second half time: " << second_t.elapsed() << "s" << endl;
}
