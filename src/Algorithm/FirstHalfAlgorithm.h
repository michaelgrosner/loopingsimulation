/*
 * FirstHalfAlgorithm.h
 *
 *  Created on: May 1, 2013
 *      Author: grosner
 */

#ifndef FIRSTHALFALGORITHM_H_
#define FIRSTHALFALGORITHM_H_

#include "Forwards.h"
#include "AlgorithmStep.h"

class FirstHalfAlgorithm : public AlgorithmStep {
public:
	FirstHalfAlgorithm(LazyIncreasingIterator& seed_provider,
					   HalfChainFactoryPtr chain_factory,
					   ChainProcessorPtr chain_processor) : seed_provider(seed_provider),
					   	   	   	   	   	   	   	   	   	    chain_factory(chain_factory),
					   	   	   	   	   	   	   	   	   	    chain_processor(chain_processor) {}
	virtual ~FirstHalfAlgorithm() {}
	virtual void run();

private:
	LazyIncreasingIterator& seed_provider;
	HalfChainFactoryPtr chain_factory;
	ChainProcessorPtr chain_processor;
};

#endif /* FIRSTHALFALGORITHM_H_ */
