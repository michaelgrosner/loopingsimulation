/*
 * FirstHalfChainProcessor.h
 *
 *  Created on: May 15, 2013
 *      Author: grosner
 */

#ifndef FIRSTHALFCHAINPROCESSOR_H_
#define FIRSTHALFCHAINPROCESSOR_H_

#include "Forwards.h"
#include "ChainProcessor.h"

class FirstHalfChainProcessor : public ChainProcessor {
public:
	FirstHalfChainProcessor(HalfChainStorage& grid,
							StatusReporter& status_reporter) : grid(grid), status_reporter(status_reporter) {}
	virtual ~FirstHalfChainProcessor() {}
	virtual void process(HalfChain& hc);
private:
	HalfChainStorage& grid;
	StatusReporter& status_reporter;
};

#endif /* FIRSTHALFCHAINPROCESSOR_H_ */
