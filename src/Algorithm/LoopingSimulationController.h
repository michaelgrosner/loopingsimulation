/*
 * LoopingSimulationController.h
 *
 *  Created on: May 18, 2013
 *      Author: grosner
 */

#ifndef LOOPINGSIMULATIONCONTROLLER_H_
#define LOOPINGSIMULATIONCONTROLLER_H_

#include "AlgorithmStep.h"
#include "Forwards.h"

class LoopingSimulationController: public AlgorithmStep {
public:
	LoopingSimulationController(const SimulationParameters& params,
								LazyIncreasingIterator& i1,
								LazyIncreasingIterator& i2) : params(params),
															  i1(i1),
															  i2(i2) {};
	virtual ~LoopingSimulationController() {};
	virtual void run();

private:
	const SimulationParameters& params;
	LazyIncreasingIterator& i1;
	LazyIncreasingIterator& i2;
};

#endif /* LOOPINGSIMULATIONCONTROLLER_H_ */
