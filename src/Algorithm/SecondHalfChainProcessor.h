/*
 * SecondHalfChainProcessor.h
 *
 *  Created on: May 15, 2013
 *      Author: grosner
 */

#ifndef SECONDHALFCHAINPROCESSOR_H_
#define SECONDHALFCHAINPROCESSOR_H_

#include "Forwards.h"
#include "ChainProcessor.h"
#include "BoundaryConditionToLacRepressorsMap.h"

class SimulationProgressReporter;
class BaseJFactorReporter;

class BaseClosureFactory;
typedef boost::shared_ptr<BaseClosureFactory> BaseClosureFactoryPtr;

class SecondHalfChainProcessor : public ChainProcessor {
public:
	SecondHalfChainProcessor(BaseClosureFactoryPtr closure_factory,
							 const SimulationParameters& params,
							 BoundaryConditionToLacRepressorsMap& named_boundaries,
							 BaseJFactorReporter& j_factor_reporter,
							 ClosureProcessor& valid_closure_processor,
							 HalfChainStorage& grid,
							 SimulationProgressReporter& progress_reporter) :
									  params(params),
            						  closure_factory(closure_factory),
									  named_boundaries(named_boundaries),
									  j_factor_reporter(j_factor_reporter),
									  valid_closure_processor(valid_closure_processor),
									  grid(grid),
									  progress_reporter(progress_reporter) {}

	virtual ~SecondHalfChainProcessor() {}
	virtual void process(HalfChain& hc);
private:
	const SimulationParameters& params;
	BaseClosureFactoryPtr closure_factory;
	BoundaryConditionToLacRepressorsMap& named_boundaries;
	BaseJFactorReporter& j_factor_reporter;
    ClosureProcessor& valid_closure_processor;
	HalfChainStorage& grid;
	SimulationProgressReporter& progress_reporter;
};

#endif /* SECONDHALFCHAINPROCESSOR_H_ */
