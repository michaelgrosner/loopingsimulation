/*
 * FirstHalfAlgorithm.cpp
 *
 *  Created on: May 1, 2013
 *      Author: grosner
 */

#include "FirstHalfAlgorithm.h"
#include "Includes.h"
#include "HalfChainFactory.h"
#include "ChainProcessor.h"
#include "LazyIncreasingIterator.h"

void FirstHalfAlgorithm::run() {
	UInteger64 i1;
	while ((i1 = seed_provider.next()) < seed_provider.end()) {
		auto hc = chain_factory->generate_first_half(i1);
		chain_processor->process(hc);
	}
}
