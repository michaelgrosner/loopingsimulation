/*
 * SecondHalfChainProcessor.cpp
 *
 *  Created on: May 15, 2013
 *      Author: grosner
 */

#include "SecondHalfChainProcessor.h"
#include "BasePairAndStep.h"
#include "Grid3D.h"
#include "HalfChain.h"
#include "BoundaryConditionToLacRepressorsMap.h"
#include "ClosureFactory.h"
#include "JFactorReporter.h"
#include "ClosureProcessor.h"
#include "SimulationProgressReporter.h"

void SecondHalfChainProcessor::process(HalfChain& hc) {
	for (auto boundary_it = named_boundaries.begin(); boundary_it != named_boundaries.end(); boundary_it++) {
		auto boundary = boundary_it->first;
		auto second_half_with_boundary = hc.bp() * boundary.end();

		auto query_point = second_half_with_boundary.topRightCorner<3,1>();
		auto seed_matches = grid.find_neighbors_to_point(query_point);

		for (auto seed_match = seed_matches.begin(); seed_match != seed_matches.end(); seed_match++) {

			auto first_half = grid.rebuild_halfchain_from_seed(*seed_match);

			if (first_half.overhang_code() != hc.overhang_code()) continue;

			auto end_to_end_bp = first_half.bp() * second_half_with_boundary;
			auto end_to_end_step = bp_to_step(end_to_end_bp);
			auto rc = end_to_end_step.tail(3).dot(end_to_end_step.tail(3));

			if (rc > params.radi_squared()) continue;
			if (end_to_end_bp(2,3) > 0.0 and (params.lac_type == FIXED or params.lac_type == MINICIRCLE)) continue;
			j_factor_reporter.loop_nears_end_point();

			double angle = 0.0;
			if (params.lac_type == FLEXIBLE) angle = end_to_end_step[1];

			if (params.lac_type == FIXED or params.lac_type == MINICIRCLE) {
				if (end_to_end_bp(2,2) < params.gam) continue;
			}
			else if (params.lac_type == FLEXIBLE) {
				if (fabs(end_to_end_step[0]) > params.trff || angle > 135 || angle < 0) continue;
			}
			else {
				cout << "No recognized lac_type given" << endl;
				throw exception();
			}

			j_factor_reporter.loop_end_is_parallel();

			if (fabs(double(end_to_end_step[2])) > params.twi) continue;
			j_factor_reporter.loop_is_not_overtwisted();

			auto l = closure_factory->generate_loop(first_half, hc, boundary, angle);

			valid_closure_processor.Process(l);
		}
	}
}
