/*
 * HalfChainProcessor.h
 *
 *  Created on: May 15, 2013
 *      Author: grosner
 */

#ifndef HALFCHAINPROCESSOR_H_
#define HALFCHAINPROCESSOR_H_

#include "Forwards.h"

class ChainProcessor {
public:
	virtual ~ChainProcessor() {}
	virtual void process(HalfChain& hc) = 0;
};

#endif /* HALFCHAINPROCESSOR_H_ */
