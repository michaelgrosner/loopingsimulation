/*
 * SecondHalfAlgorithm.cpp
 *
 *  Created on: May 1, 2013
 *      Author: grosner
 */

#include "SecondHalfAlgorithm.h"
#include "Includes.h"
#include "HalfChainFactory.h"
#include "ChainProcessor.h"
#include "LazyIncreasingIterator.h"
#include "StatusReporter.h"
#include "Reporter.h"

void SecondHalfAlgorithm::run() {

	UInteger64 i2;
	while ((i2 = seed_provider.next()) < seed_provider.end()) {

		auto second_halfs = chain_factory->generate_second_half(i2);

		for (auto second_half = second_halfs.begin(); second_half != second_halfs.end(); second_half++) {
			chain_processor->process(*second_half);
		}

		status_reporter.report(i2);

	}
}
