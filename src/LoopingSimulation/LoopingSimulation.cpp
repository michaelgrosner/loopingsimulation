#include "Includes.h"
#include "SimulationParameters.h"
#include "LazyIncreasingIterator.h"
#include "LoopingSimulationController.h"

using namespace boost::program_options;
using namespace std;

int main(int argc, char *argv[]) {

    options_description desc;
    desc.add_options()
        ("help", "produce help")
        ("dna_sequence,s", value<string>()->default_value("Ideal+78"), "Number of steps")
        ("sampling_parameters,q", value<string>()->default_value("Resources/step_params.dat"), "Sampling parameters")
        ("progress_display_interval,v", value<unsigned int>()->default_value(5000), "Interval for progress display")
        ("n_configurations,n", value<UInteger64>()->default_value(5000000), "Number of half chains")
        ("cores,c", value<UInteger64>()->default_value(-1), "Number of threads (CPU cores) to use, -1 =  all available cores")
        ("beta", value<Real>()->default_value(1.0), "Beta")
        ("n_snapshots,x", value<UInteger64>()->default_value(0), "Draw this number of structures")
        ("epsilon,e", value<Real>()->default_value(10.0), "Epsilon value")
        ("radi,r", value<Real>()->default_value(20.0), "radi")
        ("gam,g", value<Real>()->default_value(0.98), "gam")
        ("twi,t", value<Real>()->default_value(11.5), "twist")
        ("flexible_type,f", value<string>()->default_value("A1P1"), "A1P1 or A2P2, used only for flexible looping")
        ("lac,l", value<string>()->default_value("flexible"), "[fixed|flexible|miniciricle] boundaries")
        ("boundary_condition_file,b", value<string>()->default_value("Resources/fixed0.dat"), "Boundary conditions file")
        ("output_prefix,o", value<string>()->default_value("output"), "output directory")
        ("n_alongs,a", value<UInteger64>()->default_value(0), "Number of alongs")
        ("n_loops,w", value<Integer64>()->default_value(-1), "Number of loops to report (negative means all)")
        ("lac_repressor_directory,z", value<string>()->default_value("Resources/Lac"), "Location for Lac files")
        ("sampler_type,m", value<string>()->default_value("gaussian"), "bp sampler")
        ("temperature,d", value<Real>()->default_value(.66), "Temperature")
        ("dont_use_through_lac_topology", bool_switch()->default_value(false), "Use through lac topology")
        ("proteins,p", value<vector<string> >()->multitoken(), "protein description string in format [name+concentration]")
        ("seed,k", value<UInteger64>()->default_value(0), "default seed value");

    SimulationParameters params(desc, argc, argv);
	LazyIncreasingIterator i1(params.seed, params.seed+params.n_configurations);
	LazyIncreasingIterator i2(params.seed, params.seed+params.n_configurations);
	LoopingSimulationController controller(params, i1, i2);

	controller.run();
}
