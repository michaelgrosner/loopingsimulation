/*
 * Includes.h
 *
 *  Created on: Dec 7, 2012
 *      Author: grosner
 */

#ifndef INCLUDES_H_
#define INCLUDES_H_

#include <stdint.h>

typedef int64_t  Seed;
typedef int64_t  Integer64;
typedef uint64_t UInteger64;
typedef double   Real;
typedef uint8_t  OverhangCode;

#endif /* INCLUDES_H_ */
