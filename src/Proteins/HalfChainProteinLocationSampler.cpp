/*
 * HalfChainProteinLocationSampler.cpp
 *
 *  Created on: Dec 22, 2012
 *      Author: grosner
 */

#include "HalfChainProteinLocationSampler.h"

LocationCollection HalfChainProteinLocationSampler::sample_first_half(const Seed& s) {
	rng->reseed(s);
	return _sample(0, dna_sequence->first_half_length(), 1);
}

vector<LocationCollection> HalfChainProteinLocationSampler::sample_second_half(const Seed& s) {
	rng->reseed(s);
	vector<LocationCollection> pls; pls.reserve(1+overhang_map->n_overhang_codes());
                                                
	// Case when no overhung protein present
	pls.push_back(_sample(dna_sequence->first_half_length(), dna_sequence->n_steps(), 2));

	// For each overhung protein from first half...
	for (auto pl_code = overhang_map->begin(); pl_code != overhang_map->end(); pl_code++) {
		auto pl = pl_code->first;
		LocationCollection x; x.push_back(pl);
		auto t = _sample(pl.end_step(), dna_sequence->n_steps(), 2);
		x.insert(x.end(), t.begin(), t.end());

		pls.push_back(x);
	}

	return pls;
}

LocationCollection HalfChainProteinLocationSampler::_sample(const Integer64& placestart, const Integer64& placeend, const int& side) {

	Integer64 cloc = placestart - max_protein_length;
	LocationCollection locs; locs.reserve(2);
	while (true) {
		for (auto pc = 0; pc < protein_classes.size(); pc++) {
			auto protein_class = protein_classes[pc];
			if (rng->sample_uniform() < protein_class->concentration()) {
				if (placestart <= cloc and cloc < placeend) {
					if (side == 2 and cloc > placeend - protein_class->n_steps()) continue;

					auto choice  = (rng->sample_uniform()*protein_class->structures().size());
					auto reversed = rng->sample_uniform() < .5;

					ProteinLocation pl(protein_class->structures()[choice], cloc, reversed);

					locs.push_back(pl);
				}
				cloc += protein_class->n_steps();
			}
		}

		if (cloc < placeend) cloc++;
		else return locs;

	}
}
