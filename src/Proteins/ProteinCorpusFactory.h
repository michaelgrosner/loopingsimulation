/*
 * ProteinCorpusFactory.h
 *
 *  Created on: May 16, 2013
 *      Author: grosner
 */

#ifndef PROTEINCORPUSFACTORY_H_
#define PROTEINCORPUSFACTORY_H_

#include "Includes.h"
#include "Forwards.h"
#include <string>
#include <vector>

using namespace std;

class ProteinCorpusFactory {
public:
	ProteinCorpusFactory(const vector<string>& proteins_configuration,
						 const StructureFactory& structure_factory) : proteins_configuration(proteins_configuration),
						 	 	 	 	 	 	 	 	 	 	 	  structure_factory(structure_factory) {}
	vector<ProteinClassPtr> build() const;
private:
	const vector<string>& proteins_configuration;
	const StructureFactory& structure_factory;
};

#endif /* PROTEINCORPUSFACTORY_H_ */
