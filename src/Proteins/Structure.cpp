/*
 * Structure.cpp
 *
 *  Created on: Dec 16, 2012
 *      Author: grosner
 */

#include "Structure.h"

CAlphasMatrix Structure::get_c_alphas_transformed_by(const BasePair& bp) const {
	CAlphasMatrix new_ca_matrix(_c_alphas.rows(), _c_alphas.cols());
    for (int i = 0; i < _c_alphas.rows(); i++) {
    	new_ca_matrix(i,0) = bp(0,3) + _c_alphas(i,0)*bp(0,0) + _c_alphas(i,1)*bp(0,1) + _c_alphas(i,2)*bp(0,2);
    	new_ca_matrix(i,1) = bp(1,3) + _c_alphas(i,0)*bp(1,0) + _c_alphas(i,1)*bp(1,1) + _c_alphas(i,2)*bp(1,2);
    	new_ca_matrix(i,2) = bp(2,3) + _c_alphas(i,0)*bp(2,0) + _c_alphas(i,1)*bp(2,1) + _c_alphas(i,2)*bp(2,2);
    }
    return new_ca_matrix;
}
