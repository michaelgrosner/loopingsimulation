/*
 * StructureFactory.cpp
 *
 *  Created on: May 16, 2013
 *      Author: grosner
 */

#include "StructureFactory.h"
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <fstream>
#include "BasePairAndStep.h"
#include "Structure.h"
#include <iostream>

StructureFactory::StructureFactory() {}

Real trimmer(const string& s, int start, int dx) {
	return boost::lexical_cast<Real>(boost::algorithm::trim_copy(s.substr(start,dx)));
}

StructurePtr StructureFactory::build(const string protein_class_name,
                                     const string short_name,
                                     const string dat_path,
                                     const string pdb_path) const {

	ifstream infile(pdb_path.c_str());
	std::string line;
	vector<Vector3d> c_alphas_vector;
	while (std::getline(infile, line)) {
	    std::istringstream iss(line);
	    auto pdb_record = iss.str();

	    if (boost::algorithm::trim_copy(pdb_record.substr(11, 5)) != "CA") continue;

	    Vector3d c_alpha(trimmer(pdb_record,30,8),
	    				 trimmer(pdb_record,38,8),
	    				 trimmer(pdb_record,46,8));

	    c_alphas_vector.push_back(c_alpha);
	}

	CAlphasMatrix c_alphas;
	c_alphas.resize(c_alphas_vector.size(), 3);
	for (unsigned int i = 0; i < c_alphas_vector.size(); i++) {
		c_alphas.row(i) = c_alphas_vector[i];
	}

	ifstream steps_file(dat_path.c_str());

	if (!steps_file.is_open()) {
		cout << "Cannot open file " << dat_path.c_str() << endl;
		throw 1;
	}

	StepParametersVector steps, steps_reversed;
	while (steps_file.good()) {
		string line;
		getline(steps_file, line);

		// Handle comments or blank lines
		if (boost::algorithm::starts_with(line, "#") or line.empty()) continue;

		vector<string> tokens;
		boost::algorithm::split(tokens, line, boost::is_any_of(" "));

		if (tokens.size() < 6) continue;

		StepParameters step;
		for (int i = 0; i < 6; i++) {
			step[i] = boost::lexical_cast<Real>(tokens[i]);
		}

		steps.push_back(step);
	}

	steps_reversed.clear();
	for (auto s = steps.rbegin(); s != steps.rend(); s++) {
		StepParameters rstep = *s;
		rstep[0] *= -1.0; rstep[3] *= -1.0;
		steps_reversed.push_back(rstep);
	}

	return StructurePtr(new Structure(protein_class_name, short_name, dat_path, pdb_path, c_alphas, steps, steps_reversed));
}
