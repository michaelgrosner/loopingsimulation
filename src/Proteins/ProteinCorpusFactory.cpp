/*
 * ProteinCorpusFactory.cpp
 *
 *  Created on: May 16, 2013
 *      Author: grosner
 */

#include "ProteinCorpusFactory.h"
#include "StructureFactory.h"
#include "ProteinClass.h"
#include "Structure.h"
#include <glob.h>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/split.hpp>
#include <iostream>

inline std::vector<std::string> glob(const std::string& pat){
    glob_t glob_result;
    glob(pat.c_str(),GLOB_TILDE,NULL,&glob_result);
    vector<string> ret;
    for(unsigned int i=0;i<glob_result.gl_pathc;++i){
        ret.push_back(string(glob_result.gl_pathv[i]));
    }
    globfree(&glob_result);
    return ret;
}

vector<ProteinClassPtr> ProteinCorpusFactory::build() const {
	vector<ProteinClassPtr> protein_classes;
	protein_classes.reserve(proteins_configuration.size());

	for (auto pc_it = proteins_configuration.begin(); pc_it != proteins_configuration.end(); pc_it++) {
		vector<string> tokens;
		boost::algorithm::split(tokens, *pc_it, boost::is_any_of("+"));

		if (tokens.size() != 2) {
			cout << "Invalid protein format " << *pc_it << " should be name+concentration" << endl;
			throw 2;
		}

		boost::filesystem::path resources("Resources/Proteins"), pc_name(tokens[0]), pdb_glob("*.pdb"), dat_glob("*.dat");
		auto dats = resources / pc_name / dat_glob;
		auto pdbs = resources / pc_name / pdb_glob;

		auto dat_filenames = glob(dats.string());
		auto pdb_filenames = glob(pdbs.string());

		vector<StructurePtr> structures;
		for (auto i = 0; i < dat_filenames.size(); i++) {
			auto short_name = boost::filesystem::path(dat_filenames[i]).stem().string();
			auto s_ptr = structure_factory.build(tokens[0], short_name, dat_filenames[i], pdb_filenames[i]);
			structures.push_back(s_ptr);
		}

		auto n_steps = structures.front()->steps().size();
        
        ProteinClassPtr pc(new ProteinClass(n_steps, tokens[0], 1.0/boost::lexical_cast<Real>(tokens[1]), structures));
		protein_classes.push_back(pc);
	}
	return protein_classes;
}
