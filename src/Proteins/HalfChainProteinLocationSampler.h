/*
 * HalfChainProteinLocationSampler.h
 *
 *  Created on: Dec 22, 2012
 *      Author: grosner
 */

#ifndef PROTEINPLACER_H_
#define PROTEINPLACER_H_

#include "Includes.h"
#include "RandomSampler.h"
#include "ProteinClass.h"
#include "ProteinLocation.h"
#include "BaseSequence.h"
#include "LocationCollection.h"
#include "BaseProteinLocationSampler.h"
#include "OverhangCodeMapFactory.h"

typedef map<ProteinLocation, OverhangCode, ProteinLocationComparator> LocationOverhangCodeMap;

class HalfChainProteinLocationSampler : public BaseProteinLocationSampler {
public:
	virtual ~HalfChainProteinLocationSampler() {};
    HalfChainProteinLocationSampler() {}
	HalfChainProteinLocationSampler(const vector<ProteinClassPtr>& protein_classes,
									RandomSamplerPtr rng,
                                    const BaseOverhangCodeMapPtr overhang_map,
									const BaseSequencePtr& dna_sequence) : 	protein_classes(protein_classes),
																			rng(rng),
                                                                            overhang_map(overhang_map),
																			max_protein_length(0),
																			dna_sequence(dna_sequence) {
		for (auto p = protein_classes.begin(); p != protein_classes.end(); p++)
			max_protein_length = max(max_protein_length, (*p)->n_steps());
	}

	virtual LocationCollection sample_first_half(const Seed& s);
	virtual vector<LocationCollection> sample_second_half(const Seed& s);
    
private:
	LocationCollection _sample(const Integer64& placestart, const Integer64& placeend, const int& side);

    UInteger64 max_protein_length;
	BaseSequencePtr dna_sequence;
	vector<ProteinClassPtr> protein_classes;
	RandomSamplerPtr rng;
    BaseOverhangCodeMapPtr overhang_map;
};

#endif /* PROTEINPLACER_H_ */
