/*
 * BaseProteinLocationSampler.h
 *
 *  Created on: May 16, 2013
 *      Author: grosner
 */

#ifndef BASEPROTEINLOCATIONSAMPLER_H_
#define BASEPROTEINLOCATIONSAMPLER_H_

#include "Includes.h"
#include "LocationCollection.h"

using namespace std;

class BaseProteinLocationSampler {
public:
	virtual ~BaseProteinLocationSampler() {}
	virtual LocationCollection sample_first_half(const Seed& s) = 0;
	virtual vector<LocationCollection> sample_second_half(const Seed& s) = 0;
};

typedef boost::shared_ptr<BaseProteinLocationSampler> ProteinLocationSamplerPtr;

#endif /* BASEPROTEINLOCATIONSAMPLER_H_ */
