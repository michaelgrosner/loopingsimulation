/*
 * StructureFactory.h
 *
 *  Created on: May 16, 2013
 *      Author: grosner
 */

#ifndef STRUCTUREFACTORY_H_
#define STRUCTUREFACTORY_H_

#include "Forwards.h"
#include <string>

using namespace std;

class StructureFactory {
public:
	StructureFactory();
	StructurePtr build(const string protein_class_name,
			           const string short_name,
			           const string dat_path,
			           const string pdb_path) const;
};

#endif /* STRUCTUREFACTORY_H_ */
