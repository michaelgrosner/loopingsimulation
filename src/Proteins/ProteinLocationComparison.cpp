/*
 * ProteinLocationComparison.cpp
 *
 *  Created on: May 25, 2013
 *      Author: grosner
 */

#include "ProteinLocationComparison.h"
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include "Structure.h"
#include <set>

typedef boost::tuple<UInteger64, bool, string> plTuple;

bool ProteinLocationComparator::operator() (const ProteinLocation& x, const ProteinLocation& y) const {
  plTuple a(x.start_step(), x.reversed(), x.structure()->protein_class_name());
  plTuple b(y.start_step(), y.reversed(), y.structure()->protein_class_name());
  return a < b;
}
