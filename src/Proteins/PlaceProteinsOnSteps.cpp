/*
 * PlaceProteinsOnSteps.cpp
 *
 *  Created on: May 27, 2013
 *      Author: grosner
 */

#include "PlaceProteinsOnSteps.h"
#include "ProteinLocation.h"

void PlaceProteinsOnSteps::place_proteins_on_steps(const LocationCollection& lc,
												   const unsigned int start,
												   StepParametersVector& steps) {
	for (auto loc = lc.begin(); loc != lc.end(); loc++) {
		auto structure_steps = (loc->reversed() ? loc->structure()->steps_reversed() : loc->structure()->steps());
		for (auto step_i = 0; step_i < structure_steps.size(); step_i++) {
			Integer64 cstep = loc->start_step() + step_i - start;
			if (cstep < 0 || cstep >= steps.size()) continue;
			steps[cstep] = structure_steps[step_i];
		}
	}
}
