/*
 * NullProteinLocationSampler.h
 *
 *  Created on: Apr 1, 2013
 *      Author: grosner
 */

#ifndef NULLPROTEINPLACER_H_
#define NULLPROTEINPLACER_H_

#include "BaseProteinLocationSampler.h"

class NullProteinLocationSampler : public BaseProteinLocationSampler {
	vector<LocationCollection> v;
public:
	NullProteinLocationSampler() {
		v = vector<LocationCollection>();
		v.push_back(LocationCollection());
	};

	LocationCollection sample_first_half(const Seed& s) { return LocationCollection(); }
	vector<LocationCollection> sample_second_half(const Seed& s) { return v; }
};

#endif /* NULLPROTEINPLACER_H_ */
