/*
 * ProteinClass.h
 *
 *  Created on: Dec 16, 2012
 *      Author: grosner
 */

#ifndef PROTEINCLASS_H_
#define PROTEINCLASS_H_

#include "Forwards.h"
#include <boost/shared_ptr.hpp>
#include "Structure.h"
#include <vector>

using namespace std;

class ProteinClass {
public:
	ProteinClass() {}
	ProteinClass(UInteger64 n_steps,
				 string name,
				 Real concentration,
				 vector<StructurePtr> structures) : _n_steps(n_steps),
						 	 	 	 	 	 	 	_name(name),
						 	 	 	 	 	 	 	_concentration(concentration),
						 	 	 	 	 	 	 	_structures(structures) {}

	inline UInteger64 n_steps() const { return _n_steps; }
	inline const string& name() const { return _name; }
	inline Real concentration() const { return _concentration; }
	inline const vector<StructurePtr>& structures() const { return _structures; }

private:
	UInteger64           _n_steps;
	string               _name;
	Real                 _concentration;
	vector<StructurePtr> _structures;
};

typedef boost::shared_ptr<ProteinClass> ProteinClassPtr;

#endif /* PROTEINCLASS_H_ */
