/*
 * PlaceProteinsOnSteps.h
 *
 *  Created on: May 27, 2013
 *      Author: grosner
 */

#ifndef PLACEPROTEINSONSTEPS_H_
#define PLACEPROTEINSONSTEPS_H_

#include "BasePairAndStep.h"
#include "LocationCollection.h"

class PlaceProteinsOnSteps {
public:
	PlaceProteinsOnSteps() {}
	void place_proteins_on_steps(const LocationCollection& lc, const unsigned int start, StepParametersVector& steps);
};

#endif /* PLACEPROTEINSONSTEPS_H_ */
