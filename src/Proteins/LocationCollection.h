/*
 * LocationCollection.h
 *
 *  Created on: May 16, 2013
 *      Author: grosner
 */

#ifndef LOCATIONCOLLECTION_H_
#define LOCATIONCOLLECTION_H_

#include "Includes.h"
#include "Forwards.h"
#include <vector>

typedef std::vector<ProteinLocation> LocationCollection;

#endif /* LOCATIONCOLLECTION_H_ */
