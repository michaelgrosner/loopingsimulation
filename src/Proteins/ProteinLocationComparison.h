/*
 * ProteinLocationComparison.h
 *
 *  Created on: May 25, 2013
 *      Author: grosner
 */

#ifndef PROTEINLOCATIONCOMPARISON_H_
#define PROTEINLOCATIONCOMPARISON_H_

#include "ProteinLocation.h"
#include <set>
#include <functional>

using namespace std;

struct ProteinLocationComparator : binary_function <ProteinLocation,ProteinLocation,bool> {
  bool operator() (const ProteinLocation& x, const ProteinLocation& y) const;
};

typedef set<ProteinLocation, ProteinLocationComparator> ProteinLocationSet;

#endif /* PROTEINLOCATIONCOMPARISON_H_ */
