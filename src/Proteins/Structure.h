/*
 * Structure.h
 *
 *  Created on: Dec 16, 2012
 *      Author: grosner
 */

#ifndef STRUCTURE_H_
#define STRUCTURE_H_

#include "Includes.h"
#include "BasePairAndStep.h"

using namespace std;

class Structure {
public:
	Structure() {}
	Structure(const string protein_class_name,
			  const string short_name,
			  const string dat_path,
			  const string pdb_path,
			  const CAlphasMatrix c_alphas,
			  const StepParametersVector steps,
			  const StepParametersVector steps_reversed) : _protein_class_name(protein_class_name),
			  	  	  	  	  	  	  	  	  	  	  	   _short_name(short_name),
														   _dat_path(dat_path),
														   _pdb_path(pdb_path),
														   _c_alphas(c_alphas),
														   _steps(steps),
														   _steps_reversed(steps_reversed) {}

	CAlphasMatrix get_c_alphas_transformed_by(const BasePair& bp) const;

	inline const string& short_name()                   const { return _short_name; }
	inline const string& pdb_path()                     const { return _pdb_path; }
	inline const string& dat_path()                     const { return _dat_path; }
	inline const string& protein_class_name()           const { return _protein_class_name; }
	inline const StepParametersVector& steps()          const { return _steps; }
	inline const StepParametersVector& steps_reversed() const { return _steps_reversed; }

private:
	string _short_name;
	string _pdb_path;
	string _dat_path;
	string _protein_class_name;
	StepParametersVector _steps;
	StepParametersVector _steps_reversed;
	CAlphasMatrix _c_alphas;
};

#endif /* STRUCTURE_H_ */
