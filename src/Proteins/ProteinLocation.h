/*
 * ProteinLocation.h
 *
 *  Created on: Dec 16, 2012
 *      Author: grosner
 */

#ifndef PROTEINLOCATION_H_
#define PROTEINLOCATION_H_

#include "Includes.h"
#include "Forwards.h"
#include "Structure.h"

class ProteinLocation {
public:
	ProteinLocation() {}
	ProteinLocation(StructurePtr structure,
					const UInteger64 start_step,
					const bool reversed) : _structure(structure),
									       _start_step(start_step),
									       _reversed(reversed) {};

	inline StructurePtr structure()  const { return _structure; }
	inline bool         reversed()   const { return _reversed; }
	inline UInteger64   start_step() const { return _start_step; }
	inline UInteger64   end_step()   const { return start_step() + structure()->steps().size(); }

private:
	StructurePtr _structure;
	bool         _reversed;
	UInteger64   _start_step;
};

#endif /* PROTEINLOCATION_H_ */
