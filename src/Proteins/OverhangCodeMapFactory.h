//
//  OverhangCodeMapFactory.h
//  LoopingSimulation
//
//  Created by Michael on 5/23/13.
//
//

#ifndef __LoopingSimulation__OverhangCodeMapFactory__
#define __LoopingSimulation__OverhangCodeMapFactory__

#include "Includes.h"
#include "Forwards.h"
#include "ProteinLocationComparison.h"
#include "LocationCollection.h"
#include <map>

using namespace std;

class BaseOverhangCodeMap {
public:
    typedef std::map<ProteinLocation, OverhangCode, ProteinLocationComparator> StorageType;
	virtual ~BaseOverhangCodeMap() {}
	virtual void add_protein_location(const ProteinLocation& pl, UInteger64 code) = 0;
    virtual StorageType::const_iterator begin() const = 0;
    virtual StorageType::const_iterator end()   const = 0;
    virtual UInteger64 n_overhang_codes()       const = 0;
	virtual OverhangCode get_overhang_code(const LocationCollection& protein_locations) const = 0;
};
typedef boost::shared_ptr<BaseOverhangCodeMap> BaseOverhangCodeMapPtr;

class BaseOverhangCodeMapFactory {
public:
	virtual ~BaseOverhangCodeMapFactory() {};
	virtual BaseOverhangCodeMapPtr build() const = 0;
};
typedef boost::shared_ptr<BaseOverhangCodeMapFactory> BaseOverhangCodeMapFactoryPtr;

class OverhangCodeMap : public BaseOverhangCodeMap {
public:
	virtual ~OverhangCodeMap() {}
	OverhangCodeMap() : storage() {}
    
    virtual void add_protein_location(const ProteinLocation& pl, UInteger64 code);
	virtual OverhangCode get_overhang_code(const LocationCollection& protein_locations) const;
    virtual StorageType::const_iterator begin() const { return storage.begin(); }
    virtual StorageType::const_iterator end()   const { return storage.end(); }
    virtual UInteger64 n_overhang_codes()       const { return storage.size(); }
private:
	StorageType storage;
};

class OverhangCodeMapFactory : public BaseOverhangCodeMapFactory {
public:
	OverhangCodeMapFactory() {}
	virtual ~OverhangCodeMapFactory() {}
	OverhangCodeMapFactory(BaseSequencePtr dna_sequence,
						   vector<ProteinClassPtr> protein_classes) : dna_sequence(dna_sequence),
								   	   	   	   	   	   	   	   	   	  protein_classes(protein_classes) {}
    
	virtual BaseOverhangCodeMapPtr build() const;
private:
	BaseSequencePtr dna_sequence;
	vector<ProteinClassPtr> protein_classes;
};

#endif /* defined(__LoopingSimulation__OverhangCodeMapFactory__) */
