//
//  OverhangCodeMapFactory.cpp
//  LoopingSimulation
//
//  Created by Michael on 5/23/13.
//
//

#include "OverhangCodeMapFactory.h"
#include "BaseSequence.h"
#include "ProteinClass.h"

void OverhangCodeMap::add_protein_location(const ProteinLocation& pl, UInteger64 code) {
    storage[pl] = code;
}

OverhangCode OverhangCodeMap::get_overhang_code(const LocationCollection& protein_locations) const {
    if (protein_locations.size() == 0) return 0;
    
    auto pl_front = protein_locations.front();
    if (storage.find(pl_front) != storage.end())
        return storage.find(pl_front)->second;
    
    auto pl_back  = protein_locations.back();
    if (protein_locations.size() > 1 && storage.find(pl_back) != storage.end())
        return storage.find(pl_back)->second;
    
    return 0;
}

BaseOverhangCodeMapPtr OverhangCodeMapFactory::build() const {
    UInteger64 code = 1;
    BaseOverhangCodeMapPtr map(new OverhangCodeMap());
    for (unsigned int i = 0; i < protein_classes.size(); i++) {
        ProteinClassPtr pc = protein_classes.at(i);
        for (auto s = dna_sequence->first_half_length() - pc->n_steps() + 1; s < dna_sequence->first_half_length(); s++) {
            for (auto rev = 0; rev < 2; rev++) {
            	ProteinLocation pl(pc->structures().front(), s, rev);
            	map->add_protein_location(pl, code);
                code++;
            }
        }
    }
    return map;
}
