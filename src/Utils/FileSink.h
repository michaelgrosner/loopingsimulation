/*
 * FileSink.h
 *
 *  Created on: May 17, 2013
 *      Author: grosner
 */

#ifndef FILESINK_H_
#define FILESINK_H_

#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>

using namespace std;

class StreamSink {
public:
	virtual void flush() const = 0;
	virtual void add_line(const string& line) = 0;
	virtual ~StreamSink() {}

	string path;
	vector<string> lines;
};
typedef boost::shared_ptr<StreamSink> StreamSinkPtr;

class FileSink : public StreamSink {
public:
	FileSink() {}
	FileSink(const string in_path) { path = in_path; }
	virtual void flush() const;
	virtual void add_line(const string& line);
	virtual ~FileSink() {}
};

class StringSink : public StreamSink {
public:
	StringSink() {}
	StringSink(const string in_path) { path = in_path; }

	virtual void flush() const {}
	virtual void add_line(const string& line);
	virtual ~StringSink() {}
};

class StreamSinkFactory {
public:
	virtual ~StreamSinkFactory() {}
	virtual StreamSinkPtr create_sink_with_path(const string path) = 0;
};
typedef boost::shared_ptr<StreamSinkFactory> StreamSinkFactoryPtr;

class FileSinkFactory : public StreamSinkFactory {
public:
	virtual ~FileSinkFactory() {}
	virtual StreamSinkPtr create_sink_with_path(const string path) {
		return StreamSinkPtr(new FileSink(path));
	};
};

class StringSinkFactory : public StreamSinkFactory {
public:
	virtual ~StringSinkFactory() {}
	StringSinkFactory(StreamSinkPtr stream_to_save) : stream_to_save(stream_to_save) {}
	virtual StreamSinkPtr create_sink_with_path(const string path) {
		stream_to_save->path = path;
		return stream_to_save;
	};

	StreamSinkPtr stream_to_save;
};


#endif /* FILESINK_H_ */
