/*
 * Algorithm.h
 *
 *  Created on: May 1, 2013
 *      Author: grosner
 */

#ifndef ALGORITHM_H_
#define ALGORITHM_H_

class AlgorithmStep {
public:
	virtual ~AlgorithmStep() {}
	virtual void run() = 0;
};

#endif /* ALGORITHM_H_ */
