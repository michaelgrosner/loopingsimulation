#include "FileProvider.h"

#include <boost/algorithm/string.hpp>
#include <boost/tokenizer.hpp>
#include <fstream>
#include <iostream>

void FileProvider::set_input(const string& input) {
	filepath = input;
}

vector<string> FileProvider::consume() const {
	ifstream stream(filepath.c_str());
	if (!stream.is_open()) {
		cout << "Cannot open " << filepath << " for reading!" << endl;
		throw 400;
	}
	vector<string> lines;
	while (stream.good()) {
		string line;
		getline(stream, line);

		// Handle comments or blank lines
		if (boost::algorithm::starts_with(line, "#") or line.empty()) continue;

		lines.push_back(line);
	}
	return lines;
};

vector<string> StringProvider::consume() const {
	boost::char_separator<char> sep("\n");
	boost::tokenizer<boost::char_separator<char> > tokens(to_return, sep);
	vector<string> strings;
	for (auto line = tokens.begin(); line != tokens.end(); line++) {
		if (boost::algorithm::starts_with(*line, "#") or line->empty()) continue;
		strings.push_back(*line);
	}
	return strings;
}
