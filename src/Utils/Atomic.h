/*
 * Atomic.h
 *
 *  Created on: May 25, 2013
 *      Author: grosner
 */

#ifndef ATOMIC_H_
#define ATOMIC_H_

#ifdef __APPLE__
	#include <boost/atomic.hpp>
	namespace boost_or_std = boost;
#endif

// http://stackoverflow.com/questions/1936719/what-are-the-gcc-preprocessor-flags-for-the-compilers-version-number
#ifdef __linux__
	#if (__GNUC__ == 4 && __GNUC_MINOR__ == 4)
		#include <cstdatomic>
	#else
		#include <atomic>
	#endif
	namespace boost_or_std = std;
#endif

#endif /* ATOMIC_H_ */
