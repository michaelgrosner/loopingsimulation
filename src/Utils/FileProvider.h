/*
 * FileProvider.h
 *
 *  Created on: Apr 11, 2013
 *      Author: grosner
 */

#ifndef FILEPROVIDER_H_
#define FILEPROVIDER_H_

#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>

using namespace std;

class StreamProvider {
public:
	virtual vector<string> consume() const = 0;
	virtual void set_input(const string& input) = 0;
	virtual ~StreamProvider() {}
};

typedef boost::shared_ptr<StreamProvider> StreamProviderPtr;

class FileProvider : public StreamProvider {
public:
	FileProvider() {}
	FileProvider(const string& filepath) : filepath(filepath) {}
	virtual ~FileProvider() {}

	virtual void set_input(const string& input);
	virtual vector<string> consume() const;

	string filepath;
};

class StringProvider : public StreamProvider {
public:
	StringProvider() {}
	StringProvider(const string& to_return) : to_return(to_return) {}
	virtual ~StringProvider() {}

	virtual void set_input(const string& input) {}
	virtual vector<string> consume() const;

	string to_return;
};


#endif /* FILEPROVIDER_H_ */
