/*
 * ThreadGroupManager.h
 *
 *  Created on: May 2, 2013
 *      Author: grosner
 */

#ifndef THREADGROUPMANAGER_H_
#define THREADGROUPMANAGER_H_

#include "Includes.h"
#include "AlgorithmStep.h"
#include <vector>
#include <boost/bind.hpp>
#include <boost/thread.hpp>

class ThreadGroupManager {
public:
	ThreadGroupManager(vector<AlgorithmStepPtr>& algorithms) : algorithms(algorithms) {}

	void run() {
		boost::thread_group thread_group;
		for (int thread_id = 0; thread_id < algorithms.size(); thread_id++) {
			thread_group.create_thread(boost::bind(&AlgorithmStep::run, algorithms[thread_id]));
		}
		thread_group.join_all();
	}
private:
	vector<AlgorithmStepPtr>& algorithms;
};

#endif /* THREADGROUPMANAGER_H_ */
