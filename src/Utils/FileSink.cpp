#include "FileSink.h"
#include <fstream>

void FileSink::flush() const {
	ofstream f;
	f.open(path.c_str());
	for (auto line = lines.begin(); line != lines.end(); line++) {
		f << *line;
	}
	f.close();
}

void FileSink::add_line(const string& line) {
	lines.push_back(line);
}

void StringSink::add_line(const string& line) {
	lines.push_back(line);
}
