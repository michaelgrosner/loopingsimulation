/*
 * LazyIncreasingIterator.h
 *
 *  Created on: May 1, 2013
 *      Author: grosner
 */

#ifndef LAZYINCREASINGITERATOR_H_
#define LAZYINCREASINGITERATOR_H_

#include "Includes.h"
#include "Atomic.h"

class LazyIncreasingIterator {
public:
	LazyIncreasingIterator(UInteger64 start, UInteger64 stop) : v(start), stop(stop) {}

	UInteger64 next() { return v++; }
	UInteger64 end() const { return stop; }

private:
	boost_or_std::atomic<UInteger64> v;
	UInteger64 stop;
};


#endif /* LAZYINCREASINGITERATOR_H_ */
