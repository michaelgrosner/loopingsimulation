/*
 * MutexLockGuard.h
 *
 *  Created on: May 25, 2013
 *      Author: grosner
 */

#ifndef MUTEXLOCKGUARD_H_
#define MUTEXLOCKGUARD_H_

#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>

typedef boost::lock_guard<boost::mutex> MutexLockGuard;

#endif /* MUTEXLOCKGUARD_H_ */
