#include <Includes.h>
#include <SimulationParameters.h>
#include <BasePairAndStep.h>
#include <HalfChainFactory.h>
#include <RandomSampler.h>
#include <BoundaryCondition.h>
#include <BaseSequence.h>
#include <SequenceFactory.h>
#include <SequenceStepToMeanAndStdMapFactory.h>
#include <StatusReporter.h>
#include <TunableSamplingParametersProvider.h>
#include <StepParametersGenerator.h>
#include <StructureFactory.h>
#include <ProteinCorpusFactory.h>
#include <HalfChainProteinLocationSampler.h>
#include <PlaceProteinsOnSteps.h>
#include <GaussianSampler.h>
#include <google/dense_hash_map>

typedef google::dense_hash_map<Integer64, vector<Real> > LDMap;

pair<Integer64, Real> angle_calculator(const BasePair& x) {
	Integer64 d = x.topRightCorner<3,1>().norm();
	Vector3d a = x.block(0, 2, 3, 1);
	Vector3d b = Vector3d::Zero(); b[2] = 1;
	Real num = a.dot(b);
	Real den = a.norm()*b.norm();
	return make_pair(d, 57.2957795*acos(num/den));
}

int main(int argc, char *argv[]) {
	using namespace boost::program_options;

    options_description desc;
    desc.add_options()
        ("help", "produce help")
        ("dna_sequence,s", value<string>()->default_value("Ideal+78"), "Number of steps")
        ("sampling_parameters,q", value<string>()->default_value("Resources/step_params.dat"), "Sampling parameters")
        ("progress_display_interval,v", value<unsigned int>()->default_value(5000), "Interval for progress display")
        ("n_configurations,n", value<UInteger64>()->default_value(5000000), "Number of half chains")
        ("chunksize,c", value<UInteger64>()->default_value(5000), "Chunksize for threading")
        ("n_snapshots,x", value<UInteger64>()->default_value(0), "Draw this number of structures")
        ("epsilon,e", value<Real>()->default_value(10.0), "Epsilon value")
        ("radi,r", value<Real>()->default_value(20.0), "radi")
        ("gam,g", value<Real>()->default_value(0.98), "gam")
        ("twi,t", value<Real>()->default_value(11.5), "twist")
        ("flexible_type,f", value<string>()->default_value("A1P1"), "A1P1 or A2P2, used only for flexible looping")
        ("lac,l", value<string>()->default_value("flexible"), "[fixed|flexible|miniciricle] boundaries")
        ("boundary_condition_file,b", value<string>()->default_value("Resources/fixed0.dat"), "Boundary conditions file")
        ("output_prefix,o", value<string>()->default_value("output"), "output directory")
        ("proteins,p", value<vector<string> >()->multitoken(), "protein description string in format [name+concentration]");

    SimulationParameters params(desc, argc, argv);
    StatusReporter status_reporter(params.n_configurations, params.progress_display_interval);

    StepParameters mean;
    mean << 0.0, 0.0, 34.28, 0.0, 0.0, 3.4;

	SamplingParametersProviderPtr tunable_sampling_parameters_provider(new TunableSamplingParametersProvider(mean, 4.5));
	SequenceFactory sequence_factory(tunable_sampling_parameters_provider);
	BaseSequencePtr dna_sequence = sequence_factory.build(params.dna_sequence);

	StructureFactory structure_factory;
	ProteinCorpusFactory protein_corpus_factory(params.proteins_configuration, structure_factory);
	auto protein_classes = protein_corpus_factory.build();
    BaseOverhangCodeMapFactoryPtr overhang_map_factory(new OverhangCodeMapFactory(dna_sequence, protein_classes));
    auto overhang_map = overhang_map_factory->build();
    
    RandomSamplerPtr sampler(new RandomSampler);
    ProteinLocationSamplerPtr null_location_sampler(new HalfChainProteinLocationSampler(protein_classes, sampler, overhang_map, dna_sequence));
    PlaceProteinsOnStepsPtr protein_placer(new PlaceProteinsOnSteps());
    BaseSamplerPtr steps_sampler(new GaussianSampler(sampler, dna_sequence));
    StepParametersGeneratorPtr steps_generator(new StepParametersGenerator(dna_sequence, protein_placer, steps_sampler));
	HalfChainFactory chain_factory(steps_generator, BasePair::Identity(), null_location_sampler, overhang_map, dna_sequence);

	BasePair accumulator = BasePair::Zero();
	for (UInteger64 i = 0; i < params.n_configurations; i++) {
		accumulator += chain_factory.generate_first_half(i).bp();
		status_reporter.report(i);
	}
	accumulator /= params.n_configurations;

	BasePair P = BasePair::Identity();
	for (UInteger64 i = 0; i < 10000; i++) P *= accumulator;

	cout << "Persistence Length = " << P(2,3) << endl;
}
