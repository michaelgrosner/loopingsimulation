/*
 * HalfChainProteinLocationSamplerTest.cpp
 *
 *  Created on: Dec 28, 2012
 *      Author: grosner
 */


#include "TestHelpers.h"

#include "RandomSampler.h"
#include "HalfChainProteinLocationSampler.h"
#include "BaseSequence.h"
#include "LocationCollection.h"
#include "ProteinCorpusFactory.h"
#include "OverhangCodeMapFactory.h"
#include "StructureFactory.h"
#include "NullProteinLocationSampler.h"

class HalfChainProteinLocationSamplerTest : public ::testing::Test {
protected:
	RandomSamplerPtr rng;
	BaseSequencePtr iseq;
	vector<ProteinClassPtr> hu_classes, hu_and_fis_classes;
    BaseOverhangCodeMapPtr overhang_map;

	HalfChainProteinLocationSamplerTest() :
		rng(new RandomSampler),
		iseq(new BaseSequence(78)),
		hu_classes(ProteinCorpusFactory(simulation_parameters_with_HU().proteins_configuration, StructureFactory()).build()),
        overhang_map(OverhangCodeMapFactory(iseq, hu_classes).build()) {}
};

TEST_F(HalfChainProteinLocationSamplerTest,ShouldGenerateCorrectNumberOfLocationCollections) {
	HalfChainProteinLocationSampler placer(hu_classes, rng, overhang_map, iseq);
	auto lcs = placer.sample_second_half(0);
	ASSERT_EQ(27, lcs.size());
}

TEST_F(HalfChainProteinLocationSamplerTest, ShouldOnlyGiveOneLocationCollectionWhenNoProteins) {
	HalfChainProteinLocationSampler placer(vector<ProteinClassPtr>(), rng, BaseOverhangCodeMapPtr(new OverhangCodeMap()), iseq);
	auto lcs = placer.sample_second_half(0);
	ASSERT_EQ(1, lcs.size());

	NullProteinLocationSampler null_placer;
	auto null_lcs = null_placer.sample_second_half(0);
	ASSERT_EQ(1, null_lcs.size());
}
