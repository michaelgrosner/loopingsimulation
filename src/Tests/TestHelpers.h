/*
 * TestHelpers.h
 *
 *  Created on: Dec 28, 2012
 *      Author: grosner
 */

#ifndef TESTHELPERS_H_
#define TESTHELPERS_H_

#include "Includes.h"
#include "Forwards.h"
#include <gtest/gtest.h>

#include "HalfChain.h"
#include "SimulationParameters.h"
#include "FileProvider.h"

using namespace boost::program_options;

template <typename T>
void assert_matrix_equals_or_print_and_throw(const T& a, const T& b) {
	if ((a-b).norm() > 5e-2) {
		cout << a.transpose() << endl;
		cout << b.transpose() << endl;
		cout << (a-b).norm() << endl;
		EXPECT_FALSE(true);
	}
	else {
		EXPECT_FALSE(false);
	}
}

class VerificationStreamProvider : public StreamProvider {
public:
	VerificationStreamProvider() {}
	virtual vector<string> consume() const { return vector<string>(); }
	virtual void set_input(const string& input) { _count++; }
	int count() const { return _count; }
private:
	int _count;
};

StepParameters random_step();
HalfChain half_chain_without_proteins();

SimulationParameters base_parameters();
SimulationParameters simulation_parameters_with_HU_and_Fis();
SimulationParameters simulation_parameters_with_HU();
SimulationParameters simulation_parameters_without_Proteins();

variables_map default_options();

int test_n_steps_in_loop();

#endif /* TESTHELPERS_H_ */
