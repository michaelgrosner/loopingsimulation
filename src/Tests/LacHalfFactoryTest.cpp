#include "TestHelpers.h"
#include "LacHalfFactory.h"
#include "LacHalf.h"

using namespace DNASim;

class LacHalfFactoryTest : public ::testing::Test {
protected:
	LacHalfFactory factory;
	string fake_file_string;
	StreamProviderPtr fake_file;

	LacHalf lac;
	UInteger64 half_number;
	string pdb_filename, dat_filename;

	virtual void SetUp() {
		fake_file_string = "ATOM;A;{63.238,-21.426,-52.408}                                                                     \n"
				     "# Random comment!																							\n"
		             "ATOM;B;{39.676,-42.539,-64.546}                                                                           \n"
		             "TRIAD;{-0.0004,-0.0001,0.0002};{1.0,-0.0003,-0.0006};{-0.0003,-1.0,-0.0003};{-0.0006,0.0003,-1.0}         \n"
		             "TRIAD;{-0.6462,0.1542,-3.2674};{0.8261,-0.5505,0.1202};{-0.5538,-0.8326,-0.0075};{0.1042,-0.0603,-0.9927} \n"
		             "TRIAD;{-0.4007,0.2071,-6.3622};{0.4289,-0.9016,0.0559};{-0.9023,-0.4306,-0.0222};{0.044,-0.0409,-0.9982}  \n"
		             "TRIAD;{0.5198,-0.3589,-9.7849};{-0.2217,-0.9747,0.0284};{-0.9626,0.2141,-0.1661};{0.1558,-0.0641,-0.9857} \n"
		             "TRIAD;{1.7755,-1.7147,-13.1579};{-0.6796,-0.7334,0.0176};{-0.729,0.6724,-0.1283};{0.0823,-0.1,-0.9916}    \n"
		             "TRIAD;{2.409,-2.9974,-16.229};{-0.9668,-0.2432,0.0783};{-0.2506,0.9624,-0.1045};{-0.05,-0.1207,-0.9914}   \n"
		             "TRIAD;{3.8797,-3.0199,-19.2425};{-0.9028,0.4185,-0.0989};{0.43,0.8815,-0.1951};{0.0056,-0.2187,-0.9758}   \n"
		             "TRIAD;{3.6844,-1.1046,-23.4705};{-0.69,0.5671,0.4498};{0.5104,0.8218,-0.2533};{-0.5133,0.0548,-0.8565}    \n"
		             "TRIAD;{2.2355,0.4447,-25.9457};{-0.2495,0.9514,0.1804};{0.8713,0.3018,-0.387};{-0.4227,0.0606,-0.9043}    \n"
		             "TRIAD;{-0.3557,0.8962,-28.423};{0.3195,0.9443,0.0789};{0.8316,-0.2395,-0.501};{-0.4542,0.2257,-0.8618}    \n"
		             "TRIAD;{-3.1132,2.006,-30.7174};{0.7199,0.6696,-0.1827};{0.5191,-0.6942,-0.4986};{-0.4607,0.2641,-0.8473}  \n"
		             "TRIAD;{-5.0915,3.4727,-33.5634};{0.9219,0.1166,-0.3695};{0.0125,-0.9622,-0.2722};{-0.3873,0.2463,-0.8885} \n"
		             "TRIAD;{-6.213,4.1373,-36.2933};{0.7563,-0.3656,-0.5426};{-0.4706,-0.8801,-0.0628};{-0.4545,0.3028,-0.8377}\n"
		             "TRIAD;{-7.0458,4.4402,-39.6612};{0.4045,-0.8472,-0.3443};{-0.8544,-0.4844,0.1882};{-0.3262,0.2181,-0.9198}\n";
		fake_file = StreamProviderPtr(new StringProvider(fake_file_string));
		pdb_filename = "/baz/pdb.pdb";
		dat_filename = "/foo/bar/dat.dat";
		half_number = 2;
		lac = factory.build(fake_file, dat_filename, pdb_filename, half_number);
	}
};

TEST_F(LacHalfFactoryTest, ShouldLoadLacDatFileNameCorrectly) {
	ASSERT_EQ(lac.dat_filename(), dat_filename);
}

TEST_F(LacHalfFactoryTest, ShouldLoadLacPdbFileNameCorrectly) {
	ASSERT_EQ(lac.pdb_filename(), pdb_filename);
}

TEST_F(LacHalfFactoryTest, ShouldLoadHalfNumberCorrectly) {
	ASSERT_EQ(lac.half_number(), half_number);
}

TEST_F(LacHalfFactoryTest, ShouldLoadAtomsCorrectly) {
	ASSERT_EQ(lac.atoms()[0].first, "A");
	ASSERT_EQ(lac.atoms()[1].first, "B");
	ASSERT_EQ(lac.atoms()[0].second, Vector3D<Real>(63.238,-21.426,-52.408));
	ASSERT_EQ(lac.atoms()[1].second, Vector3D<Real>(39.676,-42.539,-64.546));
}

TEST_F(LacHalfFactoryTest, ShouldLoadTriadsCorrectly) {
	Vector3D<Real> o(-7.0458,4.4402,-39.6612);
	Vector3D<Real> x(0.4045,-0.8472,-0.3443);
	Vector3D<Real> y(-0.8544,-0.4844,0.1882);
	Vector3D<Real> z(-0.3262,0.2181,-0.9198);

	Triad t = lac.triads().back();
	EXPECT_LT((t.origin()-o).norm(), 1e-6);
	EXPECT_LT((t.axis(I)-x).norm() , 1e-6);
	EXPECT_LT((t.axis(J)-y).norm() , 1e-6);
	EXPECT_LT((t.axis(K)-z).norm() , 1e-6);

}
