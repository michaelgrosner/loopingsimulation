/*
 * LoopFactoryTest.cpp
 *
 *  Created on: Dec 29, 2012
 *      Author: grosner
 */

#include "TestHelpers.h"

#include "BaseSequence.h"
#include "Grid3D.h"
#include "HalfChain.h"
#include "HalfChainFactory.h"
#include "BasePairAndStep.h"
#include "RandomSampler.h"
#include "BoundaryConditionMapFactory.h"
#include "BoundaryConditionParser.h"
#include "HalfChainProteinLocationSampler.h"
#include "BoundaryCondition.h"
#include "ClosureFactory.h"
#include "LacRepressor.h"
#include "StepParametersGenerator.h"
#include "LacHalf.h"
#include "LoopToDiscreteRibbonConverter.h"
#include "StructureFactory.h"
#include "ProteinCorpusFactory.h"
#include "Closure.h"
#include "OptionalLacRepressorRetriever.h"
#include "PlaceProteinsOnSteps.h"
#include "GaussianSampler.h"

int n_steps = 80;

class ClosureFactoryTest : public ::testing::Test {
public:
	SimulationParameters sp;
	BoundaryConditionToLacRepressorsMap bc_map;
	BaseSequencePtr iseq;
	RandomSamplerPtr rng;
	ProteinLocationSamplerPtr pp_with_proteins, pp_without_proteins;
	StepParametersGeneratorPtr steps_generator;
	HalfChainFactory hcf_with_proteins, hcf_without_proteins;
	ClosureFactory lf_with_proteins, lf_without_proteins;
	HalfChain hc1, hc2;
	ClosurePtr l_without_proteins, l_with_proteins;
	BoundaryCondition bc;
	Seed s1, s2;

	virtual void SetUp() {
		sp = simulation_parameters_with_HU();
		BoundaryCondition A1_boundary(StepParameters(), "A1");
		vector<DNASim::Triad> triads(14, DNASim::Triad());
		vector<NamedDNASimVector3D> named_atoms(2, NamedDNASimVector3D("A", DNASim::Vector3D<Real>(1,2,3)));
		LacHalf symmetric_half1(1, "1.dat", "1.pdb", triads, named_atoms, false);
		LacHalf symmetric_half2(2, "2.dat", "2.pdb", triads, named_atoms, false);
		LacRepressor A1_repressor("A1", symmetric_half1, symmetric_half2);
		bc_map[A1_boundary]  = A1_repressor;
		OptionalLacRepressorRetrieverPtr retriever(new OptionalLacRepressorRetriever(bc_map));
		iseq = BaseSequencePtr(new BaseSequence(test_n_steps_in_loop()));
		rng = RandomSamplerPtr(new RandomSampler);
		BaseSamplerPtr steps_sampler(new GaussianSampler(rng, iseq));
		PlaceProteinsOnStepsPtr protein_placer(new PlaceProteinsOnSteps);
		steps_generator = StepParametersGeneratorPtr(new StepParametersGenerator(iseq, protein_placer, steps_sampler));

		DiscreteRibbonConverterPtr discrete_ribbon_converter(new LoopToDiscreteRibbonConverter(true));

		auto protein_classes = ProteinCorpusFactory(sp.proteins_configuration, StructureFactory()).build();
        BaseOverhangCodeMapFactoryPtr overhang_map_factory(new OverhangCodeMapFactory(iseq, protein_classes));
        auto overhang_codes = overhang_map_factory->build();

		pp_with_proteins = ProteinLocationSamplerPtr(new HalfChainProteinLocationSampler(protein_classes, rng, overhang_codes, iseq));
		hcf_with_proteins = HalfChainFactory(steps_generator, BasePair::Identity(), pp_with_proteins, overhang_codes, iseq);
		lf_with_proteins = ClosureFactory(steps_generator, retriever, pp_with_proteins, discrete_ribbon_converter, iseq);

        BaseOverhangCodeMapPtr no_codes(new OverhangCodeMap());
		pp_without_proteins = ProteinLocationSamplerPtr(new HalfChainProteinLocationSampler(vector<ProteinClassPtr>(), rng, no_codes, iseq));
		hcf_without_proteins = HalfChainFactory(steps_generator, BasePair::Identity(), pp_without_proteins, no_codes, iseq);
		lf_without_proteins = ClosureFactory(steps_generator, retriever, pp_without_proteins, discrete_ribbon_converter, iseq);

		// without proteins
		s1 = 1056805;
		s2 = 556102;
		hc1 = hcf_without_proteins.generate_first_half(s1);
		hc2 = hcf_without_proteins.generate_second_half(s2)[0];

		bc = bc_map.begin()->first;
		l_without_proteins = lf_without_proteins.generate_loop(hc1, hc2, bc, 0.0);

		// with proteins
		Seed x = rng->sample_uniform()*100000;
		while (true) {
			hc1 = hcf_with_proteins.generate_first_half(x);
			if (hc1.overhang_code() != 0) break;
			x++;
		}

		Seed y = rng->sample_uniform()*100000;
		vector<HalfChain> second_halfs = hcf_with_proteins.generate_second_half(y);

		for (unsigned int i = 0; i < second_halfs.size(); i++) {
			hc2 = second_halfs[i];
			if (hc1.overhang_code() == hc2.overhang_code()) break;
		}

		bc = bc_map.begin()->first;
		l_with_proteins = lf_with_proteins.generate_loop(hc1, hc2, bc, 0.0);
	};

};

TEST_F(ClosureFactoryTest, LoopGeneratedShouldHaveLacRepressorWithSameNameAsBoundary) {
	string actual = bc.name();
	string expected = l_with_proteins->boundary().name();
	ASSERT_EQ(actual, expected);
}

TEST_F(ClosureFactoryTest, CanJoinCombineTwoChainsAndGetCorrectSizeVectors) {
	ASSERT_EQ(l_with_proteins->steps().size(), iseq->n_steps());
	ASSERT_EQ(l_with_proteins->bps().size(), iseq->n_steps()+1);
}

TEST_F(ClosureFactoryTest, OverhungProteinShouldOnlyAppearOnceInLoop) {
	int expected = l_with_proteins->protein_locations().size();
	int actual = hc1.protein_locations().size() + hc2.protein_locations().size() - 1;
	ASSERT_EQ(expected, actual);
}

TEST_F(ClosureFactoryTest, ShouldGetSameStepsWhenNoProteins) {
	StepParametersVector hc_steps = steps_generator->generate_steps_vector(s1,iseq->first_half_length(), 0, LocationCollection());
	StepParametersVector hc2_steps = steps_generator->generate_steps_vector(s2,iseq->n_steps(), iseq->first_half_length(), LocationCollection());
	hc_steps.insert(hc_steps.end(), hc2_steps.begin(), hc2_steps.end());

	ASSERT_EQ(hc_steps.size(), l_without_proteins->steps().size());

	for (unsigned int i = 0; i < hc_steps.size(); i++) {
		assert_matrix_equals_or_print_and_throw(hc_steps[i], l_without_proteins->steps().at(i));
	}
}

TEST_F(ClosureFactoryTest, IdealSeqAndLoopHaveSameNumberOfSteps) {
	ASSERT_EQ(iseq->n_steps(), l_without_proteins->steps().size());
}


