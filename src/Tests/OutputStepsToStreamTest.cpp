#include "TestHelpers.h"
#include "OutputStepsToStream.h"
#include "FileProvider.h"
#include "FileSink.h"
#include <gmock/gmock.h>

using ::testing::_;
using ::testing::Return;

//class TestIdGenerator : public ClosureStringGenerator {
//public:
//	MOCK_CONST_METHOD1(generate, string(const ClosurePtr& closure));
//};
//
//class OutputStepsToStreamTest : public ::testing::Test {
//protected:
//	OutputStepsToStream processor;
//	ClosurePtr closure;
//	StreamSinkPtr sink;
//	StreamSinkFactoryPtr sink_factory;
//	TestIdGenerator id_generator;
//	string generated_id;
//
//	OutputStepsToStreamTest() :
//		sink(new StringSink()),
//		sink_factory(new StringSinkFactory(sink)),
//		id_generator(),
//		processor("test_path", id_generator, sink_factory) {}
//
//	virtual void SetUp() {
//		StepParametersVector steps;
//		StepParameters step;
//		step << 1, 2, 3, 4, 5, 6;
//		steps.push_back(step);
//
//		generated_id = "ID,ID";
//		EXPECT_CALL(id_generator, generate(_)).WillRepeatedly(Return(generated_id));
//
//		closure = ClosurePtr(new Closure(BoundaryCondition(StepParameters(), "A1"),
//								    HalfChain(BasePair(), LocationCollection(), 1, 0),
//								    HalfChain(BasePair(), LocationCollection(), 2, 0),
//								    BasePairVector(),
//								    steps,
//								    LocationCollection(),
//								    DNASim::DiscreteRibbon(DNASim::CurvePoints(), DNASim::CurvePoints()),
//								    OptionalLacRepressorAndAngle(LacRepressorAndAngle(LacRepressor(), 10)),
//								    BaseSequencePtr(new IdealSequence(78)),
//								    20));
//	}
//};
//
//TEST_F(OutputStepsToStreamTest, ShouldMakeANice3DNAStepsFile) {
//	processor.Process(closure);
//
//	vector<string> expected_lines;
//	expected_lines.push_back("  2 base-pairs");
//    expected_lines.push_back("   0  step parameters");
//    expected_lines.push_back("      shift   slide    rise    tilt    roll   twist");
//    expected_lines.push_back("A-T   0.000   0.000   0.000   0.000   0.000   0.000");
//    expected_lines.push_back("A-T   4.000   5.000   6.000   1.000   2.000   3.000");
//
//	ASSERT_EQ(sink->lines, expected_lines);
//	ASSERT_EQ(sink->path, "test_path/steps_ID,ID.dat");
//}
