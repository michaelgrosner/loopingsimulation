/*
 * TestHelpers.cpp
 *
 *  Created on: Dec 28, 2012
 *      Author: grosner
 */

#include "TestHelpers.h"
#include "BaseSequence.h"

StepParameters random_step() {
	SimulationParameters sp = base_parameters();
	StepParameters s;
	RandomSampler rng;
	BaseSequence iseq(1);

	for (unsigned int j = 0; j < 6; j++)
		s[j] = iseq.mean(0)[j] + iseq.std(0)[j]*rng.sample_normal();
	return s;
}

SimulationParameters base_parameters() {
	SimulationParameters sp;

	sp.n_configurations = 10000;
	sp.boundary_condition_file = "Resources/fixed0.dat";
	sp.lac_type = FIXED; sp.lac_string = "FIXED";
	sp.lac_repressor_directory = "Resources/Lac";

	sp.radi = 20.0;
	sp.gam = .98;
	sp.twi = 11.5;
	sp.trff = 180.0*acos(sp.gam)/M_PI;

	return sp;
}

SimulationParameters simulation_parameters_with_HU_and_Fis() {
	SimulationParameters sp = base_parameters();
	sp.proteins_configuration.push_back("HU+137");
	sp.proteins_configuration.push_back("Fis+1000");
	return sp;
}

SimulationParameters simulation_parameters_with_HU() {
	SimulationParameters sp = base_parameters();
	sp.proteins_configuration.push_back("HU+137");
	return sp;
}

SimulationParameters simulation_parameters_without_Proteins() {
	SimulationParameters sp = base_parameters();
	return sp;
}

variables_map default_options() {
    options_description desc;
    desc.add_options()
		("help", "produce help")
		("dna_sequence,s", value<string>()->default_value("Ideal+78"), "Number of steps")
		("sampling_parameters,q", value<string>()->default_value("Resources/step_params.dat"), "Sampling parameters")
		("progress_display_interval,v", value<unsigned int>()->default_value(5000), "Interval for progress display")
		("n_configurations,n", value<UInteger64>()->default_value(5000000), "Number of half chains")
		("cores,c", value<UInteger64>()->default_value(-1), "Number of threads (CPU cores) to use, -1 =  all available cores")
		("beta", value<Real>()->default_value(1.0), "Beta")
		("n_snapshots,x", value<UInteger64>()->default_value(0), "Draw this number of structures")
		("epsilon,e", value<Real>()->default_value(10.0), "Epsilon value")
		("radi,r", value<Real>()->default_value(20.0), "radi")
		("gam,g", value<Real>()->default_value(0.98), "gam")
		("twi,t", value<Real>()->default_value(11.5), "twist")
		("flexible_type,f", value<string>()->default_value("A1P1"), "A1P1 or A2P2, used only for flexible looping")
		("lac,l", value<string>()->default_value("flexible"), "[fixed|flexible|miniciricle] boundaries")
		("boundary_condition_file,b", value<string>()->default_value("Resources/fixed0.dat"), "Boundary conditions file")
		("output_prefix,o", value<string>()->default_value("output"), "output directory")
		("n_alongs,a", value<UInteger64>()->default_value(0), "Number of alongs")
		("n_loops,w", value<Integer64>()->default_value(-1), "Number of loops to report (negative means all)")
		("lac_repressor_directory,z", value<string>()->default_value("Resources/Lac"), "Location for Lac files")
		("proteins,p", value<vector<string> >()->multitoken(), "protein description string in format [name+concentration]");

    variables_map vm;
    char **empty;
	store(parse_command_line(0, empty, desc), vm);
	notify(vm);
	return vm;
}

int test_n_steps_in_loop() {
	return 79;
}
