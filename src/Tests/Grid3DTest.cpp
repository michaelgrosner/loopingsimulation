/*
 * Grid3DTest.cpp
 *
 *  Created on: Dec 28, 2012
 *      Author: grosner
 */

#include "TestHelpers.h"

#include "Grid3D.h"
#include "HalfChain.h"
#include "HalfChainFactory.h"
#include "BasePairAndStep.h"
#include "RandomSampler.h"
#include "LocationCollection.h"
#include "BoundaryConditionMapFactory.h"
#include "BoundaryConditionParser.h"

class Grid3DTest : public ::testing::Test {
protected:
	SimulationParameters sp;
	Grid3D g;

	HalfChain hc_without_proteins;
	HalfChain hc_with_proteins;

	Grid3DTest() : sp(simulation_parameters_with_HU()), g(sp) {

		BasePair bp;
		bp <<  -0.0598397,   0.992686,   0.104847,    3.53248,
				   -0.947656, -0.0234932,  -0.318427,   -18.0936,
				   -0.313634,  -0.118414,   0.942131,     128.79,
				           0,          0,          0,          1;
		UInteger64 seed = rand() % sp.n_configurations;

		LocationCollection protein_collection;
		protein_collection.push_back(ProteinLocation(StructurePtr(), 10, false));
		hc_with_proteins = HalfChain(bp, protein_collection, rand() % sp.n_configurations, 14);
		hc_without_proteins = HalfChain(bp, LocationCollection(), rand() % sp.n_configurations, 0);
	};
};

TEST_F(Grid3DTest,ShouldFindHalfChainInGrid) {
	g.add_halfchain_to_grid(hc_without_proteins);

	Vector3d v = -1.0 * hc_without_proteins.bp().topLeftCorner<3,3>().transpose() * hc_without_proteins.bp().topRightCorner<3,1>();

	ASSERT_EQ(g.find_neighbors_to_point(v).size(), 1);
}

TEST_F(Grid3DTest,ShouldReconstructInsertedHalfChain) {
	g.add_halfchain_to_grid(hc_with_proteins);

	HalfChain expected = g.rebuild_halfchain_from_seed(hc_with_proteins.seed());

	ASSERT_EQ(hc_with_proteins.seed(), expected.seed());
	assert_matrix_equals_or_print_and_throw(hc_with_proteins.bp(), expected.bp());
	ASSERT_EQ(hc_with_proteins.overhang_code(), expected.overhang_code());
}
