/*
 * DependentSequenceTest.cpp
 *
 *  Created on: Jan 4, 2013
 *      Author: grosner
 */

#include "TestHelpers.h"

#include "BaseSequence.h"
#include "SequenceStepToMeanAndStdMapFactory.h"
#include "SequenceFactory.h"

/*class DependentSequenceTest : public ::testing::Test {
protected:
	string seq, name;
	UInteger64 n_steps;
	SamplingParametersProviderPtr ssmsmf;
	BaseSequencePtr dseq;
	SequenceFactory sf;

	StepParameters means, stds;

	DependentSequenceTest() : n_steps(14),
			name("NAME"),
			ssmsmf(new SequenceStepToMeanAndStdMapFactory("Resources/step_params.dat")),
			seq("ZZZZZZZZZZZZZZZ"),
			sf(ssmsmf),
			dseq(sf.build("Seq+"+name+"+"+seq)) {
		means << 0, 0, 34.28, 0, 0, 3.4;
		stds  << 4.83934, 4.83934, 4.09273, 0.141421, 0.141421, 0.141421;
	};
};

TEST_F(DependentSequenceTest, ShouldHave24IdealSteps) {
	ASSERT_EQ(dseq->n_steps(), n_steps);
}

TEST_F(DependentSequenceTest, ShouldPickCorrectMeanValueAtStep) {
	Real expected = means[2];
	Real actual = dseq->mean(13)[2];
	ASSERT_EQ(expected, actual);
}

TEST_F(DependentSequenceTest, ShouldPickCorrectStdValueAtStep) {
	Real expected = means[5];
	Real actual = dseq->mean(6)[5];
	ASSERT_EQ(expected, actual);
}

TEST_F(DependentSequenceTest, FirstAndSecondLengthsShouldEqualNsteps) {
	ASSERT_EQ(dseq->n_steps(), dseq->first_half_length()+dseq->second_half_length());
}

TEST_F(DependentSequenceTest, ShouldMakeCorrectSequence) {
	ASSERT_EQ(dseq->sequence(), seq);
}

TEST_F(DependentSequenceTest, SequenceFactoryCanConstructCorrectly) {
	ASSERT_EQ(sf.build(seq)->n_steps(), n_steps);
}

TEST_F(DependentSequenceTest, ShouldHaveCorrectName) {
	ASSERT_EQ(name, dseq->name());
}

TEST(DependentSequenceIntegrationTest, ShouldHaveCorrectlyParsedStepsFromFile) {
	SamplingParametersProviderPtr ssmsmf(new SequenceStepToMeanAndStdMapFactory("Resources/step_params.dat"));
	SequenceFactory sf(ssmsmf);
	BaseSequencePtr seq(sf.build("Seq+NAME+ACTAG"));

	SixBySix actual_forces;
	actual_forces << 0.042, 0.000, 0.000,  0.000,  0.000,  0.000,
			         0.000, 0.042, 0.000,  0.000,  0.000,  0.000,
			         0.000, 0.000, 0.059,  0.000,  0.000,  0.000,
			         0.000, 0.000, 0.000, 50.000,  0.000,  0.000,
			         0.000, 0.000, 0.000,  0.000, 50.000,  0.000,
			         0.000, 0.000, 0.000,  0.000,  0.000, 50.000;

	StepParameters actual_stds;
	actual_stds << 4.839339, 4.839339, 4.092728, 0.141421, 0.141421, 0.141421;

	SixBySix actual_couplings = SixBySix::Identity();

	assert_matrix_equals_or_print_and_throw(seq->force_constant(2), actual_forces);

	assert_matrix_equals_or_print_and_throw(seq->coupling_matrix(2), actual_couplings);

	assert_matrix_equals_or_print_and_throw(seq->coupling_matrix(2)*seq->std(2), actual_couplings*actual_stds);

	assert_matrix_equals_or_print_and_throw(seq->std(2), actual_stds);

}

TEST(DependentSequenceIntegrationTest, ShouldHaveCorrectlyCoupledSteps) {
	SamplingParametersProviderPtr ssmsmf(new SequenceStepToMeanAndStdMapFactory("Resources/coupled_steps.dat"));
	SequenceFactory sf(ssmsmf);
	BaseSequencePtr seq(sf.build("Seq+NAME+ACTAG"));

	SixBySix actual_forces;
	actual_forces << 0.064, 0.000, 0.000,  0.000,  0.000,  0.000,
			         0.000, 0.064, 0.034,  0.000,  0.000,  0.000,
			         0.000, 0.034, 0.090,  0.000,  0.000,  0.000,
			         0.000, 0.000, 0.000, 50.000,  0.000,  0.000,
			         0.000, 0.000, 0.000,  0.000, 50.000,  0.000,
			         0.000, 0.000, 0.000,  0.000,  0.000, 50.000;

	StepParameters actual_stds;
	actual_stds << 3.9513, 4.9794, 2.97129, 0.141421, 0.141421, 0.141421;

	SixBySix actual_couplings;
	actual_couplings << 1.000,  0.000, 0.000, 0.000, 0.000, 0.000,
			            0.000,  0.823, 0.567, 0.000, 0.000, 0.000,
			            0.000, -0.567, 0.823, 0.000, 0.000, 0.000,
			            0.000,  0.000, 0.000, 1.000, 0.000, 0.000,
			            0.000,  0.000, 0.000, 0.000, 1.000, 0.000,
			            0.000,  0.000, 0.000, 0.000, 0.000, 1.000;

	assert_matrix_equals_or_print_and_throw(seq->force_constant(2), actual_forces);

	assert_matrix_equals_or_print_and_throw(seq->coupling_matrix(2), actual_couplings);

	assert_matrix_equals_or_print_and_throw(seq->coupling_matrix(2)*seq->std(2), actual_couplings*actual_stds);

	assert_matrix_equals_or_print_and_throw(seq->std(2), actual_stds);

}
*/
