#include "AlongClosureCSVGenerator.h"
#include "Closure.h"
#include "BoundaryCondition.h"
#include "BaseSequence.h"
#include <gtest/gtest.h>


class TestIdGenerator : public ClosureStringGenerator {
public:
	TestIdGenerator() {}
	virtual ~TestIdGenerator() {}
	virtual string generate(const ClosurePtr& closure) const { return "ID,ID"; }
};

class AlongClosureCSVGeneratorTest : public ::testing::Test {
protected:
	TestIdGenerator id_generator;
	AlongClosureCSVGenerator generator;
	ClosurePtr closure;

	AlongClosureCSVGeneratorTest() : id_generator(), generator(id_generator) {
		DNASim::Vertex a(1.2,2,3);
		DNASim::Vertex b(4,5,6);
		DNASim::CurvePoints base, edge;
		base.push_back(a); edge.push_back(b);
		base.push_back(b); edge.push_back(a);

		closure = ClosurePtr(new Closure(BoundaryCondition(StepParameters(), "A1"),
									    HalfChain(BasePair(), LocationCollection(), 1, 0),
									    HalfChain(BasePair(), LocationCollection(), 2, 0),
									    BasePairVector(),
									    StepParametersVector(),
									    LocationCollection(),
									    DNASim::DiscreteRibbon(base, edge),
									    OptionalLacRepressorAndAngle(),
									    BaseSequencePtr(new BaseSequence(78)),
									    10));
	}
};

TEST_F(AlongClosureCSVGeneratorTest, ShouldBeImplemented) {
	string actual = generator.generate(closure);
	string expected = "ID,ID,twist_density,nan,nan\n"; // FIXME
	ASSERT_EQ(expected, actual);
}
