#include "TestHelpers.h"
#include "BoundaryConditionToLacRepressorsMap.h"
#include "SecondHalfChainProcessor.h"
#include <DNASim/dsMatrix4.h>

#include "MockClosureFactory.h"
#include "MockClosureProcessor.h"
#include "MockHalfChainStorage.h"
#include "MockJFactorReporter.h"

using ::testing::_;
using ::testing::Return;
using ::testing::NiceMock;

class SecondHalfChainProcessorTest : public ::testing::Test {
protected:
	SimulationParameters params;
	BoundaryConditionToLacRepressorsMap named_boundaries;
	boost::shared_ptr<NiceMock<MockClosureFactory> > closure_factory_ptr;
	NiceMock<MockJFactorReporter> j_factor_reporter;
	NiceMock<MockClosureProcessor> valid_closure_processor;
	NiceMock<MockHalfChainStorage> grid;
	SecondHalfChainProcessor processor;

	SecondHalfChainProcessorTest() :
		params(simulation_parameters_without_Proteins()),
		closure_factory_ptr(new NiceMock<MockClosureFactory>()),
		processor(closure_factory_ptr, params, named_boundaries, j_factor_reporter, valid_closure_processor, grid) {}

	virtual void SetUp() {
		vector<Seed> returned_seeds; returned_seeds.push_back(0);
		ON_CALL(grid, find_neighbors_to_point(_)).WillByDefault(Return(returned_seeds));
	}

	void attach_flexible_boundary() {
		DNASim::Triad ta(DNASim::Vector3D<Real>("{57.6981,   26.5508,   60.8759}"),
				         DNASim::Vector3D<Real>("{-0.5869,    0.5716,   -0.5734}"),
				         DNASim::Vector3D<Real>("{-0.2543,    0.5422,    0.8009}"),
				         DNASim::Vector3D<Real>("{ 0.7686,    0.6159,   -0.1729}"));


	    DNASim::Triad tb(DNASim::Vector3D<Real>("{75.2230,  -18.0357,   31.6546}"),
				         DNASim::Vector3D<Real>("{-0.7810,   -0.5870,   -0.2132}"),
				         DNASim::Vector3D<Real>("{-0.5779,    0.5497,    0.6033}"),
				         DNASim::Vector3D<Real>("{-0.2369,    0.5944,   -0.7685}"));

		BoundaryCondition boundary(dsMatrix4_to_bp(ta.matrix_representation()),
								   dsMatrix4_to_bp(tb.matrix_representation()).inverse(),
								   "A1");
		named_boundaries[boundary] = OptionalLacRepressor();
	}
};

TEST_F(SecondHalfChainProcessorTest, ShouldTickJFactorNearingEndPointFixed) {
	BasePair A, B;
	A <<  0.4073792,    0.7921420,    0.4544812,  -26.5886347,
	     -0.8153648,    0.0913163,    0.5717006,   -9.3879686,
	      0.4113665,   -0.6034669,    0.6830851,  115.2914956,
	      0.0000000,    0.0000000,    0.0000000,    1.0000000;
	B << -0.8282274,   -0.4467702,    0.3382836,  -10.0639623,
	     -0.5433935,    0.7878239,   -0.2899259,   28.2278846,
	     -0.1369776,   -0.4239457,   -0.8952693,  -54.4252730,
	      0.0000000,    0.0000000,    0.0000000,    1.0000000;
	HalfChain  first(A, LocationCollection(), 0, 0);
	HalfChain second(B, LocationCollection(), 0, 0);

	StepParameters boundary_step;
	boundary_step << 149.5031,-40.3931,-39.3564,-17.7460,-31.3849,-67.0004;
	BoundaryCondition boundary(boundary_step, "A1");
	named_boundaries[boundary] = OptionalLacRepressor();

	ON_CALL(grid, rebuild_halfchain_from_seed(0)).WillByDefault(Return(first));

	EXPECT_CALL(j_factor_reporter, loop_nears_end_point()).Times(1);
	EXPECT_CALL(j_factor_reporter, loop_end_is_parallel()).Times(0);
	EXPECT_CALL(j_factor_reporter, loop_is_not_overtwisted()).Times(0);

	processor.process(second);
}

TEST_F(SecondHalfChainProcessorTest, ShouldTickJFactorLoopIsParallelFixed) {
	BasePair A, B;
	A << -0.449600,  0.056049, -0.891470, -44.521160,
	     -0.825899,  0.354054,  0.438790,   3.861558,
	      0.340223,  0.933544, -0.112892, 100.820364,
	      0.000000,  0.000000,  0.000000,   1.000000;
	B <<  0.623056,  0.681660, -0.383590, -18.784127,
	      0.103502, -0.557951, -0.823394, -85.448348,
	     -0.775299,  0.473318, -0.418188,  45.307967,
	      0.000000,  0.000000,  0.000000,   1.000000;
	HalfChain  first(A, LocationCollection(), 0, 0);
	HalfChain second(B, LocationCollection(), 0, 0);

	StepParameters boundary_step;
	boundary_step << -148.5286,-38.83339,-41.24981,18.472,-31.101,-67.438;
	BoundaryCondition boundary(boundary_step, "A1");
	named_boundaries[boundary] = OptionalLacRepressor();

	ON_CALL(grid, rebuild_halfchain_from_seed(0)).WillByDefault(Return(first));

	EXPECT_CALL(j_factor_reporter, loop_nears_end_point()).Times(1);
	EXPECT_CALL(j_factor_reporter, loop_end_is_parallel()).Times(1);
	EXPECT_CALL(j_factor_reporter, loop_is_not_overtwisted()).Times(0);

	processor.process(second);
}

TEST_F(SecondHalfChainProcessorTest, ShouldTickJFactorLoopIsNotOvertwistedFixed) {
	BasePair A, B;
	A <<  0.5700850,    0.5992636,   -0.5620375,  -47.0816961,
		  0.5591417,   -0.7842157,   -0.2690098,  -43.0640685,
		 -0.6019664,   -0.1609001,   -0.7821429,   93.3051398,
		  0.0000000,    0.0000000,    0.0000000,    1.0000000;
	B <<  0.4297304,    0.8152287,    0.3882447,   57.6302551,
	      0.5077490,   -0.5737190,    0.6426799,   35.6536533,
	      0.7466744,   -0.0790482,   -0.6604761,   70.0855796,
	      0.0000000,    0.0000000,    0.0000000,    1.0000000;
	HalfChain  first(A, LocationCollection(), 0, 0);
	HalfChain second(B, LocationCollection(), 0, 0);

	StepParameters boundary_step;
	boundary_step << 0.8957179,71.51884,-27.00661,-0.0492293,-55.11152,-10.55767;
	BoundaryCondition boundary(boundary_step, "P1");
	named_boundaries[boundary] = OptionalLacRepressor();

	ON_CALL(grid, rebuild_halfchain_from_seed(0)).WillByDefault(Return(first));

	EXPECT_CALL(j_factor_reporter, loop_nears_end_point()).Times(1);
	EXPECT_CALL(j_factor_reporter, loop_end_is_parallel()).Times(1);
	EXPECT_CALL(j_factor_reporter, loop_is_not_overtwisted()).Times(1);

	ON_CALL(*closure_factory_ptr, generate_loop(_,_,_,_)).WillByDefault(Return(ClosurePtr()));

	processor.process(second);
}

TEST_F(SecondHalfChainProcessorTest, ShouldTickNothingFlexible) {
	params.lac_type = FLEXIBLE;
	BasePair A, B;
	A <<  0.3205954,   -0.8460401,    0.4259515,   94.9592180,
	     -0.3649010,   -0.5252927,   -0.7687098,   99.8451519,
	      0.8741086,    0.0910147,   -0.4771273,  -28.6836411,
	      0.0000000,    0.0000000,    0.0000000,    1.0000000;
	B << -0.6562216,    0.5255717,    0.5414310,   32.1264515,
	      0.7140009,    0.2003948,    0.6708536,   58.0032567,
	      0.2440817,    0.8268109,   -0.5067621,   33.9468838,
	      0.0000000,    0.0000000,    0.0000000,    1.0000000;
	HalfChain  first(A, LocationCollection(), 0, 0);
	HalfChain second(B, LocationCollection(), 0, 0);

	attach_flexible_boundary();

	ON_CALL(grid, rebuild_halfchain_from_seed(0)).WillByDefault(Return(first));

	EXPECT_CALL(j_factor_reporter, loop_nears_end_point()).Times(0);
	EXPECT_CALL(j_factor_reporter, loop_end_is_parallel()).Times(0);
	EXPECT_CALL(j_factor_reporter, loop_is_not_overtwisted()).Times(0);

	processor.process(second);
}

TEST_F(SecondHalfChainProcessorTest, ShouldTickNearsEndpointFlexible) {
	params.lac_type = FLEXIBLE;
	BasePair A, B;
	A <<  -0.3218328,   -0.9463625,    0.0286660,  143.8091841,
			   0.1223639,   -0.0715975,   -0.9898994,   54.0412708,
			   0.9388561,   -0.3150744,    0.1388430,   -9.5972370,
			   0.0000000,    0.0000000,    0.0000000,    1.0000000;
	B << -0.6562216,    0.5255717,    0.5414310,   32.1264515,
			   0.7140009,    0.2003948,    0.6708536,   58.0032567,
			   0.2440817,    0.8268109,   -0.5067621,   33.9468838,
			   0.0000000,    0.0000000,    0.0000000,    1.0000000;
	HalfChain  first(A, LocationCollection(), 0, 0);
	HalfChain second(B, LocationCollection(), 0, 0);

	attach_flexible_boundary();

	ON_CALL(grid, rebuild_halfchain_from_seed(0)).WillByDefault(Return(first));

	EXPECT_CALL(j_factor_reporter, loop_nears_end_point()).Times(1);
	EXPECT_CALL(j_factor_reporter, loop_end_is_parallel()).Times(0);
	EXPECT_CALL(j_factor_reporter, loop_is_not_overtwisted()).Times(0);

	processor.process(second);
}

TEST_F(SecondHalfChainProcessorTest, ShouldTickJFactorLoopIsParallelFlexible) {
	params.lac_type = FLEXIBLE;
	BasePair A, B;
	A <<  0.3901771,   -0.6446060,   -0.6574534,  102.2169097,
	      0.9180280,    0.3271198,    0.2240922,  132.2667952,
	      0.0706149,   -0.6909962,    0.7194010,   56.0794795,
	      0.0000000,    0.0000000,    0.0000000,    1.0000000;
	B << -0.4231680,   -0.5029096,   -0.7536649,  -88.6289260,
	     -0.4741389,   -0.5859123,    0.6571903,   73.4794833,
	     -0.7720888,    0.6354438,    0.0094903,   36.8444170,
	      0.0000000,    0.0000000,    0.0000000,    1.0000000;
	HalfChain  first(A, LocationCollection(), 0, 0);
	HalfChain second(B, LocationCollection(), 0, 0);

	attach_flexible_boundary();

	ON_CALL(grid, rebuild_halfchain_from_seed(0)).WillByDefault(Return(first));

	EXPECT_CALL(j_factor_reporter, loop_nears_end_point()).Times(1);
	EXPECT_CALL(j_factor_reporter, loop_end_is_parallel()).Times(1);
	EXPECT_CALL(j_factor_reporter, loop_is_not_overtwisted()).Times(0);

	processor.process(second);
}

TEST_F(SecondHalfChainProcessorTest, ShouldTickJFactorLoopIsNotOvertwistedFlexible) {
	params.lac_type = FLEXIBLE;
	BasePair A, B;
	A << -0.7629900,    0.6450269,   -0.0422664,  137.6993206,
	     -0.1426383,   -0.2317774,   -0.9622544,   -3.3024673,
	     -0.6304764,   -0.7281617,    0.2688494,   61.2597629,
	      0.0000000,    0.0000000,    0.0000000,    1.0000000;
	B <<  0.4843512,   -0.3973544,    0.7794315,   77.2698861,
	     -0.6361521,   -0.7715611,    0.0019730,    3.4753389,
	      0.6005950,   -0.4967926,   -0.6264844,    3.9342700,
	      0.0000000,    0.0000000,    0.0000000,    1.0000000;
	HalfChain  first(A, LocationCollection(), 0, 0);
	HalfChain second(B, LocationCollection(), 0, 0);

	attach_flexible_boundary();

	ON_CALL(grid, rebuild_halfchain_from_seed(0)).WillByDefault(Return(first));

	EXPECT_CALL(j_factor_reporter, loop_nears_end_point()).Times(1);
	EXPECT_CALL(j_factor_reporter, loop_end_is_parallel()).Times(1);
	EXPECT_CALL(j_factor_reporter, loop_is_not_overtwisted()).Times(1);

	ON_CALL(*closure_factory_ptr, generate_loop(_,_,_,_)).WillByDefault(Return(ClosurePtr()));

	processor.process(second);
}
