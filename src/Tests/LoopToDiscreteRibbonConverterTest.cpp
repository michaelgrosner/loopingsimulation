#include "TestHelpers.h"
#include "LoopToDiscreteRibbonConverter.h"

class LoopToDiscreteRibbonConverterTests : public ::testing::Test {
protected:
	LoopToDiscreteRibbonConverter converter;

	LoopToDiscreteRibbonConverterTests() : converter(true) {}

	virtual void SetUp() {

	}
};

TEST_F(LoopToDiscreteRibbonConverterTests, TriadsWithRepressorShouldBeNPlus14Length) {
	UInteger64 n_loop_bps = 78;
	UInteger64 n_half_triads = 14;
	UInteger64 n_atoms = 2;

	BasePairVector loop_bps(n_loop_bps);
	vector<DNASim::Triad> half1_bps(n_half_triads);
	vector<DNASim::Triad> half2_bps(n_half_triads);
	vector<NamedDNASimVector3D> atoms(n_atoms);

	LacHalf h1(1, "1", "1", half1_bps, atoms, false);
	LacHalf h2(2, "2", "2", half2_bps, atoms, false);
	LacRepressor lac("O", h1, h2);
	OptionalLacRepressorAndAngle lac_and_angle(LacRepressorAndAngle(lac, 0));

	DNASim::DiscreteRibbon ribbon = converter.convert(loop_bps, lac_and_angle);
	ASSERT_EQ(ribbon.ribbon_base_curve().size(), n_loop_bps+n_half_triads+n_atoms);
}

TEST_F(LoopToDiscreteRibbonConverterTests, TriadsWithWithoutRepressorShouldBeSameLengthAsTriads) {
	UInteger64 n_loop_bps = 78;
	BasePairVector loop_bps(n_loop_bps);

	DNASim::DiscreteRibbon ribbon = converter.convert(loop_bps, OptionalLacRepressorAndAngle());
	ASSERT_EQ(ribbon.ribbon_base_curve().size(), n_loop_bps);
}
