/*
 * X3DNAStepsReaderTest.cpp
 *
 *  Created on: Jan 21, 2013
 *      Author: grosner
 */

#include "TestHelpers.h"
#include "X3DNAStepsReader.h"
#include "ProteinLocation.h"

class X3DNAStepsReaderTest : public ::testing::Test {
protected:
	StringProvider provider;
	X3DNAStepsReader reader;

	X3DNAStepsReaderTest() : provider("$HU-3,14,0                                         \n"
			                          "$HU-3,34,1                                         \n"
			                          "$HU-2,49,1                                         \n"
			                          "  79 base-pairs                                    \n"
			                          "   0  step parameters                              \n"
			                          "      shift   slide    rise    tilt    roll   twist\n"
			                          "A-T    0.00    0.00    0.00    0.00    0.00    0.00\n"
			                          "A-T    -0.03  -0.23   3.38  -8.31  -5.23  31.56    \n"
			                          "A-T     0.22   0.03   3.41   3.81   6.50  28.08    \n"
			                          "A-T     0.11  -0.05   3.40   3.49  -7.76  36.32    \n"
			                          "A-T     0.09  -0.06   3.43  -0.49  -7.35  32.82    \n"), reader(provider) {}
};

TEST_F(X3DNAStepsReaderTest, ShouldBeAbleToReadTestFile) {
	ASSERT_EQ(reader.get_steps_vector().size(), 4);
}

TEST_F(X3DNAStepsReaderTest, ShouldAccuratelyBeAbleToReadSteps) {
	StepParameters expected;
	expected << 3.49, -7.76, 36.32, 0.11, -0.05, 3.40;
	ASSERT_TRUE(reader.get_steps_vector()[2].isApprox(expected));
}

/*TEST(X3DNAStepsReaderTest, ShouldAccuratelyReadAndGetLocationCollection) {
	X3DNAStepsReader xr("TestResources/overlapping_steps.dat");
	StepParameters expected;
	expected << 5.62, 2.51, 33.78, 0.62, -0.17, 2.82;

	ProteinClassPtr pclass(new ProteinClass(14, "HU", 137));

	pair<StepParametersVector, LocationCollection> res = xr.get_steps_and_protein_locations(pclass);
	StepParametersVector steps = res.first;

	ASSERT_TRUE(steps[14].isApprox(expected));
	ASSERT_EQ(res.second.front().reversed, false);
	ASSERT_EQ(res.second.front().start_step, 14);
	ASSERT_EQ(res.second.size(), 3);
}*/
