#include <TestHelpers.h>
#include <BasePairAndStep.h>
#include <TunableSamplingParametersProvider.h>

TEST(TunableSamplingParametersProviderTest, ShouldBeAbleToGetCorrectValuesFromEEquals4) {
	StepParameters mean;
	mean << 0.0, 0.0, 34.28, 0.0, 0.0, 3.4;
	TunableSamplingParametersProvider tssp(mean, 4.0);
	SequenceStepToMeanAndStdMap map = tssp.build();
	MeanStdCouplingEnergy msce = map[make_pair('A', 'A')];

	StepParameters expected_std;
	expected_std << 4.   ,  5.887, -0.459,  0.141,  0.141,  0.141;
	assert_matrix_equals_or_print_and_throw(expected_std, msce.get<1>());

	SixBySix expected_coupling;
	expected_coupling <<  1.,  0.   ,  0.   , 0.,   0.,   0.,
			              0.,  0.82 ,  0.573, 0.,   0.,   0.,
			              0., -0.573,  0.82 , 0.,   0.,   0.,
			              0.,  0.   ,  0.   , 1.,   0.,   0.,
			              0.,  0.   ,  0.   , 0.,   1.,   0.,
			              0.,  0.   ,  0.   , 0.,   0.,   1.;
	assert_matrix_equals_or_print_and_throw(expected_coupling, msce.get<2>());

	SixBySix expected_force;
	expected_force << 0.062,   0.   ,   0.   ,   0.,      0.,      0.,
			          0.   ,   0.062,   0.034,   0.,      0.,      0.,
			          0.   ,   0.034,   0.087,   0.,      0.,      0.,
			          0.   ,   0.   ,   0.   ,  50.,      0.,      0.,
			          0.   ,   0.   ,   0.   ,   0.,     50.,      0.,
			          0.   ,   0.   ,   0.   ,   0.,      0.,     50.;
	assert_matrix_equals_or_print_and_throw(expected_force, msce.get<3>());
}
