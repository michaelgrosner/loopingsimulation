#include "TestHelpers.h"
#include "OverhangCodeMapFactory.h"
#include <boost/assign/list_of.hpp>
#include "FileSink.h"
#include "BaseSequence.h"
#include "ProteinClass.h"

using namespace boost::assign;

class OverhangCodeMapFactoryTest : public ::testing::Test {
protected:
	OverhangCodeMapFactory factory;
	vector<StructurePtr> structures;
	vector<ProteinClassPtr> protein_classes;
	BaseSequencePtr dna_sequence;

	virtual void SetUp() {
		structures = list_of(StructurePtr(new Structure("PC1", "A", "dat", "pdb", CAlphasMatrix(), StepParametersVector(), StepParametersVector())))
							(StructurePtr(new Structure("PC1", "B", "dat", "pdb", CAlphasMatrix(), StepParametersVector(), StepParametersVector())));

		protein_classes = list_of(ProteinClassPtr(new ProteinClass(10, "PC1", 1.0, structures)));
								 (ProteinClassPtr(new ProteinClass(14, "PC2", 0.2, structures)));

		dna_sequence = BaseSequencePtr(new BaseSequence(78));
		factory = OverhangCodeMapFactory(dna_sequence, protein_classes);
	}
};

TEST_F(OverhangCodeMapFactoryTest, ShouldGiveNonZeroOverhangCode) {
	auto map = factory.build();
	auto locations = list_of(ProteinLocation(structures.back(), 37, 0));
	ASSERT_GT(map->get_overhang_code(locations), 0);
}

TEST_F(OverhangCodeMapFactoryTest, ShouldGiveZeroOverhangCode) {
	auto map = factory.build();
	auto locations = list_of(ProteinLocation(structures.back(), 4, 0));
	ASSERT_EQ(map->get_overhang_code(locations), 0);
}

TEST_F(OverhangCodeMapFactoryTest, SameStructuresShouldGiveSameOverhangCode) {
	auto map = factory.build();

	auto locations = list_of(ProteinLocation(structures.back(), 36, 0));
	auto code1 = map->get_overhang_code(locations);

	locations = list_of(ProteinLocation(structures.front(), 36, 0));
	auto code2 = map->get_overhang_code(locations);

	ASSERT_EQ(code1, code2);
}
