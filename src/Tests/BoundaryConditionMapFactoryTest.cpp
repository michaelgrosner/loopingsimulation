#include <gtest/gtest.h>
#include "TestHelpers.h"
#include "BoundaryConditionMapFactory.h"
#include "LacAndOrientationFileParser.h"
#include "LacHalfFactory.h"

class BoundaryConditionMapFactoryTest : public ::testing::Test {
public:
	StreamProviderPtr sample_fixed_file;
	SimulationParameters sp;

	virtual void SetUp() {
		SimulationParameters sp = simulation_parameters_with_HU();
		sample_fixed_file = StreamProviderPtr(new FileProvider(sp.boundary_condition_file));
	}
};

TEST_F(BoundaryConditionMapFactoryTest, ShouldBeAbleToParseFixedBoundariesCorrectly) {
	LacHalfFactoryPtr lac_half_factory(new LacHalfFactory());
	StreamProviderPtr verify_lacs_were_made(new FileProvider());
	LacAndOrientationFileParser parser("Resources/Lac", lac_half_factory, verify_lacs_were_made);
	BoundaryConditionMapFactory factory(parser, sample_fixed_file, FIXED, "A1P1");

	auto map = factory.build();

	// A1
	StepParameters step;
	step << -148.5286, -38.83339, -41.24981, 18.472, -31.101, -67.438;
	BoundaryCondition expected_A1(step, "A1");
	auto actual_A1 = map.find(expected_A1);
	ASSERT_TRUE(actual_A1 != map.end());
	ASSERT_TRUE(actual_A1->second);
	ASSERT_EQ("A1", actual_A1->second->orientation());
	ASSERT_EQ(14,   actual_A1->second->half1().triads().size());
	ASSERT_EQ(14,   actual_A1->second->half2().triads().size());
	ASSERT_EQ(2,    actual_A1->second->half1().atoms().size());
	ASSERT_EQ(2,    actual_A1->second->half2().atoms().size());

	// P2
	step << 0.8725,56.7926,-118.2530,0.1633,44.4719,-99.0222;
	BoundaryCondition expected_P2(step, "P2");
	auto actual_P2 = map.find(expected_P2);
	ASSERT_TRUE(actual_P2 != map.end());
	ASSERT_TRUE(actual_P2->second);
	ASSERT_EQ("P2", actual_P2->second->orientation());
	ASSERT_EQ(14,   actual_P2->second->half1().triads().size());
	ASSERT_EQ(14,   actual_P2->second->half2().triads().size());
	ASSERT_EQ(2,    actual_P2->second->half1().atoms().size());
	ASSERT_EQ(2,    actual_P2->second->half2().atoms().size());
}

TEST_F(BoundaryConditionMapFactoryTest, ShouldBeAbleToParseFlexibleBoundariesCorrectly) {
	LacHalfFactoryPtr lac_half_factory(new LacHalfFactory());
	StreamProviderPtr verify_lacs_were_made(new VerificationStreamProvider());
	LacAndOrientationFileParser parser(sp.lac_repressor_directory, lac_half_factory, verify_lacs_were_made);
	BoundaryConditionMapFactory factory(parser, sample_fixed_file, FLEXIBLE, "A1P1");
	auto map = factory.build();

	BasePair A, D;

	A << -0.5869, -0.2543,  0.7686,  57.6981,   // A1P1
		  0.5716,  0.5422,  0.6159,  26.5508,
		 -0.5734,  0.8009, -0.1729,  60.8759,
		       0,       0,       0,        1;
	D << -0.4194, -0.3722,  -0.828,  89.3486,   // 4
		  0.8861, -0.3661, -0.2843,  -1.0677,
		 -0.1973, -0.8529,  0.4833,  -3.0732,
		       0,       0,       0,        1;

	BoundaryCondition expected_A1(A, D.inverse(), "A1");

	auto actual_A1 = map.find(expected_A1);
	ASSERT_TRUE(actual_A1 != map.end());
	ASSERT_TRUE(actual_A1->second);
	ASSERT_EQ("A1", actual_A1->second->orientation());
}
