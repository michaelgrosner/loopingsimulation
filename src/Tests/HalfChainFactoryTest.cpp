/*
 * HalfChainFactoryTest.cpp
 *
 *  Created on: Dec 28, 2012
 *      Author: grosner
 */

#include "TestHelpers.h"

/*#include "Grid3D.h"
#include "HalfChain.h"
#include "HalfChainFactory.h"
#include "BasePairAndStep.h"
#include "RandomSampler.h"
#include "HalfChainProteinLocationSampler.h"
#include "BoundaryCondition.h"
#include "ClosureFactory.h"
#include "IdealSequence.h"
#include "BoundaryConditionMapFactory.h"
#include "BoundaryConditionParser.h"
#include "GaussianSampler.h"
#include "StructureFactory.h"
#include "ProteinCorpusFactory.h"

class HalfChainFactoryTestWithoutHU : public ::testing::Test {
public:
	SimulationParameters sp;
	BaseSequencePtr iseq;
	RandomSamplerPtr rng;
    BaseOverhangCodeMapPtr overhang_map;
	ProteinLocationSamplerPtr pp;
	BaseSamplingAlgorithmPtr hcsp;
	HalfChainFactory hcf;
	Grid3D g;

	HalfChainFactoryTestWithoutHU() : sp(simulation_parameters_without_Proteins()),
			iseq(new IdealSequence(test_n_steps_in_loop())),
			rng(new RandomSampler),
            overhang_map(new OverhangCodeMap()),
			pp(new HalfChainProteinLocationSampler(vector<ProteinClassPtr>(), rng, overhang_map, iseq)),
			hcsp(new GaussianSampler(rng, iseq)),
			hcf(hcsp, BasePair::Identity(), pp, overhang_map, iseq),
			g(sp) {};

};

class HalfChainFactoryTestWithHU : public ::testing::Test {
public:
	SimulationParameters sp;
	BaseSequencePtr iseq;
	RandomSamplerPtr rng;
	ProteinLocationSamplerPtr pp;
	BaseSamplingAlgorithmPtr hcsp;
	HalfChainFactory hcf;
    vector<ProteinClassPtr> protein_classes;
    BaseOverhangCodeMapPtr overhang_map;
	Grid3D g;

	HalfChainFactoryTestWithHU() : sp(simulation_parameters_with_HU()),
			iseq(new IdealSequence(test_n_steps_in_loop())),
			rng(new RandomSampler),
            protein_classes(ProteinCorpusFactory(sp.proteins_configuration, StructureFactory()).build()),
            overhang_map(OverhangCodeMapFactory(iseq, protein_classes).build()),
			pp(new HalfChainProteinLocationSampler(protein_classes, rng, overhang_map, iseq)),
			hcsp(new GaussianSampler(rng, iseq)),
			hcf(hcsp, BasePair::Identity(), pp, overhang_map, iseq),
			g(sp) {};

};

TEST_F(HalfChainFactoryTestWithHU,ShouldPairCorrectly) {

	for (auto seed = 0; seed < 25; seed++) {
		auto hc1 = hcf.generate_first_half(seed);
		auto hcs2 = hcf.generate_second_half(seed);

		int paired = 0;
		for (auto i = 0; i < hcs2.size(); i++) {
			if (hc1.overhang_code() == hcs2[i].overhang_code()) {
				paired++;
			}
		}

		ASSERT_EQ(paired, 1);
	}
}

TEST_F(HalfChainFactoryTestWithHU, ShouldGetOverhangCodeAboveZeroWhenOverhung) {
	auto seed = 0;
	auto n_verified = 0;
	while (n_verified < 8) {
		auto hc1 = hcf.generate_first_half(seed);

		if (hc1.protein_locations().size() > 0 and hc1.protein_locations().back().end_step() > iseq->first_half_length()) {
			ASSERT_GT(hc1.overhang_code(), 0);
			n_verified++;
		}
		seed++;
	}
}

TEST_F(HalfChainFactoryTestWithHU,ShouldFindHalfChainInOverhangCache) {
	Seed x = 0;
	HalfChain hc;
	while (true) {
		hc = hcf.generate_first_half(x);
		if (hc.protein_locations().size() > 0 and hc.protein_locations().back().end_step() > iseq->first_half_length()) break;
		x++;
	}

	sp.n_configurations = x + 5;

	g.add_halfchain_to_grid(hc);
	HalfChain hc2 = g.rebuild_halfchain_from_seed(hc.seed());
	ASSERT_TRUE(hc2.overhang_code() > 0);
}

TEST_F(HalfChainFactoryTestWithHU,ShouldGenerateCorrectNumberOfHalfChains) {
	auto expected = hcf.generate_second_half(0).size();
	ASSERT_EQ(expected, 27);
}

TEST_F(HalfChainFactoryTestWithHU,ShouldGenerateSecondHalfsWithAllOverhangCodes) {
	auto second_halfs = hcf.generate_second_half(0);

	// very weak
	for (auto i = 0; i < second_halfs.size(); i++) {
		ASSERT_EQ(second_halfs[i].overhang_code(), i);
	}

}

TEST_F(HalfChainFactoryTestWithoutHU,ShouldGenerateSecondHalfWithZeroOverhangCode) {
	auto second_halfs = hcf.generate_second_half(0)[0];
	ASSERT_EQ(second_halfs.overhang_code(), 0);
}

//TEST_F(HalfChainFactoryTestWithHU,InsertedProteinShouldAppearInGeneratedSteps) {
//	StructurePtr s = boost::dynamic_pointer_cast<HalfChainProteinLocationSampler>(pp)->get_random_structure();
//	ProteinLocation pl = ProteinLocation(s, 5, false);
//	LocationCollection lc; lc.push_back(pl);
//
//	StepParametersVector sp_vec = hcsp->generate_steps_vector(0, 40, 0, lc);
//
//	ASSERT_TRUE(sp_vec[10].isApprox(s->steps[5]));
//
//}
//
//TEST_F(HalfChainFactoryTestWithHU,ReversedProteinShouldAppearInGeneratedSteps) {
//	StructurePtr s = boost::dynamic_pointer_cast<HalfChainProteinLocationSampler>(pp)->get_random_structure();
//	ProteinLocation pl = ProteinLocation(s, 5, true);
//	LocationCollection lc; lc.push_back(pl);
//
//	StepParametersVector sp_vec = hcsp->generate_steps_vector(0, 40, 0, lc);
//
//	ASSERT_TRUE(sp_vec[10].isApprox(s->steps_reversed[5]));
//
//}
//
//TEST_F(HalfChainFactoryTestWithHU,OverhungProteinShouldAppearInGeneratedSteps) {
//	StructurePtr s = boost::dynamic_pointer_cast<HalfChainProteinLocationSampler>(pp)->get_random_structure();
//	ProteinLocation pl = ProteinLocation(s, 38, false);
//	LocationCollection lc; lc.push_back(pl);
//
//	StepParametersVector sp_vec = hcsp->generate_steps_vector(0, 40, 0, lc);
//
//	ASSERT_TRUE(sp_vec[39].isApprox(s->steps[1]));
//
//}

TEST_F(HalfChainFactoryTestWithHU,RepeatedCallsShouldGenerateSameChains) {
	auto second_halfs_1 = hcf.generate_second_half(0);
	auto second_halfs_2 = hcf.generate_second_half(0);

	for (unsigned int i = 0; i < second_halfs_1.size(); i++) {
		ASSERT_TRUE(second_halfs_1[i].bp().isApprox(second_halfs_2[i].bp()));
	}
}

TEST_F(HalfChainFactoryTestWithoutHU, ShouldOnlyGetOneHalfChainWithoutProteins) {
	ASSERT_EQ(hcf.generate_second_half(0).size(), 1);
}

TEST_F(HalfChainFactoryTestWithoutHU, ShouldGenerateCorrectNumberOfFirstHalfSteps) {
	auto steps = hcsp->generate_steps_vector(0, iseq->first_half_length(), 0, LocationCollection());
	auto expected = iseq->first_half_length();
	auto actual = steps.size();
	ASSERT_EQ(actual, expected);
}

TEST_F(HalfChainFactoryTestWithoutHU, ShouldGenerateCorrectNumberOfSecondHalfSteps) {
	auto steps = hcsp->generate_steps_vector(0, iseq->n_steps(), iseq->first_half_length(), LocationCollection());
	auto expected = iseq->second_half_length();
	auto actual = steps.size();
	ASSERT_EQ(actual, expected);
 }*/
