/*
 * StructureFactoryTest.cpp
 *
 *  Created on: Dec 28, 2012
 *      Author: grosner
 */

#include "TestHelpers.h"

#include "HalfChainFactory.h"
#include "BasePairAndStep.h"
#include "RandomSampler.h"
#include "StructureFactory.h"

class StructureFactoryTest : public ::testing::Test {
public:
	string protein_class_name, short_name, dat_filepath, pdb_filepath;
	StructurePtr s;

	StructureFactoryTest() {}

	virtual void SetUp() {
		protein_class_name = "HU";
		short_name = "HU-1";
		dat_filepath = "Resources/Proteins/HU/HU-1.dat";
		pdb_filepath = "Resources/Proteins/HU/HU-1.pdb";
		s = StructureFactory().build(protein_class_name, short_name, dat_filepath, pdb_filepath);
	}
};

TEST_F(StructureFactoryTest,ReversedStepsShouldBeAsLongAsRegularSteps) {
	ASSERT_EQ(s->steps_reversed().size(), s->steps().size());
}

TEST_F(StructureFactoryTest,ShouldReverseStepsCorrectly) {
	StepParameters step  = s->steps()[3];
	int i;
	StepParameters rstep = s->steps_reversed()[s->steps().size()-4];
	rstep[0] *= -1.0; rstep[3] *= -1.0;
	ASSERT_TRUE(step.isApprox(rstep));
}

TEST_F(StructureFactoryTest, CanGetShortNameFromPath) {
	ASSERT_EQ(s->short_name(), short_name);
}

TEST_F(StructureFactoryTest, CanGetProteinClassName) {
	ASSERT_EQ(s->protein_class_name(), protein_class_name);
}

TEST_F(StructureFactoryTest, CanGetDatFilename) {
	ASSERT_EQ(s->dat_path(), dat_filepath);
}

TEST_F(StructureFactoryTest, CanGetPdbFilename) {
	ASSERT_EQ(s->pdb_path(), pdb_filepath);
}

TEST_F(StructureFactoryTest, CanTransformCoordinates) {
	ASSERT_EQ(s->get_c_alphas_transformed_by(BasePair::Identity()).rows(), 187);
}
