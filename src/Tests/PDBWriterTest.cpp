#include "TestHelpers.h"
#include "FileSink.h"
#include "FileProvider.h"
#include "PDBWriter.h"

class PDBWriterTest : public ::testing::Test {
protected:
	StringProvider source;
	StringSink sink;
	PDBWriter pdb_writer;
	BasePair identity;

	virtual void SetUp() {
		source = StringProvider("ATOM    794  N   MET A   1       5.759  23.002  -8.913  1.00 15.00\n"
				                "ATOM    795  CA  MET A   1       7.062  23.333  -9.561  1.00 15.00\n"
				                "ATOM    796  C   MET A   1       7.972  22.119  -9.483  1.00 15.00\n"
				                "ATOM    797  O   MET A   1       8.204  21.614  -8.397  1.00 15.00\n"
				                "ATOM    798  CB  MET A   1       7.714  24.508  -8.850  1.00 15.00\n"
				                "ATOM    799  CG  MET A   1       8.863  25.103  -9.601  1.00 15.00\n"
				                "ATOM    800  SD  MET A   1       9.593  26.457  -8.686  1.00 15.00\n"
				                "ATOM    801  CE  MET A   1       8.616  27.863  -9.438  1.00 15.00\n"
				                "ATOM    802  N   ASN A   2       8.459  21.640 -10.630  1.00 15.00\n"
				                "ATOM    803  CA  ASN A   2       9.517  20.631 -10.653  1.00 15.00\n");
		identity = BasePair::Identity();
	}
};

TEST_F(PDBWriterTest, CanGiveBackSameFileWhenNoRotationApplied) {
	pdb_writer.rewrite_pdb(source, sink, identity);
	ASSERT_EQ(sink.lines, source.consume());
}

TEST_F(PDBWriterTest, CanGiveBackSameFileWhenRotationApplied) {
	StringProvider  expected_lines("ATOM    794  N   MET A   1      -5.759  23.002  -8.913  1.00 15.00\n"
								   "ATOM    795  CA  MET A   1      -7.062  23.333  -9.561  1.00 15.00\n"
								   "ATOM    796  C   MET A   1      -7.972  22.119  -9.483  1.00 15.00\n"
								   "ATOM    797  O   MET A   1      -8.204  21.614  -8.397  1.00 15.00\n"
								   "ATOM    798  CB  MET A   1      -7.714  24.508  -8.850  1.00 15.00\n"
								   "ATOM    799  CG  MET A   1      -8.863  25.103  -9.601  1.00 15.00\n"
								   "ATOM    800  SD  MET A   1      -9.593  26.457  -8.686  1.00 15.00\n"
								   "ATOM    801  CE  MET A   1      -8.616  27.863  -9.438  1.00 15.00\n"
								   "ATOM    802  N   ASN A   2      -8.459  21.640 -10.630  1.00 15.00\n"
								   "ATOM    803  CA  ASN A   2      -9.517  20.631 -10.653  1.00 15.00\n");

	identity(0,0) = -1.0;
	pdb_writer.rewrite_pdb(source, sink, identity);

	ASSERT_EQ(sink.lines, expected_lines.consume());
}
