#include "TestHelpers.h"
#include "WriteTopologyToStream.h"
#include "Closure.h"
#include "BaseSequence.h"

class TestIdGenerator : public ClosureStringGenerator {
public:
	TestIdGenerator() {}
	virtual ~TestIdGenerator() {}
	virtual string generate(const ClosurePtr& closure) const { return "ID,ID"; }
};

class WriteTopologyToStreamTest : public ::testing::Test {
public:
	TestIdGenerator id_generator;
	StreamSinkPtr sink;
	StreamSinkFactoryPtr sink_factory;
	WriteTopologyToStream writer;
	ClosurePtr closure;

	WriteTopologyToStreamTest() :
		id_generator(),
		sink(new StringSink()),
		sink_factory(new StringSinkFactory(sink)),
		writer(sink_factory, id_generator, "output_directory") {

		DNASim::Vertex a(1.2,2,3);
		DNASim::Vertex b(4,5,6);
		DNASim::CurvePoints base, edge;
		base.push_back(a); edge.push_back(b);
		base.push_back(b); edge.push_back(a);

		closure = ClosurePtr(new Closure(BoundaryCondition(StepParameters(), "A1"),
									    HalfChain(BasePair(), LocationCollection(), 1, 0),
									    HalfChain(BasePair(), LocationCollection(), 2, 0),
									    BasePairVector(),
									    StepParametersVector(),
									    LocationCollection(),
									    DNASim::DiscreteRibbon(base, edge),
									    OptionalLacRepressorAndAngle(),
									    BaseSequencePtr(new BaseSequence(78)),
									    10));
	}
};

TEST_F(WriteTopologyToStreamTest, ShouldWriteDiscreteRibbonToStream) {
	vector<string> expected_lines;
	expected_lines.push_back("1.200,2.000,3.000,4.000,5.000,6.000");
	expected_lines.push_back("4.000,5.000,6.000,1.200,2.000,3.000");

	writer.Process(closure);

	ASSERT_EQ(sink->lines, expected_lines);
}
