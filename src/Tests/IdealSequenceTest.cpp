/*
 * SequenceTest.cpp
 *
 *  Created on: Jan 4, 2013
 *      Author: grosner
 */

#include "TestHelpers.h"

#include "BaseSequence.h"
#include "SequenceStepToMeanAndStdMapFactory.h"
#include "SequenceFactory.h"

class IdealSequenceTest : public ::testing::Test {
protected:
	UInteger64 n_steps;
	BaseSequence iseq;
	StepParameters means, stds;

	IdealSequenceTest() : n_steps(79), iseq(n_steps) {
		means << 0, 0, 34.28, 0, 0, 3.4;
		stds  << 4.83934, 4.83934, 4.09273, 0.141421, 0.141421, 0.141421;
	};
};

TEST_F(IdealSequenceTest, ShouldHave78IdealSteps) {
	ASSERT_EQ(iseq.n_steps(), n_steps);
}

TEST_F(IdealSequenceTest, ShouldPickCorrectMeanValueAtStep) {
	Real expected = means[2];
	Real actual = iseq.mean(63)[2];
	ASSERT_EQ(expected, actual);
}

TEST_F(IdealSequenceTest, ShouldPickCorrectStdValueAtStep) {
	Real expected = means[5];
	Real actual = iseq.mean(56)[5];
	ASSERT_EQ(expected, actual);
}

TEST_F(IdealSequenceTest, FirstAndSecondLengthsShouldEqualNsteps) {
	ASSERT_EQ(iseq.n_steps(), iseq.first_half_length()+iseq.second_half_length());
}
