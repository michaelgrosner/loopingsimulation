#include "TestHelpers.h"
#include "RandomSampler.h"
#include "StepParametersGenerator.h"
#include "BaseSequence.h"
#include "Structure.h"
#include "StructureFactory.h"
#include "ProteinClass.h"
#include "ProteinCorpusFactory.h"
#include "PlaceProteinsOnSteps.h"
#include "GaussianSampler.h"

class StepParametersGeneratorTest : public ::testing::Test {
public:
	StructurePtr s;
	RandomSamplerPtr rng;
	PlaceProteinsOnStepsPtr placer;
	BaseSequencePtr iseq;
	BaseSamplerPtr gs;
	StepParametersGenerator steps_generator;

	StepParametersGeneratorTest() : s(ProteinCorpusFactory(simulation_parameters_with_HU().proteins_configuration, StructureFactory()).build().front()->structures().front()),
			rng(new RandomSampler),
			placer(new PlaceProteinsOnSteps()),
			iseq(new BaseSequence(80)),
			gs(new GaussianSampler(rng, iseq)),
			steps_generator(iseq, placer, gs) {}
};

TEST_F(StepParametersGeneratorTest, ShouldGenerateCorrectNumberOfFirstHalfSteps) {
	StepParametersVector steps = steps_generator.generate_steps_vector(0, iseq->first_half_length(), 0, LocationCollection());
	int expected = iseq->first_half_length();
	int actual = steps.size();
	ASSERT_EQ(actual, expected);
}

TEST_F(StepParametersGeneratorTest, ShouldGenerateCorrectNumberOfSecondHalfSteps) {
	StepParametersVector steps = steps_generator.generate_steps_vector(0, iseq->n_steps(), iseq->first_half_length(), LocationCollection());
	int expected = iseq->second_half_length();
	int actual = steps.size();
	ASSERT_EQ(actual, expected);
}

TEST_F(StepParametersGeneratorTest,InsertedProteinShouldAppearInGeneratedSteps) {
	ProteinLocation pl = ProteinLocation(s, 5, false);
	LocationCollection lc; lc.push_back(pl);

	StepParametersVector sp_vec = steps_generator.generate_steps_vector(0, 40, 0, lc);

	ASSERT_TRUE(sp_vec[10].isApprox(s->steps()[5]));

}

TEST_F(StepParametersGeneratorTest,ReversedProteinShouldAppearInGeneratedSteps) {
	ProteinLocation pl = ProteinLocation(s, 5, true);
	LocationCollection lc; lc.push_back(pl);

	StepParametersVector sp_vec = steps_generator.generate_steps_vector(0, 40, 0, lc);

	ASSERT_TRUE(sp_vec[10].isApprox(s->steps_reversed()[5]));

}

TEST_F(StepParametersGeneratorTest,OverhungProteinShouldAppearInGeneratedSteps) {
	ProteinLocation pl = ProteinLocation(s, 38, false);
	LocationCollection lc; lc.push_back(pl);

	StepParametersVector sp_vec = steps_generator.generate_steps_vector(0, 40, 0, lc);

	ASSERT_TRUE(sp_vec[39].isApprox(s->steps()[1]));

}
