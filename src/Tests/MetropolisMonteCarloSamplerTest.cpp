#include "TestHelpers.h"
#include "RandomSampler.h"
#include "BaseSampler.h"
#include "GaussianSampler.h"
#include "MetropolisMonteCarloAcceptanceCriteria.h"
#include "MetropolisMonteCarloSampler.h"
#include "BaseSequence.h"

class MetropolisMonteCarloSamplerTest : public ::testing::Test {
protected:
	RandomSamplerPtr rng;
	BaseSequencePtr dna;
	BaseSamplerPtr gaussian_sampler;

	MetropolisMonteCarloSamplerTest() : rng(new RandomSampler),
										dna(new BaseSequence(2)),
										gaussian_sampler(new GaussianSampler(rng, dna)) {}
};

class AlwaysAcceptOrReject : public MetropolisMonteCarloAcceptanceCriteria {
	bool which;
public:
	AlwaysAcceptOrReject(bool which) : which(which) {}
	virtual ~AlwaysAcceptOrReject() {}
	virtual bool accept(const Real previous_energy, const Real new_energy) { return which; }
};

TEST_F(MetropolisMonteCarloSamplerTest, ShouldReturnSameExactStepsVectorWhenNotAcceptingMove) {
	MetropolisMonteCarloAcceptanceCriteriaPtr criteria(new AlwaysAcceptOrReject(false));
	MetropolisMonteCarloSampler sampler(rng, criteria, gaussian_sampler, dna);

	auto steps_0 = sampler.sample(0, 2, 0);
	auto steps_1 = sampler.sample(1, 2, 0);
	auto steps_2 = sampler.sample(2, 2, 0);
	ASSERT_EQ(steps_0.size(), 2);
	ASSERT_EQ(steps_1.size(), 2);
	ASSERT_EQ(steps_2.size(), 2);
	assert_matrix_equals_or_print_and_throw(steps_0.front(), steps_1.front());
	assert_matrix_equals_or_print_and_throw(steps_0.back(), steps_1.back());
	assert_matrix_equals_or_print_and_throw(steps_0.back(), steps_2.back());
}

class AcceptOnGivenCall : public MetropolisMonteCarloAcceptanceCriteria {
	int which, call;
public:
	AcceptOnGivenCall(int which) : which(which), call(0) {}
	virtual ~AcceptOnGivenCall() {}
	virtual bool accept(const Real previous_energy, const Real new_energy) { return which == call++; }
};

TEST_F(MetropolisMonteCarloSamplerTest, ShouldModifyGivenStepWhenAcceptingAMove) {
	MetropolisMonteCarloAcceptanceCriteriaPtr criteria(new AcceptOnGivenCall(1));
	MetropolisMonteCarloSampler sampler(rng, criteria, gaussian_sampler, dna);

	auto steps_0 = sampler.sample(0, 2, 0);
	auto steps_1 = sampler.sample(1, 2, 0);
	ASSERT_EQ(steps_0.size(), 2);
	ASSERT_EQ(steps_1.size(), 2);
	assert_matrix_equals_or_print_and_throw(steps_0.back(), steps_1.back());
	ASSERT_FALSE(steps_0.front().isApprox(steps_1.front()));
}
