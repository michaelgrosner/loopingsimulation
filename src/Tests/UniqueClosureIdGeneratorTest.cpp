#include "TestHelpers.h"
#include "UniqueClosureIdGenerator.h"
#include "Closure.h"
#include "BaseSequence.h"

TEST(UniqueClosureIdGeneratorTest, ShouldGenerateAGoodId) {
	UniqueClosureIdGenerator generator("fixed", "HU+137");
	BaseSequencePtr dna_sequence(new BaseSequence(78));

	ClosurePtr closure(new Closure(BoundaryCondition(StepParameters(), "A1"),
								    HalfChain(BasePair(), LocationCollection(), 1, 0),
								    HalfChain(BasePair(), LocationCollection(), 2, 0),
								    BasePairVector(),
								    StepParametersVector(),
								    LocationCollection(),
								    DNASim::DiscreteRibbon(DNASim::CurvePoints(), DNASim::CurvePoints()),
								    OptionalLacRepressorAndAngle(),
								    dna_sequence,
								    10));

	string expected = "78,fixed,HU+137,A1,1,2";
	ASSERT_EQ(expected, generator.generate(closure));
}
