/*
 * LoopTest.cpp
 *
 *  Created on: Jan 21, 2013
 *      Author: grosner
 */

#include "TestHelpers.h"
#include "BasePairAndStep.h"
#include "Closure.h"
#include "X3DNAStepsReader.h"
#include "ClosureOverlapDeterminator.h"

/*TEST(ClosureOverlapDeterminatorTest, ThisShouldTriggerOverlap) {
	FileProvider steps_file("TestResources/overlapping_steps.dat");
	X3DNAStepsReader reader(steps_file);
	ProteinClassPtr pclass(new ProteinClass(14, "HU", 137, vector<StructurePtr>()));
	pair<StepParametersVector, LocationCollection> steps_locs = reader.get_steps_and_protein_locations(pclass);

	StepParametersVector steps = steps_locs.first;
	BasePairVector bps;

	BasePair A = BasePair::Identity();
	bps.push_back(A);
	BOOST_FOREACH(StepParameters& s, steps) {
		A *= step_to_bp(s);
		bps.push_back(A);
	}

	ClosurePtr l = ClosurePtr(new Loop(SimulationParameters(), BoundaryCondition(), LacRepressor(), HalfChain(), HalfChain(),
			bps, steps, steps_locs.second, BaseSequencePtr(new IdealSequence(78)), DNASim::DiscreteRibbon(DNASim::CurvePoints(), DNASim::CurvePoints()), 0.0, 0.0));

	ClosureOverlapDeterminator determiner;
	ASSERT_TRUE(determiner.has_overlap(l));
}

TEST(ClosureOverlapDeterminatorTest, ThisShouldNotTriggerOverlap) {
	FileProvider steps_file("TestResources/non_overlapping_steps.dat");
	X3DNAStepsReader reader(steps_file);
	ProteinClassPtr pclass(new ProteinClass(14, "HU", 137, vector<StructurePtr>()));
	pair<StepParametersVector, LocationCollection> steps_locs = reader.get_steps_and_protein_locations(pclass);

	StepParametersVector steps = steps_locs.first;
	BasePairVector bps;

	BasePair A = BasePair::Identity();
	bps.push_back(A);
	BOOST_FOREACH(StepParameters& s, steps) {
		A *= step_to_bp(s);
		bps.push_back(A);
	}

	ClosurePtr l = ClosurePtr(new Loop(SimulationParameters(), BoundaryCondition(), LacRepressor(), HalfChain(),
			HalfChain(), bps, steps, steps_locs.second, BaseSequencePtr(new IdealSequence(78)), DNASim::DiscreteRibbon(DNASim::CurvePoints(), DNASim::CurvePoints()), 0.0, 0.0));

	ClosureOverlapDeterminator determiner;
	ASSERT_FALSE(determiner.has_overlap(l));
}
*/
