#include "TestHelpers.h"
#include "ThreadGroupManager.h"
#include "LazyIncreasingIterator.h"
#include "MutexLockGuard.h"

class TestAlgorithm : public AlgorithmStep {
public:
	TestAlgorithm(int& v) : v(v) {}
	virtual void run() {
		MutexLockGuard lock_guard(mutex);
		v++;
	}
	mutable boost::mutex mutex;
	int& v;
};

TEST(ThreadGroupManagerTests, ShouldDoWorkOnAllCPUThreadsBeforeReturning) {
	int v(0);
	vector<AlgorithmStepPtr> algorithm_steps;
	for (UInteger64 thread_id = 0; thread_id != boost::thread::hardware_concurrency(); thread_id++) {
		algorithm_steps.push_back(AlgorithmStepPtr(new TestAlgorithm(v)));
	}

	ThreadGroupManager tgm(algorithm_steps);
	tgm.run();

	ASSERT_EQ(boost::thread::hardware_concurrency(), v);
}
