/*
 * test_main.cpp
 *
 *  Created on: Dec 24, 2012
 *      Author: grosner
 */

#include "TestHelpers.h"

#include "Grid3D.h"
#include "HalfChain.h"
#include "HalfChainFactory.h"
#include "BasePairAndStep.h"
#include "RandomSampler.h"
#include "BoundaryCondition.h"
#include "SequenceStepToMeanAndStdMapFactory.h"
#include "BoundaryConditionMapFactory.h"
#include "BoundaryConditionParser.h"
#include "LazyIncreasingIterator.h"
#include "ThreadGroupManager.h"
#include "LacHalfFactory.h"

#include <boost/algorithm/string/predicate.hpp>
#include <DNASim/dsMatrix4.h>

TEST(BasePairAndStepTests,ShouldConvertToBasePair) {
	StepParameters step;
	step << -148.52860,
			 -38.833390,
			 -41.249810,
			  18.472000,
			 -31.101000,
			 -67.438000;

	BasePair bp;
	bp << 0.865727,  0.498361,  0.046394,   2.290821,
	      0.429196, -0.786859,  0.443445, -74.248240,
	      0.257502, -0.363991, -0.895100,  18.393528,
	      0.000000,  0.000000,  0.000000,   1.000000;

	assert_matrix_equals_or_print_and_throw(step, bp_to_step(bp));
	assert_matrix_equals_or_print_and_throw(bp, step_to_bp(step));
}

TEST(BasePairAndStepTests,LukeSucks) {
	StepParameters step;
	step << -148.52860,
			 -38.833390,
			 -41.249810,
			  18.472000,
			 -31.101000,
			 -67.438000;

	BasePair ba;
	ba << 0.865727, 0.429196, 0.257502  ,25.147437,
	      0.498361, -0.78685,  -0.363991, -52.8694,
	      0.046394, 0.443445, -0.895100 ,49.282803,
	      0.000000, 0.000000, 0.000000  ,1.000000;

	BasePair x = step_to_bp(step).inverse();
	assert_matrix_equals_or_print_and_throw(x, ba);
}

TEST(BasePairAndStepTests,ShouldBeAbleToIntroconvertSPsToBPs) {
	StepParameters s = random_step();
	BasePair bp = step_to_bp(s);
	ASSERT_TRUE(s.isApprox(bp_to_step(bp)));
}

TEST(BoundaryConditionTests,ShouldBeAbleToReadBoundaryCorrectly) {
	BoundaryConditionParser bcp;
	BoundaryCondition bc = bcp.parse("A1 -148.5286 -38.83339 -41.24981 18.472 -31.101 -67.438");

	BasePair start = BasePair::Identity();
	BasePair end;
	end <<   0.865727,  0.429196,  0.257502,  25.1474,
			 0.498361, -0.786859, -0.363991, -52.8695,
			0.0463943,  0.443445,   -0.8951,  49.2828,
			        0,         0,         0,        1;

	assert_matrix_equals_or_print_and_throw(bc.start(), start);
	assert_matrix_equals_or_print_and_throw(bc.end(),   end);
	ASSERT_EQ("A1", bc.name());
}

TEST(BoundaryConditionTests,CanLoadBoundaryFromStep) {
	StepParameters step;
	step << -149.50310, 40.393100, 39.356400, 17.746000, 31.384900, 67.000400;
	BoundaryCondition boundary(step, "BC");

	BasePair expected_end;
	expected_end <<  0.859640, -0.509784, -0.033762,   3.115377,
			        -0.449730, -0.786417,  0.423428,  73.475187,
			        -0.242408, -0.348812, -0.905300, -19.510957,
			         0.000000,  0.000000,  0.000000,   1.000000;

	assert_matrix_equals_or_print_and_throw(boundary.end(), expected_end);
}

TEST(BasePairAndStepTests,CanConvertToDSTriad) {
	StepParameters s = random_step();
	BasePair bp = step_to_bp(s);
	Vector3d e = bp.topRightCorner<3,1>();

	DNASim::Triad t = bp_to_dsMatrix4(bp);
	Vector3d v(t.origin()[X], t.origin()[Y], t.origin()[Z]);

	ASSERT_TRUE(e.isApprox(v));
}

TEST(SimulationParametersTests,ShouldBeAbleToReadFromVariableMap) {
	variables_map vm = default_options();
	SimulationParameters params(vm);
	ASSERT_EQ(params.n_configurations, 5000000);
}

TEST(SimulationParametersTests,ShouldHaveAStringSequenceCorrespondingToIdealOrSequenceFile) {
	variables_map vm = default_options();
	SimulationParameters params(vm);
	ASSERT_TRUE(boost::algorithm::starts_with(params.dna_sequence, "Ideal+"));
	ASSERT_FLOAT_EQ(11.5, params.twi);
}

TEST(LacHalfTests, ShouldNotBarfOnGettingTriads) {
	string fake_file_string = "ATOM;A;{77.824,-23.453,-27.043}                                                                           \n"
							  "ATOM;B;{61.331,-52.584,-32.302}                                                                           \n"
							  "TRIAD;{-0.0004,-0.0001,0.0002};{1.0,-0.0003,-0.0006};{-0.0003,-1.0,-0.0003};{-0.0006,0.0003,-1.0}         \n"
							  "TRIAD;{-0.6462,0.1542,-3.2674};{0.8261,-0.5505,0.1202};{-0.5538,-0.8326,-0.0075};{0.1042,-0.0603,-0.9927} \n"
							  "TRIAD;{-0.4007,0.2071,-6.3622};{0.4289,-0.9016,0.0559};{-0.9023,-0.4306,-0.0222};{0.044,-0.0409,-0.9982}  \n"
							  "TRIAD;{0.5198,-0.3589,-9.7849};{-0.2217,-0.9747,0.0284};{-0.9626,0.2141,-0.1661};{0.1558,-0.0641,-0.9857} \n"
							  "TRIAD;{1.7755,-1.7147,-13.1579};{-0.6796,-0.7334,0.0176};{-0.729,0.6724,-0.1283};{0.0823,-0.1,-0.9916}    \n"
							  "TRIAD;{2.409,-2.9974,-16.229};{-0.9668,-0.2432,0.0783};{-0.2506,0.9624,-0.1045};{-0.05,-0.1207,-0.9914}   \n"
							  "TRIAD;{3.8797,-3.0199,-19.2425};{-0.9028,0.4185,-0.0989};{0.43,0.8815,-0.1951};{0.0056,-0.2187,-0.9758}   \n"
							  "TRIAD;{3.6844,-1.1046,-23.4705};{-0.69,0.5671,0.4498};{0.5104,0.8218,-0.2533};{-0.5133,0.0548,-0.8565}    \n"
							  "TRIAD;{2.2355,0.4447,-25.9457};{-0.2495,0.9514,0.1804};{0.8713,0.3018,-0.387};{-0.4227,0.0606,-0.9043}    \n"
							  "TRIAD;{-0.3557,0.8962,-28.423};{0.3195,0.9443,0.0789};{0.8316,-0.2395,-0.501};{-0.4542,0.2257,-0.8618}    \n"
							  "TRIAD;{-3.1132,2.006,-30.7174};{0.7199,0.6696,-0.1827};{0.5191,-0.6942,-0.4986};{-0.4607,0.2641,-0.8473}  \n"
							  "TRIAD;{-5.0915,3.4727,-33.5634};{0.9219,0.1166,-0.3695};{0.0125,-0.9622,-0.2722};{-0.3873,0.2463,-0.8885} \n"
							  "TRIAD;{-6.213,4.1373,-36.2933};{0.7563,-0.3656,-0.5426};{-0.4706,-0.8801,-0.0628};{-0.4545,0.3028,-0.8377}\n"
							  "TRIAD;{-7.0458,4.4402,-39.6612};{0.4045,-0.8472,-0.3443};{-0.8544,-0.4844,0.1882};{-0.3262,0.2181,-0.9198}";

	StreamProviderPtr fake_file(new StringProvider(fake_file_string));
	string pdb_filename = "/baz/pdb.pdb";
	string dat_filename = "/foo/bar/dat.dat";
	UInteger64 half_number = 1;
	LacHalf lac = LacHalfFactory().build(fake_file, dat_filename, pdb_filename, half_number);

	vector<DNASim::Triad> triads = lac.get_triads_nearest_triad(DNASim::Triad());
	for (unsigned int i = 0; i < triads.size(); i++) {
		cout << triads[i].origin() << endl;
	}
}

TEST(LazyIncreasingIteratorTests, CanIncreaseInValue) {
	LazyIncreasingIterator lazy_it(0, 10);

	ASSERT_EQ(0, lazy_it.next());
	ASSERT_EQ(1, lazy_it.next());
	ASSERT_EQ(2, lazy_it.next());
}
