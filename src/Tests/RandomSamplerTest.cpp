/*
 * RandomSamplerTest.cpp
 *
 *  Created on: Dec 28, 2012
 *      Author: grosner
 */

#include "TestHelpers.h"

#include "RandomSampler.h"
#include "BasePairAndStep.h"

TEST(RandomSamplerTests,ShouldRepeatedlyGetSameRandomSequenceAfterReseeding) {
	RandomSampler rng;
	rng.reseed(10);
	Matrix<Real, 51, 1> a, b;
	for (unsigned int i = 0; i < 51; i++) a[i] = rng.sample_normal();
	rng.reseed(10);
	for (unsigned int i = 0; i < 51; i++) b[i] = rng.sample_normal();
	ASSERT_TRUE(a.isApprox(b));
}

TEST(RandomSamplerTests,ShouldRepeatedlyGetSameUniformRandomSequenceAfterReseeding) {
	RandomSampler rng;
	rng.reseed(10);
	Matrix<Real, 51, 1> a, b;
	for (unsigned int i = 0; i < 51; i++) a[i] = rng.sample_uniform();
	rng.reseed(10);
	for (unsigned int i = 0; i < 51; i++) b[i] = rng.sample_uniform();
	ASSERT_TRUE(a.isApprox(b));
}
