#include "TestHelpers.h"
#include "LacAndOrientationFileParser.h"
#include "LacHalfFactory.h"
#include "LacHalf.h"

class LacHalfFactoryMock : public LacHalfFactory {
	typedef map<tuple<string, string, UInteger64>, UInteger64 > FuncCalls;
public:
	LacHalfFactoryMock() {}
	virtual LacHalf build(StreamProviderPtr stream, const string& dat, const string& pdb, const UInteger64& half_number) {
		func_calls[make_tuple(dat, pdb, half_number)] += 1;
		return LacHalf();
	}
	FuncCalls func_calls;
};

class LacAndOrientationFileParserTest : public ::testing::Test {
protected:
	LacHalfFactoryPtr lac_half_factory_ptr;
	LacAndOrientationFileParser parser;
	string fake_file_string;
	StreamProviderPtr fake_file;
	BoundaryConditionToLacRepressorsMap actual_map;
	BoundaryCondition first_boundary, last_boundary;
	boost::shared_ptr<VerificationStreamProvider> verify_lacs_were_built;

	virtual void SetUp() {
		lac_half_factory_ptr = LacHalfFactoryPtr(new LacHalfFactoryMock());
		verify_lacs_were_built = boost::shared_ptr<VerificationStreamProvider>(new VerificationStreamProvider());
		parser = LacAndOrientationFileParser("lac/dir", lac_half_factory_ptr, verify_lacs_were_built);
		fake_file_string = "# Should handle comments!\n"
				     "A2_fr1_0011_fr3_0010;fr1_0011;fr3_0010;{146.60,-35.98,85.86,27.78,-62.20,-32.99}\n"
					 "A2_fr1_0011_fr3_0011;fr1_0011;fr3_0011;{154.90,-48.44,72.65,22.26,-65.11,-34.15}\n"
					 "P2_fr1_0001_fr4_0001;fr1_0001;fr4_0001;{-2.21,5.71,-97.89,-0.75,44.80,-100.29}  \n"
					 "P2_fr1_0001_fr4_0002;fr1_0001;fr4_0002;{3.24,9.61,-91.28,5.55,41.58,-102.20}    \n"
					 "P2_fr1_0001_fr4_0003;fr1_0001;fr4_0003;{2.77,12.02,-101.35,3.93,40.77,-102.87}   ";
		fake_file = StreamProviderPtr(new StringProvider(fake_file_string));

		StepParameters sp;
		sp << 146.60,-35.98,85.86,27.78,-62.20,-32.99;
		first_boundary = BoundaryCondition(sp, "A2_fr1_0011_fr3_0010");

		sp << 2.77,12.02,-101.35,3.93,40.77,-102.87;
		last_boundary = BoundaryCondition(sp, "P2_fr1_0001_fr4_0003");
	}
};


TEST_F(LacAndOrientationFileParserTest, ShouldFindCorrectNumberOfBoundaries){
	actual_map = parser.parse_file(fake_file);
	ASSERT_EQ(5, actual_map.size());
}

TEST_F(LacAndOrientationFileParserTest, ShouldParseFirstBoundaryCorrectly) {
	actual_map = parser.parse_file(fake_file);

	boost::shared_ptr<LacHalfFactoryMock> lac_half_factory_mock = boost::dynamic_pointer_cast<LacHalfFactoryMock>(lac_half_factory_ptr);
	ASSERT_EQ(2, lac_half_factory_mock->func_calls[make_tuple("lac/dir/fr1_0011.dat", "lac/dir/fr1_0011.pdb", 1)]);
	ASSERT_EQ(1, lac_half_factory_mock->func_calls[make_tuple("lac/dir/fr3_0010.dat", "lac/dir/fr3_0010.pdb", 2)]);
	ASSERT_TRUE(actual_map.find(first_boundary) != actual_map.end());
}

TEST_F(LacAndOrientationFileParserTest, ShouldParseLastBoundaryCorrectly) {
	actual_map = parser.parse_file(fake_file);

	boost::shared_ptr<LacHalfFactoryMock> lac_half_factory_mock = boost::dynamic_pointer_cast<LacHalfFactoryMock>(lac_half_factory_ptr);
	ASSERT_EQ(3, lac_half_factory_mock->func_calls[make_tuple("lac/dir/fr1_0001.dat", "lac/dir/fr1_0001.pdb", 1)]);
	ASSERT_EQ(1, lac_half_factory_mock->func_calls[make_tuple("lac/dir/fr4_0003.dat", "lac/dir/fr4_0003.pdb", 2)]);
	ASSERT_TRUE(actual_map.find(last_boundary) != actual_map.end());
}
