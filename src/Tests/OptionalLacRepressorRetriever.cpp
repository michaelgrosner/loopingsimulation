//
//  OptionalLacRepressorRetriever.cpp
//  LoopingSimulation
//
//  Created by Michael on 5/24/13.
//
//

#include "TestHelpers.h"
#include "OptionalLacRepressorRetriever.h"

class OptionalLacRepressorRetrieverTest : public ::testing::Test {
public:
    OptionalLacRepressorRetriever retriever;
    BoundaryConditionToLacRepressorsMap map;
    BoundaryCondition empty_boundary, fake_boundary;

    OptionalLacRepressorRetrieverTest() : map(), retriever(map) {}

    virtual void SetUp() {
    	empty_boundary = BoundaryCondition(StepParameters(), "Minicircle");
    	auto ins = make_pair(empty_boundary, OptionalLacRepressor());
    	map.insert(ins);

    	fake_boundary = BoundaryCondition(StepParameters(), "Boundary");
    	auto o = OptionalLacRepressor(LacRepressor(LacRepressor()));
    	map.insert(make_pair(fake_boundary, o));

    }
};

TEST_F(OptionalLacRepressorRetrieverTest, ShouldGiveEmptyLacAndAngleForMinicircle) {
	auto opt_lac = retriever.retrieve(empty_boundary, 0);
	ASSERT_FALSE(opt_lac);
}

TEST_F(OptionalLacRepressorRetrieverTest, ShouldFullLacAndAngleForLoops) {
	auto opt_lac = retriever.retrieve(fake_boundary, 20);
	ASSERT_TRUE(opt_lac);
	ASSERT_EQ(opt_lac->angle(), 20);
}
