/*
 * MockClosureProcessor.h
 *
 *  Created on: Jun 5, 2013
 *      Author: grosner
 */

#ifndef MOCKCLOSUREPROCESSOR_H_
#define MOCKCLOSUREPROCESSOR_H_

#include <gmock/gmock.h>
#include "ClosureProcessor.h"

class MockClosureProcessor : public ClosureProcessor {
public:
	MOCK_METHOD1(Process, void(const ClosurePtr& closure));
};

#endif /* MOCKCLOSUREPROCESSOR_H_ */
