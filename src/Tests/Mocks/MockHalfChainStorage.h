/*
 * MockHalfChainStorage.h
 *
 *  Created on: Jun 5, 2013
 *      Author: grosner
 */

#ifndef MOCKHALFCHAINSTORAGE_H_
#define MOCKHALFCHAINSTORAGE_H_

#include <gmock/gmock.h>
#include "HalfChainStorage.h"

class MockHalfChainStorage : public HalfChainStorage {
public:
	MOCK_METHOD1(add_halfchain_to_grid, void(const HalfChain& hc));
	MOCK_CONST_METHOD1(find_neighbors_to_point, vector<Seed>(const Vector3d& point));
	MOCK_CONST_METHOD1(rebuild_halfchain_from_seed, HalfChain(const Seed& s));
	MOCK_METHOD0(finalize, void());
};


#endif /* MOCKHALFCHAINSTORAGE_H_ */
