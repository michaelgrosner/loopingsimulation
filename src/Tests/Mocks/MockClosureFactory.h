/*
 * MockClosureFactory.h
 *
 *  Created on: Jun 5, 2013
 *      Author: grosner
 */

#ifndef MOCKCLOSUREFACTORY_H_
#define MOCKCLOSUREFACTORY_H_

#include <gmock/gmock.h>
#include "ClosureFactory.h"

class MockClosureFactory : public BaseClosureFactory {
public:
	MOCK_METHOD4(generate_loop, ClosurePtr(HalfChain& first_half,
				  	                       const HalfChain& second_half,
				 	                       const BoundaryCondition& boundary,
				 	                       const Real& angle));
};


#endif /* MOCKCLOSUREFACTORY_H_ */
