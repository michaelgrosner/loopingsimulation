/*
 * MockJFactorReporter.h
 *
 *  Created on: Jun 5, 2013
 *      Author: grosner
 */

#ifndef MOCKJFACTORREPORTER_H_
#define MOCKJFACTORREPORTER_H_

#include <gmock/gmock.h>
#include "JFactorReporter.h"

class MockJFactorReporter : public BaseJFactorReporter {
public:
	MOCK_METHOD0(loop_nears_end_point, void());
	MOCK_METHOD0(loop_end_is_parallel, void());
	MOCK_METHOD0(loop_is_not_overtwisted, void());
	MOCK_METHOD1(loop_is_good, void(const BoundaryCondition& boundary));
	MOCK_METHOD0(report, void());
};


#endif /* MOCKJFACTORREPORTER_H_ */
