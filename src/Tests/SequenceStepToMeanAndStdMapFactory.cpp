/*
 * SequenceStepToMeanAdnStdMapFactory.cpp
 *
 *  Created on: Jan 4, 2013
 *      Author: grosner
 */

#include "TestHelpers.h"

#include "SequenceStepToMeanAndStdMapFactory.h"

class SequenceStepToMeanAndStdMapFactoryTest : public ::testing::Test {
protected:
	SequenceStepToMeanAndStdMapFactory ssmsmf;
	StepParameters means, stds;
	SixBySix coupling, energy;

	SequenceStepToMeanAndStdMapFactoryTest() : ssmsmf("Resources/step_params.dat") {
		means << 0, 0, 34.28, 0, 0, 3.4;
		stds  << 4.83934, 4.83934, 4.09273, 0.141421, 0.141421, 0.141421;
		coupling = SixBySix::Identity();
		energy << 0.0427,      0,      0,      0,      0,      0,
                       0, 0.0427,      0,      0,      0,      0,
                       0,      0, 0.0597,      0,      0,      0,
                       0,      0,      0,     50,      0,      0,
                       0,      0,      0,      0,     50,      0,
                       0,      0,      0,      0,      0,     50;
	};
};

TEST_F(SequenceStepToMeanAndStdMapFactoryTest, CanParseSingleStepUsingTokenizer) {
	string step = " 4.83934  4.83934  4.09273 0.141421 0.141421 0.141421";
	StepParameters actual = ssmsmf.parse_step(step);

	assert_matrix_equals_or_print_and_throw(actual, stds);
}

TEST_F(SequenceStepToMeanAndStdMapFactoryTest, CanParseSingleRecordUsingTokenizer) {
	SequenceStep ss_actual('A', 'A');
	MeanStdCouplingEnergy msp_actual(means, stds, coupling, energy);
	pair<SequenceStep, MeanStdCouplingEnergy> actual(ss_actual, msp_actual);

	string expected_str = "A;A;    0    -0 34.28     0    -0   3.4;0.0427 0.0 0.0 0.0 0.0 0.0 0.0 0.0427 0.0 0.0 0.0 0.0 0.0 0.0 0.0597 0.0 0.0 0.0 0.0 0.0 0.0 50.0 0.0 0.0 0.0 0.0 0.0 0.0 50.0 0.0 0.0 0.0 0.0 0.0 0.0 50.0";
	pair<SequenceStep, MeanStdCouplingEnergy> expected = ssmsmf.parse_sequence_and_step(expected_str);

	ASSERT_EQ(expected.first.second, actual.first.second);
	ASSERT_EQ(expected.first.second, actual.first.second);
	assert_matrix_equals_or_print_and_throw(expected.second.get<0>(), actual.second.get<0>());
	assert_matrix_equals_or_print_and_throw(expected.second.get<1>(), actual.second.get<1>());
	assert_matrix_equals_or_print_and_throw(expected.second.get<2>(), actual.second.get<2>());
	assert_matrix_equals_or_print_and_throw(expected.second.get<3>(), actual.second.get<3>());
}

TEST_F(SequenceStepToMeanAndStdMapFactoryTest, CanLoadSequenceStepToMeanAndStdMapFromFile) {
	SequenceStepToMeanAndStdMap map = ssmsmf.build();

	SequenceStep ss('Z', 'Z');
	assert_matrix_equals_or_print_and_throw(map[ss].get<0>(), means);
	assert_matrix_equals_or_print_and_throw(map[ss].get<1>(), stds);
	assert_matrix_equals_or_print_and_throw(map[ss].get<2>(), coupling);
	assert_matrix_equals_or_print_and_throw(map[ss].get<3>(), energy);
}

TEST_F(SequenceStepToMeanAndStdMapFactoryTest, CanParseASixBySix) {
	SixBySix expected;
	expected << 0.0427,      0,      0,      0,      0,      0,
                     0, 0.0427,      0,      0,      0,      0,
                     0,      0, 0.0597,      0,      0,      0,
                     0,      0,      0,     50,      0,      0,
                     0,      0,      0,      0,     50,      0,
                     0,      0,      0,      0,      0,     50;

	string s = "0.0427 0.0 0.0 0.0 0.0 0.0 0.0 0.0427 0.0 0.0 0.0 0.0 0.0 0.0 0.0597 0.0 0.0 0.0 0.0 0.0 0.0 50.0 0.0 0.0 0.0 0.0 0.0 0.0 50.0 0.0 0.0 0.0 0.0 0.0 0.0 50.0";

	SixBySix actual = ssmsmf.parse_forces(s);

	assert_matrix_equals_or_print_and_throw(actual, expected);

}
