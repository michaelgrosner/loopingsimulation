#include "TestHelpers.h"
#include "Closure.h"
#include "ClosureCSVSummaryGenerator.h"
#include "BaseSequence.h"

class TestHeaderGenerator : public ClosureStringGenerator {
public:
	virtual ~TestHeaderGenerator() {}
	virtual string generate(const ClosurePtr& closure) const { return "HEADER"; }
};

TEST(ClosureCSVSummaryGeneratorTest, ShouldGenerateAGoodCSVVersionOfClosure) {
	TestHeaderGenerator id_generator;
	ClosureCSVSummaryGenerator generator(id_generator);
	BaseSequencePtr dna_sequence(new BaseSequence(78));

	DNASim::Vertex a(1.2,2,3);
	DNASim::Vertex b(4,5,6);
	DNASim::CurvePoints base, edge;
	base.push_back(a); edge.push_back(b);
	base.push_back(b); edge.push_back(a);
	base.push_back(a); edge.push_back(b);
	base.push_back(b); edge.push_back(a);

	ClosurePtr closure(new Closure(BoundaryCondition(StepParameters(), "A1"),
							    HalfChain(BasePair(), LocationCollection(), 1, 0),
							    HalfChain(BasePair(), LocationCollection(), 2, 0),
							    BasePairVector(),
							    StepParametersVector(),
							    LocationCollection(),
							    DNASim::DiscreteRibbon(base, edge),
							    OptionalLacRepressorAndAngle(LacRepressorAndAngle(LacRepressor(), 10)),
							    dna_sequence,
							    20));

	string expected = "HEADER,20.000,0,nan,0.000,0.000,10.000"; // hacky
	ASSERT_EQ(expected, generator.generate(closure));
}

TEST(ClosureCSVSummaryGeneratorTest, ShouldGenerateAGoodCSVVersionOfMinicircle) {
	TestHeaderGenerator id_generator;
	ClosureCSVSummaryGenerator generator(id_generator);
	BaseSequencePtr dna_sequence(new BaseSequence(78));
    
	DNASim::Vertex a(1.2,2,3);
	DNASim::Vertex b(4,5,6);
	DNASim::CurvePoints base, edge;
	base.push_back(a); edge.push_back(b);
	base.push_back(b); edge.push_back(a);
	base.push_back(a); edge.push_back(b);
	base.push_back(b); edge.push_back(a);
    
	ClosurePtr closure(new Closure(BoundaryCondition(StepParameters(), "A1"),
                                   HalfChain(BasePair(), LocationCollection(), 1, 0),
                                   HalfChain(BasePair(), LocationCollection(), 2, 0),
                                   BasePairVector(),
                                   StepParametersVector(),
                                   LocationCollection(),
                                   DNASim::DiscreteRibbon(base, edge),
                                   OptionalLacRepressorAndAngle(),
                                   dna_sequence,
                                   20));
    
	string expected = "HEADER,20.000,0,nan,0.000,0.000,0.000";
	ASSERT_EQ(expected, generator.generate(closure));
}
