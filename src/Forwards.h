//
//  Forwards.h
//  LoopingSimulation
//
//  Created by Michael on 5/23/13.
//
//

#ifndef LoopingSimulation_Forwards_h
#define LoopingSimulation_Forwards_h

#include <boost/shared_ptr.hpp>

class StructureFactory;
typedef boost::shared_ptr<StructureFactory> StructureFactoryPtr;

class ProteinClass;
typedef boost::shared_ptr<ProteinClass> ProteinClassPtr;

class Structure;
typedef boost::shared_ptr<Structure> StructurePtr;

class ProteinLocation;
typedef boost::shared_ptr<ProteinLocation> ProteinLocationPtr;

class BaseProteinLocationSampler;
typedef boost::shared_ptr<BaseProteinLocationSampler> ProteinLocationSamplerPtr;

class DiscreteRibbonConverter;
typedef boost::shared_ptr<DiscreteRibbonConverter> DiscreteRibbonConverterPtr;

class BaseSequence;
typedef boost::shared_ptr<BaseSequence> BaseSequencePtr;

class HalfChain;
typedef boost::shared_ptr<HalfChain> HalfChainPtr;

class Closure;
typedef boost::shared_ptr<Closure> ClosurePtr;

class BaseOverhangCodeMap;
typedef boost::shared_ptr<BaseOverhangCodeMap> BaseOverhangCodeMapPtr;

class OptionalLacRepressorRetriever;
typedef boost::shared_ptr<OptionalLacRepressorRetriever> OptionalLacRepressorRetrieverPtr;

class BoundaryCondition;
typedef boost::shared_ptr<BoundaryCondition> BoundaryConditionPtr;

class LacHalfFactory;
typedef boost::shared_ptr<LacHalfFactory> LacHalfFactoryPtr;

class StreamProvider;
typedef boost::shared_ptr<StreamProvider> StreamProviderPtr;

class StreamSinkFactory;
typedef boost::shared_ptr<StreamSinkFactory> StreamSinkFactoryPtr;

class LacAndOrientationFileParser;
typedef boost::shared_ptr<LacAndOrientationFileParser> LacAndOrientationFileParserPtr;

class RandomSampler;
typedef boost::shared_ptr<RandomSampler> RandomSamplerPtr;

class ClosureStringGenerator;
typedef boost::shared_ptr<ClosureStringGenerator> ClosureStringGeneratorPtr;

class SimulationParameters;
typedef boost::shared_ptr<SimulationParameters> SimulationParametersPtr;

class SamplingParametersProvider;
typedef boost::shared_ptr<SamplingParametersProvider> SamplingParametersProviderPtr;

class PDBWriter;
typedef boost::shared_ptr<PDBWriter> PDBWriterPtr;

class AlgorithmStep;
typedef boost::shared_ptr<AlgorithmStep> AlgorithmStepPtr;

class LazyIncreasingIterator;
typedef boost::shared_ptr<LazyIncreasingIterator> LazyIncreasingIteratorPtr;

class ChainProcessor;
typedef boost::shared_ptr<ChainProcessor> ChainProcessorPtr;

class HalfChainStorage;
typedef boost::shared_ptr<HalfChainStorage> HalfChainStoragePtr;

class TwoSidedHalfChainFactory;
typedef boost::shared_ptr<TwoSidedHalfChainFactory> HalfChainFactoryPtr;

class StatusReporter;
typedef boost::shared_ptr<StatusReporter> StatusReporterPtr;

class Reporter;
typedef boost::shared_ptr<Reporter> ReporterPtr;

class JFactorReporter;
typedef boost::shared_ptr<JFactorReporter> JFactorReporterPtr;

class ClosureProcessor;
typedef boost::shared_ptr<ClosureProcessor> ClosureProcessorPtr;

class PlaceProteinsOnSteps;
typedef boost::shared_ptr<PlaceProteinsOnSteps> PlaceProteinsOnStepsPtr;

class StepParametersGenerator;
typedef boost::shared_ptr<StepParametersGenerator> StepParametersGeneratorPtr;

class BaseSampler;
typedef boost::shared_ptr<BaseSampler> BaseSamplerPtr;

#endif
