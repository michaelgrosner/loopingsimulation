import glob
import json
import random
import os
import re
import sys
import subprocess
import numpy

try:
    from pymol.cmd import *
    pymol_imported = True
except ImportError:
    pymol_imported = False

try:
    import LoopSetup
except:
    print "failed"

def single_loop(directory, state=1):
    d = os.path.join(os.getcwd(), directory)
   
    pwd = os.getcwd()
    os.chdir(d)
    #if not os.path.exists('structure.pdb'):
    if True:
        import LoopSetup
        print "Setting up %s from PymolLoops.py" % directory
        LoopSetup.from_path(directory)

    for pdb in glob.glob("*.pdb"):
        load(pdb, state=state)
    os.chdir(pwd)
    return os.path.basename(d)
    
def all_loops():
    ld = os.path.expanduser("~/Dropbox/LoopingSimulation/output/*")
    loop_glob = glob.glob(ld)

    loops = []
    for state, d in enumerate(loop_glob, start=1):
        loops.append(single_loop(d, state=state))
    print "Loaded %i loops" % len(loops)
    if pymol_imported:
        color("0xFFC1C1", "h*")
        color("0x6495ED", "structure")
        color("0xFFC125", "pro*")

        # Gradient coloring
        """
        stored.list = []
        iterate_state(1, "chain i+j", "stored.list.append(resi)")
        nr = range(1, max(int(x) for x in stored.list)+1)
        pairings = list(zip(nr[:len(nr)/2+1], nr[:len(nr)/2-1:-1]))
        a = numpy.array([100,149,237]) #cornflowerblue
        b = numpy.array([202,225,255]) #some other blue
        for base_a, base_b in pairings:
            coloring = a+base_a*(b-a)/float(len(pairings))
            select("hl", "(chain i+j & resi %(base_a)i) or (chain i+j & resi %(base_b)i)" % locals())
            set_color("color%(base_a)i" % locals(), list(coloring))
            color("color%(base_a)i" % locals(), "hl")
        #color("color%(base_a)i" % locals(), "chain g+h")
        #color("color1", "chain e+f")
        """

        """
        # Site coloring
        # -10 site
        m10 = "red", pairings[23:29]
        m35 = "blue", pairings[47:53]
        CAP = "green", pairings[68:88]
        
        for c, list_of_pairs in (m10, m35, CAP):
            sel = "chain k+l & resi "
            sel += "+".join("%i+%i" % p for p in list_of_pairs)
            color(c, sel)
        """

        hide("lines")
        show("cartoon")
        cartoon("rect")
        create("lac", "all")
        delete("h* & structure & pro*")
        select("cas", "chain a+b+e+f & name CA")
        intra_fit("cas", 1)
        origin("cas")
        orient("cas")
        zoom()
        bg_color("white")
        deselect()
        
        set("cartoon_tube_radius", "0.8")
        set("cartoon_ring_mode", "3")
        set("cartoon_ring_transparency", "0.5")
        set("cartoon_ring_finder", "1")
        set("ray_trace_fog", "off")
        set("ray_opaque_background", "off")
        set("ray_trace_frames", "on")
        set("hash_max", "50")
        #viewport(1400,1400)
        viewport(140,140)

        if not os.path.exists("FadedPictures"): 
            os.mkdir("FadedPictures")
        for state, l in enumerate(loops, start=1):
            png_filename = "FadedPictures/%(l)s.png" % locals()
            print png_filename
            mpng(png_filename, state, state)
    else:
        print "pymol not loaded"

all_loops()
