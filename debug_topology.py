try:
    from pymol.cgo import *
    from pymol.cmd import *
except:
    print "Not using Pymol"
import os, glob, subprocess, random
import numpy as np

devnull = os.devnull

loops_glob = glob.glob("output/steps_*.dat")
for state, steps_dat in enumerate(loops_glob, start=1):

    loop_index = steps_dat.split("_", 1)[-1].split(".", 1)[0]
    print loop_index.split(",")

    steps_pdb = os.path.splitext(steps_dat)[0]+".pdb"
    if not os.path.exists(steps_pdb):
        os.system("rebuild -atomic %(steps_dat)s %(steps_pdb)s 2> %(devnull)s" % locals())
    if not os.path.exists(steps_pdb):
        continue
    load(steps_pdb, "steps", state=state)

    half1_pdb = steps_pdb.replace("steps", "half1")
    if os.path.exists(half1_pdb):
        load(half1_pdb, "half1", state=state)
    else:
        print "failed to find", half1_pdb

    half2_pdb = steps_pdb.replace("steps", "half2")
    if os.path.exists(half2_pdb):
        load(half2_pdb, "half2", state=state)

    p = steps_pdb.replace("steps", "HU*")
    for pi, prot in enumerate(glob.iglob(p)):
        load(prot, "proteins%i" % pi, state=state)

    curves_csv = steps_pdb.replace("steps", "curves")\
            .replace("pdb", "dat")
    curves = np.genfromtxt(curves_csv, delimiter=",")
    edge_cgo = [LINEWIDTH, 10, BEGIN, LINES, COLOR, 1.0, 0.0, 0.0]
    base_cgo = [LINEWIDTH, 10, BEGIN, LINES, COLOR, 1.0, 0.0, 1.0]
    for vertex, vertex_next in zip(curves[:-1], curves[1:]):
        base_cgo.extend([VERTEX] + list(vertex[:3]))
        base_cgo.extend([VERTEX] + list(vertex_next[:3]))
        edge_cgo.extend([VERTEX] + list(vertex[3:]))
        edge_cgo.extend([VERTEX] + list(vertex_next[3:]))

    base_cgo.extend([VERTEX] + list(curves[0]))
    base_cgo.extend([VERTEX] + list(curves[-1]))
    edge_cgo.extend([VERTEX] + list(curves[0]))
    edge_cgo.extend([VERTEX] + list(curves[-1]))

    load_cgo(base_cgo, "base", state=state)
    load_cgo(edge_cgo, "edge", state=state)

    print state, steps_dat


select("dna", "resn a+g+t+c")
select("lac", "half* & !dna")
select("hu", "pro*")

color("0xFFC1C1", "lac")
color("0x6495ED", "dna")
color("0xFFC125", "hu")

hide("lines")
show("cartoon")
cartoon("rect")

deselect()

orient("lac")
zoom(complete=1)
