from os import system as s
import itertools

header = """#!/bin/sh
#PBS -l nodes=1
#PBS -l walltime=5:00:00
#PBS -V
#PBS -m bea
#PBS -M michaelgrosner@gmail.com
cd $PBS_O_WORKDIR

"""

proteins = [("", int(10**8)), 
			("-p HU+137", int(10**6))]
lacs = ["-l fixed", 
		"-l flexible -f A1P1", 
		"-l flexible -f A2P2"]
bps = [78, 95, 101]
methods = itertools.product(proteins, lacs, bps)

for i, ((prot, n), lac, bp) in enumerate(methods):
	lsplit = lac.split()
	lstring = "_%s_%s"%(lsplit[1],lsplit[3]) if len(lsplit) ==4 else "_fixed"
	name = "%i_None_%i"%(bp,n) if not prot else "%i_HU_%i"%(bp,n)
	j = "job_%(name)s_%(lstring)s" % locals()
	with open(j, "w") as x:
		x.write("%(header)s ./LoopingSimulation -n %(n)s %(lac)s %(prot)s -s Ideal+%(bp)i" % locals())
	s("qsub %(j)s" % locals())
