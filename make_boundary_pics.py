try:
    from pymol.cgo import *
    from pymol.cmd import *
except:
    print "Not using Pymol"
import os, glob, subprocess, random, string, sets
import numpy as np

devnull = os.devnull

# Load loop directory folders
loops_glob = glob.glob("output_pics/76*")
max_loops = 15
if len(loops_glob) > max_loops: 
    loops_glob = random.sample(loops_glob, max_loops)

for state, steps_dir in enumerate(loops_glob, start=1):
    steps_pdb = glob.glob(steps_dir+"/steps*")[0]
    load(steps_pdb, "steps", state=state)

    half1_pdb = steps_pdb.replace("steps", "half1")
    if os.path.exists(half1_pdb):
        load(half1_pdb, "half1", state=state)

    half2_pdb = steps_pdb.replace("steps", "half2")
    if os.path.exists(half2_pdb):
        load(half2_pdb, "half2", state=state)

    print state, steps_dir

select("dna", "resn a+g+t+c+da+dg+dt+dc")
select("lac", "half* & !dna")
select("hu", "pro*")

color("0xFFC1C1", "lac")
color("0x6495ED", "dna")
color("0xFFC125", "hu")

# some chains in half1/2/steps are the same, need to rename them to
# different names, or else PyMol gets really confused when aligning
# and displaying as cartoon
objs = ("steps", "half1", "half2")
new_chains = ["E", "F", "G", "H"]
for state, steps_dir in enumerate(loops_glob, start=1):
    for new_ch, ch in zip(new_chains, get_chains('half1', state=state)):
        do("alter (half1 & chain %s), chain='%s'" % (ch, new_ch))
    create("everything", "all", state, state)

delete("dna or half* or hu or steps or lac")
half_chains = ['E', 'F', 'G', 'H', 'C', 'D', 'X', 'Y']
half_chains_sele = " | ".join("chain "+c for c in half_chains)
intra_fit(half_chains_sele, 1)

orient(half_chains_sele)
orient(half_chains_sele)
zoom(complete=1)
viewport(1400,1400)

hide("lines")
show("cartoon")
cartoon("rect")
set("cartoon_tube_radius", "0.8")
set("cartoon_ring_mode", "3")
set("cartoon_ring_transparency", "0.5")
set("cartoon_ring_finder", "1")
set("ray_trace_fog", "off")
set("ray_opaque_background", "off")
set("ray_trace_frames", "on")
set("hash_max", "50")
bg_color("white") 

deselect()
