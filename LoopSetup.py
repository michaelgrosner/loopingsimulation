import sys
import os
import subprocess
import glob
import re

def change_chain(filename, first_chain):
	if isinstance(first_chain, basestring):
		first_chain = ord(first_chain)

	with open(filename, "r") as f:
		f_lines = [line for line in f if line.startswith("ATOM")]

	chain_set = sorted(set(line[21] for line in f_lines))

	chain_mapping = {}
	for i, c in enumerate(chain_set):
		chain_mapping[c] = chr(first_chain+i)
	last_chain = first_chain+i

	with open(filename, "w") as f:
		for line in f_lines:
			l = list(line)
			l[21] = chain_mapping[l[21]]
			line = ''.join(l)
			f.write(line)
	return last_chain+1

def setup(lac, orientation, directory=None):

	if directory:
		pwd = os.getcwd()
		os.chdir(directory)
	if not os.path.isfile("loop.info"):
		raise Exception("Directory %s doesn't look like a Loop directory" % os.getcwd())
	
	if not glob.glob(os.path.join(directory, "Atom*")):
		os.system("cp_std BDNA &> /dev/null")

	half1_pdb = "half1.pdb"
	if not os.path.isfile(half1_pdb):
		one_zero = "0" if "1" in orientation else "1"
		if "flexible" in lac.lower():
			if orientation == "A1":
				one_zero = "1"
			elif orientation == "A2":
				one_zero = "0"
		half1_link = os.path.expanduser("~/Dropbox/closure_v2/bin/half1_%s.pdb" %\
				one_zero)
		os.system("ln -f %(half1_link)s half1.pdb" % locals())

	half2_pdb = "half2.pdb"
	new_chain = change_chain(half2_pdb, "E")

	structure_pdb = "structure.pdb"
	if not os.path.isfile(structure_pdb):
		devnull = "2> "+os.devnull if True else ""
		cmd =  "rebuild -atomic structure.dat structure.pdb %(devnull)s; \
				get_part -n half1.pdb h1_dna.pdb; \
				get_part -p half1.pdb h1no.pdb; \
				get_part -n half2.pdb h2_dna.pdb; \
				get_part -p half2.pdb h2no.pdb; \
				cat h1_dna.pdb structure.pdb h2_dna.pdb | awk '$1 == \"ATOM\"' > dna.pdb; \
				find_pair dna.pdb dna.out %(devnull)s; \
				analyze dna.out %(devnull)s; \
				rebuild -atomic bp_step.par rebuilt_dna.pdb %(devnull)s; \
				frame_mol -15 ref_frames.dat rebuilt_dna.pdb framed_dna.pdb %(devnull)s; " % locals()
		subprocess.call(cmd, shell=True)
		
		new_chain = change_chain("framed_dna.pdb", new_chain)	
		with open("framed_dna.pdb", "r") as fd:
			dd = fd.readlines()

		with open("structure.pdb", "w") as ndna:
			for line in dd:
				ndna.write(line)
			with open("rebuilt_dna.pdb", "r") as dna:
				for dna_line in dna:
					if dna_line.startswith("CONECT"):
						ndna.write(dna_line)
			ndna.write("END")
		os.system("rm -f *.par *.dat best* *.scr *.out framed* *[0-9]_dna.pdb ha* hel* \
				hst* re* sta* dna.pdb")
	
	proteins_pdb = "proteins.pdb"
	if not glob.glob("proteins[0-9]*.pdb") and os.path.isfile(proteins_pdb):
		protein_head = os.path.splitext(proteins_pdb)[0]
		with open(proteins_pdb, "r") as pf:
			pf = pf.read()
		for i, p in enumerate(pf.split("TER")):
			if len(p) < 10: continue
			p_new_filename = protein_head+str(i)+".pdb"
			with open(p_new_filename, "w") as pnew:
				pnew.write(p)
			new_chain = change_chain(p_new_filename, new_chain)
		os.system("rm %s" % proteins_pdb)

	if os.path.isfile("h1no.pdb"): os.system("mv h1no.pdb half1.pdb")
	if os.path.isfile("h2no.pdb"): os.system("mv h2no.pdb half2.pdb")

	if glob.glob(os.path.join(directory, "Atom*")):
		os.system("rm -f Atom*")

	if directory: os.chdir(pwd)

def from_path(d):
	lac, proteins,  bp, orientation, number = re.match(r".+/(\w+)/proteins_(\w+)\.dat/(\d+)/(.+)/Loops/(\d+)$", d).groups()
	setup(lac, orientation, directory=d)

def setup_multiple_directories(list_of_directories, multithreaded=False):
	if multithreaded:
		from multiprocessing import Pool, cpu_count
		p = Pool(20)
		r = p.map_async(from_path, list_of_directories)
		r.wait()
	else:
		map(from_path, list_of_directories)

if __name__ == "__main__":
	if len(sys.argv) > 2:
		list_of_directories = map(os.path.abspath, sys.argv[1:])
	else:
		list_of_directories = glob.glob("output/*/*/*/*/*/*")
	print "Beginning to setup %i Loops" % len(list_of_directories)
	setup_multiple_directories(list_of_directories)
